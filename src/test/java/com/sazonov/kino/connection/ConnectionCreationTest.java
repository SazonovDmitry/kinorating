package com.sazonov.kino.connection;

import com.sazonov.kino.exception.WrapperConnectionException;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.sql.PreparedStatement;

public class ConnectionCreationTest {
    @Test
    public void connectionPoolTest() {
        String query = "SELECT c_uid, c_name FROM country;";
        ConnectionPool pool = ConnectionPool.getInstance();
        PreparedStatement statement = null;
        try {
            statement = pool.getConnection().getPreparedStatement(query);
        } catch (WrapperConnectionException exception) {
            Assert.fail();
        }
        Assert.assertNotNull(statement);
    }
}
