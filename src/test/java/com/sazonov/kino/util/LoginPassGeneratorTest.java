package com.sazonov.kino.util;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginPassGeneratorTest {
    @Test
    public void checkLoginGenerator() {
        String firstLogin = LoginPassGenerator.buildLogin();
        String secondLogin = LoginPassGenerator.buildLogin();
        Assert.assertNotEquals(firstLogin, secondLogin);
    }

    @Test
    public void checkPassGenerator() {
        String firstPass = LoginPassGenerator.buildPassword();
        String secondPass = LoginPassGenerator.buildPassword();
        Assert.assertNotEquals(firstPass, secondPass);
    }
}
