package com.sazonov.kino.util;

import com.sazonov.kino.entity.UserStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UserStatusTest {
    @Test
    public void checkUserStatus() {
        UserStatusDefiner definer = new UserStatusDefiner();
        UserStatus expected = UserStatus.KINOMAN;
        UserStatus actual = definer.defineStatus(3);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void checkIncorrectUserStatus() {
        UserStatusDefiner definer = new UserStatusDefiner();
        UserStatus expected = UserStatus.KINO_BEGINNER;
        UserStatus actual = definer.defineStatus(15);
        Assert.assertEquals(actual, expected);
    }
}
