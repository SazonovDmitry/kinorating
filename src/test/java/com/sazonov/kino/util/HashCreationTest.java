package com.sazonov.kino.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class HashCreationTest {
    @Test
    public void checkHashCreation() {
        HashCreator creator = new HashCreator();
        String testString = "admin";
        String expected = "beed895c639d9f04443bd3a38599db92";
        String actual = creator.createHash(testString);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void checkMapHashDefiner() {
        String[] testArray = {"a", "b", "c"};
        String testKey = "key";
        Map testMap = new HashMap();
        testMap.put(testKey, testArray);
        HashCreator creator = new HashCreator();
        String expected = "507cf21f0ba27a5ca37edd069cd14cfc";
        String actual = creator.mapHashDefiner(testMap);
        Assert.assertEquals(actual, expected);
    }
}
