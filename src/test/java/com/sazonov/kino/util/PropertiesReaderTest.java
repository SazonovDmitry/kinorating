package com.sazonov.kino.util;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Properties;

public class PropertiesReaderTest {
    @Test
    public void checkPoolSizeProperties() {
        int expected = 30;
        PropertiesReader reader = new PropertiesReader();
        Properties properties = reader.createPoolSizeProperties();
        int actual = Integer.parseInt(properties.getProperty("poolSize"));
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void checkConnectionProperties() {
        String expectedUrl = "jdbc:mysql://localhost:3306/kinorating_db";
        PropertiesReader reader = new PropertiesReader();
        Properties properties = reader.createConnectionProperties();
        String actualUrl = properties.getProperty("url");
        Assert.assertEquals(actualUrl, expectedUrl);
    }

    @Test
    public void checkMailProperties() {
        String expectedHost = "smtp.gmail.com";
        PropertiesReader reader = new PropertiesReader();
        Properties properties = reader.createMailProperties();
        String actualHost = properties.getProperty("mail.smtp.host");
        Assert.assertEquals(actualHost, expectedHost);
    }
}
