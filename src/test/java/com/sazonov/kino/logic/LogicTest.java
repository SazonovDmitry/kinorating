package com.sazonov.kino.logic;

import com.sazonov.kino.entity.Country;
import com.sazonov.kino.entity.Genre;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class LogicTest {
    @Test
    public void countriesCountTest() {
        int expected = 251;
        PictureLogic logic = new PictureLogic();
        List<Country> countries = logic.createCountryList();
        int actual = countries.size();
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void genresCountTest() {
        int expected = 32;
        PictureLogic logic = new PictureLogic();
        List<Genre> genres = logic.createGenreList();
        int actual = genres.size();
        Assert.assertEquals(actual, expected);
    }
}
