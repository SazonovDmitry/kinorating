package com.sazonov.kino.command;

import com.sazonov.kino.logic.PictureLogic;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Optional;

public class ActionFactoryTest {
    @Test
    public void checkActionFactory() {
        String topPictures = "top-pictures";
        Optional<Command> optionalCommand = ActionFactory.defineCommand(topPictures);
        Command actual = optionalCommand.orElse(new EmptyCommand());
        Command expected = new TopPicturesCommand(new PictureLogic());
        Assert.assertEquals(actual.getClass(), expected.getClass());
    }

    @Test
    public void checkActionFactoryWithInvalidParam() {
        String command = "logon";
        Optional<Command> optionalCommand = ActionFactory.defineCommand(command);
        Command actual = optionalCommand.orElse(new EmptyCommand());
        Command expected = new EmptyCommand();
        Assert.assertEquals(actual.getClass(), expected.getClass());
    }
}
