package com.sazonov.kino.validator;

import org.testng.Assert;
import org.testng.annotations.Test;

public class PasswordValidatorTest {
    @Test
    public void checkForInvalidPassword() {
        LoginValidator validator = new LoginValidator();
        String incorrectPassword = "AbKc";
        Assert.assertFalse(validator.isPasswordValid(incorrectPassword));
    }

    @Test
    public void checkForNullPassword() {
        LoginValidator validator = new LoginValidator();
        String nullPassword = null;
        Assert.assertFalse(validator.isPasswordValid(nullPassword));
    }

    @Test
    public void checkForCorrectPassword() {
        LoginValidator validator = new LoginValidator();
        String password = "Ab1Dc4ff";
        Assert.assertTrue(validator.isPasswordValid(password));
    }
}
