package com.sazonov.kino.validator;

import org.testng.Assert;
import org.testng.annotations.Test;

public class EmailValidatorTest {
    @Test
    public void checkForInvalidEmail() {
        LoginValidator validator = new LoginValidator();
        String incorrectEmail = "kdgflk!J.@fsdklf.sdfd";
        Assert.assertFalse(validator.isEmailValid(incorrectEmail));
    }

    @Test
    public void checkForNullEmail() {
        LoginValidator validator = new LoginValidator();
        String nullEmail = null;
        Assert.assertFalse(validator.isEmailValid(nullEmail));
    }

    @Test
    public void checkForCorrectEmail() {
        LoginValidator validator = new LoginValidator();
        String email = "kinoman.best_1990@tut.by";
        Assert.assertTrue(validator.isEmailValid(email));
    }
}
