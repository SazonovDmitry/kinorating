package com.sazonov.kino.validator;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginValidatorTest {
    @Test
    public void checkForInvalidLogin() {
        LoginValidator validator = new LoginValidator();
        String incorrectLogin = "AbKc";
        Assert.assertFalse(validator.isLoginValid(incorrectLogin));
    }

    @Test
    public void checkForNullDirector() {
        LoginValidator validator = new LoginValidator();
        String nullLogin = null;
        Assert.assertFalse(validator.isLoginValid(nullLogin));
    }

    @Test
    public void checkForCorrectLogin() {
        LoginValidator validator = new LoginValidator();
        String login = "Ab1Dc4";
        Assert.assertTrue(validator.isLoginValid(login));
    }
}
