package com.sazonov.kino.validator;

import org.testng.Assert;
import org.testng.annotations.Test;

public class NameValidatorTest {
    @Test
    public void checkForInvalidName() {
        LoginValidator validator = new LoginValidator();
        String incorrectName= "Петя123";
        Assert.assertFalse(validator.isNameValid(incorrectName));
    }

    @Test
    public void checkForNullName() {
        LoginValidator validator = new LoginValidator();
        String nullName = null;
        Assert.assertFalse(validator.isNameValid(nullName));
    }

    @Test
    public void checkForCorrectName() {
        LoginValidator validator = new LoginValidator();
        String name = "Admin";
        Assert.assertTrue(validator.isNameValid(name));
    }
}
