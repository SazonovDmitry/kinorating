package com.sazonov.kino.command;

import com.sazonov.kino.entity.*;
import com.sazonov.kino.logic.DirectorLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import com.sazonov.kino.validator.DirectorValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Class {@code AddFavoriteCommand} is realisation of {@code Command} interface.
 * It provides the addition of new director to database.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK9.0
 * @version 1.0
 */

public class CreateDirectorCommand implements Command {
    /*Class logger initialization.*/
    private static final Logger LOGGER = LogManager.getLogger(CreateDirectorCommand.class);
    private static final String PARAM_NAME = "name";
    private static final String PARAM_LAST_NAME= "surnname";
    private static final String PARAM_BIRTHDAY = "birthday";
    private static final String PARAM_COUNTRY= "countries";
    private static final String PARAM_IMAGE = "image";
    private static final String DATE_PATTERN = "yyyy-MM-dd";

    private DirectorLogic logic;

    /** Constructs CreateDirectorCommand object and initialize private Logic field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as DirectorLogic object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.DirectorLogic
     */
    CreateDirectorCommand(DirectorLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the addition of new director to database after
     * validation of income data and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining and
     *                {@code Director} object creation.
     * @return director JSP address as String in any case.
     *
     * @see com.sazonov.kino.entity.Director
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String name = request.getParameter(PARAM_NAME);
        String surnName = request.getParameter(PARAM_LAST_NAME);
        String birthday = request.getParameter(PARAM_BIRTHDAY);
        String countryId = request.getParameter(PARAM_COUNTRY);

        InputStream inputStream;
        Part filePart;
        byte[] image = null;
        try {
            filePart = request.getPart(PARAM_IMAGE);
            inputStream = filePart.getInputStream();
            image = inputStream.readAllBytes();
        } catch (IOException|ServletException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        Director director = new Director();

        try {
            director.setName(name);
            director.setLastName(surnName);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
            LocalDate date = LocalDate.parse(birthday, formatter);
            director.setBirthDate(date);
            director.setPhoto(image);
            Country country = new Country();
            country.setCountryId(Integer.parseInt(countryId));
            director.setCountry(country);
        } catch (NumberFormatException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        DirectorValidator validator = new DirectorValidator();

        if (validator.isDirectorValid(director, request)) {
            if (logic.createDirector(director)) {
                List<Director> directors = logic.updateDirectorList();
                request.getServletContext().setAttribute("directorsDirectory", directors);
                request.setAttribute("newDirectorCreated", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("admin.new.director.created"));
            } else {
                request.setAttribute("directorCreationFailed", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("admin.director.creation.failed"));
            }
        } else {
            request.setAttribute("directorCreationFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.director.creation.failed"));
        }
        return PassManager.PASS_ADMIN_DIRECTOR.getPass();
    }
}
