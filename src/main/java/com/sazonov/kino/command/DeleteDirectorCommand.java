package com.sazonov.kino.command;

import com.sazonov.kino.logic.DirectorLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Class {@code DeleteDirectorCommand} is realisation of {@code Command} interface.
 * It provides the deleting of director information from database.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */

public class DeleteDirectorCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(DeleteDirectorCommand.class);
    private static final String PARAM_DIRECTOR_ID = "directorId";

    private DirectorLogic logic;

    /** Constructs DeleteDirectorCommand object and initialize private DirectorLogic field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as DirectorLogic object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.DirectorLogic
     */
    DeleteDirectorCommand(DirectorLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the deleting of director information from database
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return director JSP  address as String in any case.
     *
     * @see com.sazonov.kino.entity.Director
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        int directorId;
        String director = request.getParameter(PARAM_DIRECTOR_ID);
        try {
            directorId = Integer.parseInt(director);
        } catch (NumberFormatException exception) {
            request.setAttribute("directorDeleteFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.director.delete.failed"));
            return PassManager.PASS_ADMIN_DIRECTOR.getPass();
        }

        if (logic.deleteDirector(directorId)) {
            LOGGER.log(Level.DEBUG, "Director " + directorId + " deleted.");
            request.setAttribute("directorSuccessfullyDeleted", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.director.successfully.deleted"));
        } else {
            request.setAttribute("directorDeleteFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.director.delete.failed"));
        }
        return PassManager.PASS_ADMIN_DIRECTOR.getPass();
    }
}
