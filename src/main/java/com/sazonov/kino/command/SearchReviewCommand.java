package com.sazonov.kino.command;

import com.sazonov.kino.entity.Review;
import com.sazonov.kino.logic.ReviewLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
/**
 * Class {@code SearchReviewCommand} is realisation of {@code Command} interface.
 * It provides search of reviews between given dates.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */

public class SearchReviewCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(SearchReviewCommand.class);
    private static final String PARAM_DATE_FROM = "from";
    private static final String PARAM_DATE_TO = "to";
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private ReviewLogic logic;
    /** Constructs {@code SearchReviewCommand} object and initialize private {@code ReviewLogic} field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as {@code ReviewLogic} object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.ReviewLogic
     */
    SearchReviewCommand(ReviewLogic logic) {
        this.logic = logic;
    }
    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides search of reviews between given dates
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return management JSP address as String in any case.
     *
     * @see com.sazonov.kino.entity.Review
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("reviewSelection");
        String fromDate = request.getParameter(PARAM_DATE_FROM);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        LocalDate dateFrom = LocalDate.parse(fromDate, formatter);
        String toDate = request.getParameter(PARAM_DATE_TO);
        LocalDate dateTo = LocalDate.parse(toDate, formatter);
        if (dateTo.isBefore(dateFrom)) {
            request.setAttribute("reviewSearchFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.search.review.failed"));
            return PassManager.PASS_ADMIN_MANAGEMENT.getPass();
        }

        List<Review> reviews = logic.reviewsByQuery(dateFrom, dateTo);

        if (!reviews.isEmpty()) {
            LOGGER.log(Level.DEBUG, "Rating updated");
            request.getSession().setAttribute("reviewSelection", reviews);
            request.setAttribute("reviewsFound", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.reviews.found"));
        } else {
            request.setAttribute("reviewSearchFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.search.review.failed"));
        }
        return PassManager.PASS_ADMIN_MANAGEMENT.getPass();
    }
}
