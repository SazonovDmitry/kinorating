package com.sazonov.kino.command;

import com.sazonov.kino.logic.ReviewLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Class {@code DeleteReviewCommand} is realisation of {@code Command} interface.
 * It provides the deleting of review information from database.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */

public class DeleteReviewCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(DeleteReviewCommand.class);
    private static final String PARAM_REVIEW_ID = "reviewId";
    private ReviewLogic logic;

    /** Constructs DeleteReviewCommand object and initialize private ReviewLogic field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as ReviewLogic object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.ReviewLogic
     */
    DeleteReviewCommand(ReviewLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the deleting of review information from database
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return management JSP  address as String in any case.
     *
     * @see com.sazonov.kino.entity.Review
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        int reviewId;
        String review = request.getParameter(PARAM_REVIEW_ID);
        try {
            reviewId = Integer.parseInt(review);
        } catch (NumberFormatException exception) {
            request.setAttribute("reviewDeleteFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.review.delete.failed"));
            return PassManager.PASS_ADMIN_MANAGEMENT.getPass();
        }

        if (logic.deleteReview(reviewId)) {
            LOGGER.log(Level.DEBUG, "Review " + reviewId + " deleted");
            request.setAttribute("reviewSuccessfullyDeleted", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.review.successfully.deleted"));
        } else {
            request.setAttribute("reviewDeleteFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.review.delete.failed"));
        }
        return PassManager.PASS_ADMIN_MANAGEMENT.getPass();
    }
}
