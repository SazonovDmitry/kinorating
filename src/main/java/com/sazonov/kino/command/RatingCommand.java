package com.sazonov.kino.command;

import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.entity.User;
import com.sazonov.kino.logic.PictureLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Class {@code RatingCommand} is realisation of {@code Command} interface.
 * It provides the addition of the evaluation of the picture to database.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class RatingCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(RatingCommand.class);
    private static final String ATTR_USER = "currentUser";
    private static final String ATTR_PICTURE = "currentPicture";
    private static final String PARAM_MARK = "mark";
    private PictureLogic logic;
    /** Constructs {@code RatingCommand} object and initialize private {@code PictureLogic} field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as {@code PictureLogic} object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.PictureLogic
     */
    RatingCommand(PictureLogic logic) {
        this.logic = logic;
    }
    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the addition of the evaluation of the picture to database
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return picture JSP address as String in any case.
     *
     * @see com.sazonov.kino.entity.Review
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        try {
            int rating = Integer.parseInt(request.getParameter(PARAM_MARK));
            User user = (User)request.getSession().getAttribute(ATTR_USER);
            Picture picture = (Picture) request.getSession().getAttribute(ATTR_PICTURE);
            int userId = 0;
            int pictureId = 0;
            if (user != null && picture != null) {
                userId = user.getUserId();
                pictureId = picture.getPictureId();
            }
            if (logic.ratePicture(userId, pictureId, rating)) {
                request.setAttribute("successfulRating", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("picture.rating.success"));
            } else {
                request.setAttribute("ratingFailed", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("picture.rating.failed"));
            }
        } catch (ClassCastException|NumberFormatException exception) {
            LOGGER.log(Level.ERROR, "Can't define rating mark", exception);
            request.setAttribute("ratingFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("picture.rating.failed"));
        }

        return PassManager.PASS_PAGE_PICTURE.getPass();
    }
}
