package com.sazonov.kino.command;

import com.sazonov.kino.entity.User;
import com.sazonov.kino.logic.UserLogic;
import com.sazonov.kino.mail.MailThread;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import com.sazonov.kino.util.PropertiesReader;
import com.sazonov.kino.validator.LoginValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Properties;

/**
 * Class {@code RegisterCommand} is realisation of {@code Command} interface.
 * It provides registration of new user in system and saving it's personal data.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class RegisterCommand implements Command{
    private static final Logger LOGGER = LogManager.getLogger(RegisterCommand.class);
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_NAME = "name";
    private static final String MAIL_SUBJECT = "Successful registration";
    private UserLogic logic;
    /** Constructs {@code RegisterCommand} object and initialize private {@code UserLogic} field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as {@code UserLogic} object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.UserLogic
     */
    RegisterCommand(UserLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides registration of new user in system and saving it's personal data
     * and writes the result of operation and validation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return main JSP address as String in success registration and
     * registration JSP otherwise.
     *
     * @see com.sazonov.kino.entity.Review
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        User user;
        String login = request.getParameter(PARAM_LOGIN);
        String password = request.getParameter(PARAM_PASSWORD);
        String name = request.getParameter(PARAM_NAME);
        String email = request.getParameter(PARAM_EMAIL);
        boolean checkName = isNameSuits(name, request);
        boolean checkPassword = isPasswordSuits(password, request);
        boolean checkLogin = isLoginSuits(login, request);
        boolean checkEmail = isEmailSuits(email, request);

        if (checkName && checkPassword && checkLogin && checkEmail) {
            user = logic.createUser(login, password, email, name);
            if (user!=null) {
                PropertiesReader reader = new PropertiesReader();
                Properties mailConfiguration = reader.createMailProperties();
                String mailBody = "Congratulations! Your've successfully registered on KinoRating.com. \nYour login: " + login +
                        ", your password: " + password + ". You can reset them in your personal office. \nKinoRating command.";
                MailThread mailOperator = new MailThread(email, MAIL_SUBJECT, mailBody, mailConfiguration);
                mailOperator.start();

                HttpSession session = request.getSession();
                session.setAttribute("userName", user.getName());
                session.setAttribute("currentUser", user);
                session.setAttribute("userRole", user.getRole());

                request.setAttribute("congrOnSuccessRegister", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("congr.success.register"));
                page = PassManager.PASS_PAGE_MAIN.getPass();
            } else {
                request.setAttribute("registrationFailed", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("registration.failed"));
                page = PassManager.PASS_PAGE_REGISTRATION.getPass();
            }
        } else {
            page = PassManager.PASS_PAGE_REGISTRATION.getPass();
        }
        return page;
    }
    /*Method checks if the name suits*/
    private boolean isNameSuits(String name, HttpServletRequest request) {
        LoginValidator validator = new LoginValidator();
        if(validator.isNameValid(name)) {
            return true;
        } else {
            request.setAttribute("errorNameMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.name.message"));
            return false;
        }
    }
    /*Method checks if the password suits*/
    private boolean isPasswordSuits(String password, HttpServletRequest request) {
        LoginValidator validator = new LoginValidator();
        if(validator.isPasswordValid(password)) {
            return true;
        } else {
            request.setAttribute("errorPasswordMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.password.message"));
            return false;
        }
    }
    /*Method checks if the login suits*/
    private boolean isLoginSuits(String login, HttpServletRequest request) {
        LoginValidator validator = new LoginValidator();
        if(validator.isLoginValid(login)) {
            if (logic.isLoginFree(login)) {
                return true;
            } else {
                request.setAttribute("errorLoginMessage", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("error.login.message"));
                return false;
            }
        } else {
            request.setAttribute("errorLoginMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.login.message"));
            return false;
        }
    }
    /*Method checks if the email suits*/
    private boolean isEmailSuits(String email, HttpServletRequest request) {
        LoginValidator validator = new LoginValidator();
        if(validator.isEmailValid(email)) {
            if (logic.isEmailFree(email)) {
                return true;
            } else {
                request.setAttribute("errorEmailMessage", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("error.email.message"));
                return false;
            }
        } else {
            request.setAttribute("errorEmailMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.email.message"));
        }
        return false;
    }
}
