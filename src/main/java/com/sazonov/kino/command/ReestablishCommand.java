package com.sazonov.kino.command;

import com.sazonov.kino.entity.User;
import com.sazonov.kino.logic.UserLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import com.sazonov.kino.validator.LoginValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class {@code ReestablishCommand} is realisation of {@code Command} interface.
 * It provides changing of user personal data.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK8.0
 * @version 1.0
 */

public class ReestablishCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(ReestablishCommand.class);
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_NAME = "name";
    private UserLogic logic;
    /** Constructs {@code ReestablishCommand} object and initialize private {@code UserLogic} field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as {@code UserLogic} object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.UserLogic
     */
    ReestablishCommand(UserLogic logic) {
        this.logic = logic;
    }
    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides changing of user personal data
     * and writes the result of operation and validation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return admin JSP address as String in any case.
     *
     * @see com.sazonov.kino.entity.User
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String name = request.getParameter(PARAM_NAME);
        String password = request.getParameter(PARAM_PASSWORD);
        String login = request.getParameter(PARAM_LOGIN);
        String email = request.getParameter(PARAM_EMAIL);

        if (name.isEmpty()&&email.isEmpty()&&login.isEmpty()&&password.isEmpty()) {
            request.setAttribute("emptyUserFormFields", MessageManager.valueOf(request.getSession().
                    getAttribute("currentLocale").toString()).getMessage("user.data.empty.fields"));
        } else {
            boolean checkName = isNameSuits(name, request);
            boolean checkPassword = isPasswordSuits(password, request);
            boolean checkLogin = isLoginSuits(login, request);
            boolean checkEmail = isEmailSuits(email, request);

            if (checkName&&checkPassword&&checkLogin&&checkEmail) {
                try {
                    User user = (User)request.getSession().getAttribute("currentUser");
                    if (!email.isEmpty()) {
                        user.setEmail(email);
                    }
                    if (!name.isEmpty()) {
                        user.setName(name);
                    }

                    if (logic.changeUserData(user, login, password)) {
                        HttpSession session = request.getSession();
                        session.setAttribute("userName", name);
                        session.setAttribute("currentUser", user);
                        session.setAttribute("userRole", user.getRole());

                        request.setAttribute("newDataEstablished", MessageManager.valueOf(request.getSession().
                                getAttribute("currentLocale").toString()).getMessage("new.data.established"));
                    } else {
                        request.setAttribute("dataChangeFailed", MessageManager.valueOf(request.getSession().
                                getAttribute("currentLocale").toString()).getMessage("user.data.change.failed"));
                    }
                } catch (ClassCastException exception) {
                    LOGGER.log(Level.ERROR, exception);
                    request.setAttribute("dataChangeFailed", MessageManager.valueOf(request.getSession().
                            getAttribute("currentLocale").toString()).getMessage("user.data.change.failed"));
                }
            } else {
                request.setAttribute("dataChangeFailed", MessageManager.valueOf(request.getSession().
                        getAttribute("currentLocale").toString()).getMessage("user.data.change.failed"));
            }
        }

        return PassManager.PASS_PAGE_ADMIN.getPass();
    }

    /*Method provides the validation ot new user name*/
    private boolean isNameSuits(String name, HttpServletRequest request) {
        if (name.isEmpty()) {
            return true;
        }
        LoginValidator validator = new LoginValidator();
        if(validator.isNameValid(name)) {
            return true;
        } else {
            request.setAttribute("errorNameMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.name.message"));
            return false;
        }
    }
    /*Method provides the validation ot new user password*/
    private boolean isPasswordSuits(String password, HttpServletRequest request) {
        if (password.isEmpty()) {
            return true;
        }
        LoginValidator validator = new LoginValidator();
        if(validator.isPasswordValid(password)) {
            return true;
        } else {
            request.setAttribute("errorPasswordMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.password.message"));
            return false;
        }
    }
    /*Method provides the validation ot new user login*/
    private boolean isLoginSuits(String login, HttpServletRequest request) {
        if (login.isEmpty()) {
            return true;
        }
        LoginValidator validator = new LoginValidator();

        if(validator.isLoginValid(login)) {
            if (logic.isLoginFree(login)) {
                return true;
            } else {
                request.setAttribute("errorLoginMessage", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("error.login.message"));
            }
        } else {
            request.setAttribute("errorLoginMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.login.message"));
        }
        return false;
    }
    /*Method provides the validation ot new user email*/
    private boolean isEmailSuits(String newEmail, HttpServletRequest request) {
        if (newEmail.isEmpty()) {
            return true;
        }
        LoginValidator validator = new LoginValidator();
        if(validator.isEmailValid(newEmail)) {
            if (logic.isEmailFree(newEmail)) {
                return true;
            } else {
                request.setAttribute("errorEmailMessage", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("error.email.message"));
            }
        } else {
            request.setAttribute("errorEmailMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.email.message"));
        }
        return false;
    }
}
