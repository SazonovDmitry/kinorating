package com.sazonov.kino.command;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

/**
 * Class {@code ActionFactory} is a factory class for creating
 * command classes, depends on the income request parameter, given
 * as a command type.
 *
 * @author Sazonov
 * @see java.util.Optional
 * @since JDK8.0
 * @version 1.0
 */
public class ActionFactory {
    private static final Logger LOGGER = LogManager.getLogger(ActionFactory.class);
    private static final String DASH = "-";
    private static final String UNDERLINE = "_";

    /** Method creates command with type matches to the income parameter.
    * @param commandName income parameter, which is extracted from request and
                *used to define command type.
     * @return {@code Optional#empty()} if income parameter is null or
     * not defined.
    * Otherwise returns initialized {@code Command} object.
    * @see com.sazonov.kino.command.Command
    * @see com.sazonov.kino.controller.ProjectServlet
    */
    public static Optional<Command> defineCommand(String commandName) {
        Optional<Command> current = Optional.empty();
        if (commandName == null) {
            return current;
        }
        try {
            CommandType type = CommandType.valueOf(commandName.toUpperCase().replace(DASH,UNDERLINE));
            current = Optional.of(type.getCommand());
        } catch (IllegalArgumentException exception) {
            LOGGER.log(Level.ERROR, "Unknown command.", exception);
        }
        return current;
    }
}
