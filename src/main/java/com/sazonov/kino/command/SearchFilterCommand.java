package com.sazonov.kino.command;

import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.logic.PictureLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import com.sazonov.kino.validator.PictureValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
/**
 * Class {@code SearchFilterCommand} is realisation of {@code Command} interface.
 * It provides search of pictures according to the filtered search query.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class SearchFilterCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(SearchFilterCommand.class);
    private static final String PARAM_DATE_FROM = "from";
    private static final String PARAM_DATE_TO = "to";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_AGE = "age";
    private static final String PARAM_COUNTRIES = "countries";
    private static final String PARAM_GENRES = "genres";
    private static final String PARAM_DIRECTORS = "directors";
    private static final String REMOVE_ATTRIBUTE = "picturesByQuery";
    private PictureLogic logic;
    /** Constructs {@code SearchCommand} object and initialize private {@code PictureLogic} field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as {@code PictureLogic} object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.PictureLogic
     */
    SearchFilterCommand(PictureLogic logic) {
        this.logic = logic;
    }
    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides search of pictures according to the filtered search query
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return main JSP address as String in any case.
     *
     * @see com.sazonov.kino.entity.Review
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute(REMOVE_ATTRIBUTE);
        String fromDate = request.getParameter(PARAM_DATE_FROM);
        String toDate = request.getParameter(PARAM_DATE_TO);
        String type = request.getParameter(PARAM_TYPE);
        String ageLimit = request.getParameter(PARAM_AGE);
        String[] countries = request.getParameterValues(PARAM_COUNTRIES);
        String[] genres = request.getParameterValues(PARAM_GENRES);
        String[] directors = request.getParameterValues(PARAM_DIRECTORS);

        PictureValidator validator = new PictureValidator();
        if (validator.isEmptyQuery(fromDate, toDate, type, ageLimit, countries, genres, directors)) {
            request.setAttribute("emptyQueryResult", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("no.pictures.found"));
            request.setAttribute("numberPicturesByQuery", null);
        } else {
            List<Picture> pictures = logic.searchPicturesByQuery(fromDate, toDate, type, ageLimit, countries, genres, directors);
            if (pictures.isEmpty()) {
                request.setAttribute("emptyQueryResult", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("no.pictures.found"));
                request.setAttribute("numberPicturesByQuery", null);
            } else {
                request.getSession().setAttribute("picturesByQuery", pictures);
                request.setAttribute("numberPicturesByQuery", pictures.size());
            }
        }

        return PassManager.PASS_PAGE_MAIN.getPass();
    }
}
