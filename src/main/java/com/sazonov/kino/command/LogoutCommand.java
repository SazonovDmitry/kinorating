package com.sazonov.kino.command;

import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class {@code LogoutCommand} is realisation of {@code Command} interface.
 * It provides the invalidation of current session.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */

public class LogoutCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(LogoutCommand.class);

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the invalidation of current session.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return index JSP address as String in any case.
     *
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
        return PassManager.PASS_PAGE_INDEX.getPass();
    }
}
