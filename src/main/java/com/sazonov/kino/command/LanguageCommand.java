package com.sazonov.kino.command;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class {@code LanguageCommand} is realisation of {@code Command} interface.
 * It provides changing of current session locale.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class LanguageCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(LanguageCommand.class);
    private static final String PARAM_LANGUAGE = "lang";
    private static final String PARAM_PAGE = "from";
    private static final String LOCALE = "currentLocale";

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides  changing of current session locale.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return request JSP address as String.
     *
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String language = request.getParameter(PARAM_LANGUAGE);
        String page = request.getParameter(PARAM_PAGE);

        HttpSession session = request.getSession();

        if (session!=null) {
            session.setAttribute(LOCALE, language);
            LOGGER.log(Level.DEBUG, session.getAttribute(LOCALE));
        }
        return page;
    }
}
