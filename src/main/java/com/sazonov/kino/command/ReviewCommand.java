package com.sazonov.kino.command;

import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.entity.User;
import com.sazonov.kino.logic.ReviewLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * Class {@code ReviewCommand} is realisation of {@code Command} interface.
 * It provides the addition of new picture review to database.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class ReviewCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(ReviewCommand.class);
    private static final String PARAM_CONTENT = "reviewContent";
    private static final String ATTR_USER = "currentUser";
    private static final String ATTR_PICTURE = "currentPicture";
    private ReviewLogic logic;

    /** Constructs {@code ReviewCommand} object and initialize private {@code ReviewLogic} field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as {@code ReviewLogic} object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.ReviewLogic
     */
    ReviewCommand(ReviewLogic logic) {
        this.logic = logic;
    }
    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the addition of new picture review to database
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return picture JSP address as String in any case.
     *
     * @see com.sazonov.kino.entity.Review
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */

    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();

        try {
            User user = (User)session.getAttribute(ATTR_USER);
            Picture picture = (Picture)session.getAttribute(ATTR_PICTURE);
            int userId = 0;
            int pictureId = 0;
            if (user != null && picture != null) {
                userId = user.getUserId();
                pictureId = picture.getPictureId();
            }
            String reviewContent = request.getParameter(PARAM_CONTENT);
            if (reviewContent.isEmpty()) {
                request.setAttribute("reviewEmpty",  MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("review.empty.message"));
            }
            if (logic.addReview(reviewContent, userId, pictureId)) {
                request.setAttribute("reviewAdded",  MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("review.added.message"));
            } else {
                request.setAttribute("reviewFailed",  MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("review.failed.message"));
            }

        } catch (ClassCastException exception) {
            request.setAttribute("reviewFailed",  MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("review.failed.message"));
            LOGGER.log(Level.ERROR, exception);
        }
        return PassManager.PASS_PAGE_PICTURE.getPass();
    }
}
