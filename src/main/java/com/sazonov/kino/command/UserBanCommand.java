package com.sazonov.kino.command;

import com.sazonov.kino.logic.UserLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
/**
 * Class {@code UserBanCommand} is realisation of {@code Command} interface.
 * It provides user banning by admin.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class UserBanCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UserBanCommand.class);
    private static final String PARAM_BAN_DATE = "banDate";
    private static final String PARAM_USER_ID = "userId";
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private UserLogic logic;
    /** Constructs {@code UserBanCommand} object and initialize private {@code UserLogic} field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as {@code UserLogic} object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.UserLogic
     */
    UserBanCommand(UserLogic logic) {
        this.logic = logic;
    }
    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides user banning by admin
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return management JSP address as String in any case.
     *
     * @see com.sazonov.kino.entity.User
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {

        String ban = request.getParameter(PARAM_BAN_DATE);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        LocalDate banDate = LocalDate.parse(ban, formatter);
        String user = request.getParameter(PARAM_USER_ID);
        int userId;

        LocalDate current = LocalDate.now();
        if (banDate.isBefore(current)) {
            request.setAttribute("userBanFailed", MessageManager.valueOf(request.getSession().getAttribute("currentLocale")
                    .toString()).getMessage("admin.user.ban.failed"));
            return PassManager.PASS_ADMIN_MANAGEMENT.getPass();
        }
        try {
            userId = Integer.parseInt(user);
        } catch (NumberFormatException exception) {
            request.setAttribute("userBanFailed", MessageManager.valueOf(request.getSession().getAttribute("currentLocale")
                    .toString()).getMessage("admin.user.ban.failed"));
            return PassManager.PASS_ADMIN_MANAGEMENT.getPass();
        }

        if (logic.banUser(userId, banDate)) {
            LOGGER.log(Level.DEBUG, "User banned");
            request.setAttribute("userSuccessfullyBanned", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.user.ban.success"));
        } else {
            request.setAttribute("userBanFailed", MessageManager.valueOf(request.getSession().getAttribute("currentLocale")
                    .toString()).getMessage("admin.user.ban.failed"));
        }

        return PassManager.PASS_ADMIN_MANAGEMENT.getPass();
    }
}
