package com.sazonov.kino.command;

import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.entity.User;
import com.sazonov.kino.logic.PictureLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Class {@code PictureCommand} is realisation of {@code Command} interface.
 * It provides {@code Picture} object building by picture id.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class PictureCommand implements Command{
    private static final Logger LOGGER = LogManager.getLogger(PictureCommand.class);
    private static final String PARAM_ID = "pictureId";
    private PictureLogic logic;

    /** Constructs PictureCommand object and initialize private PictureLogic field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as PictureLogic object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.PictureLogic
     */
    PictureCommand(PictureLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides {@code Picture} object building by picture id.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return picture JSP address as String in any case.
     *
     * @see com.sazonov.kino.entity.Picture
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        LOGGER.log(Level.DEBUG, request.getParameter(PARAM_ID));
        String pictureId = request.getParameter(PARAM_ID);
        Picture picture = logic.searchPicturesById(pictureId);

        if (picture!=null) {
            LOGGER.log(Level.DEBUG, picture);
            request.getSession().setAttribute("currentPicture", picture);
        } else {
            request.setAttribute("pictureNotFound", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("picture.not.found"));
        }

        return PassManager.PASS_PAGE_PICTURE.getPass();
    }
}
