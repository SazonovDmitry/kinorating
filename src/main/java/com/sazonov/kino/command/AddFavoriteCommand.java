package com.sazonov.kino.command;

import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.entity.User;
import com.sazonov.kino.logic.UserLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class {@code AddFavoriteCommand} is realisation of {@code Command} interface.
 * It provides the addition of the specified picture to the user's personal list.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */

public class AddFavoriteCommand implements Command {
    /*Class logger initialization.*/
    private static final Logger LOGGER = LogManager.getLogger(AddFavoriteCommand.class);
    /*Constant, which defines session attribute which contains chosen picture.*/
    private static final String ATTR_PICTURE = "currentPicture";
    /*Constant, which defines session attribute which contains current user data.*/
    private static final String ATTR_USER = "currentUser";
    /*Variable of type UserLogic, which contains reference to Logic object.*/
    private UserLogic logic;

    /** Constructs AddFavoriteCommand and initialize private Logic variable.
     * @param logic income parameter, which is passed to the constructor during object initialization.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.UserLogic
     */
    AddFavoriteCommand(UserLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. Method provides the addition of the specified picture
     * to the user's personal list and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return picture JSP address as String in any case.
     *
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Picture currentPicture = null;
        User currentUser = null;

        try {
            currentPicture = (Picture)session.getAttribute(ATTR_PICTURE);
            currentUser = (User)session.getAttribute(ATTR_USER);
        } catch (ClassCastException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        if (currentPicture != null && currentUser != null) {
            if (logic.addFavoritePicture(currentUser.getUserId(), currentPicture.getPictureId())) {
                LOGGER.log(Level.DEBUG, "Picture " + currentPicture.getName() +
                        " added to user " + currentUser.getUserId() + " list.");
                request.setAttribute("successfulFavorite", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("admin.success.favorite"));
                currentUser.getFavorite().add(currentPicture);
            } else {
                request.setAttribute("favoriteFailed", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("admin.failed.favorite"));
            }
        } else {
            request.setAttribute("favoriteFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.failed.favorite"));
        }
        return PassManager.PASS_PAGE_PICTURE.getPass();
    }
}
