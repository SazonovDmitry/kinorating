package com.sazonov.kino.command;

import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.logic.PictureLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Class {@code TopPicturesCommand} is realisation of {@code Command} interface.
 * It provides creation of top-20 pictures list.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK8.0
 * @version 1.0
 */
public class TopPicturesCommand implements Command{
    private static final Logger LOGGER = LogManager.getLogger(TopPicturesCommand.class);
    private PictureLogic logic;
    /** Constructs {@code TopPicturesCommand} object and initialize private {@code PictureLogic} field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as {@code PictureLogic} object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.PictureLogic
     */
    TopPicturesCommand(PictureLogic logic) {
        this.logic = logic;
    }
    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides creation of top-20 pictures list
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return main JSP address as String in any case.
     *
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {

        List<Picture> topPictures = logic.searchTopPictures();
        if (!topPictures.isEmpty()) {
            request.getSession().setAttribute("picturesByQuery", topPictures);
            request.setAttribute("numberPicturesByQuery", topPictures.size());
        } else {
            request.setAttribute("emptyQueryResult", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("no.pictures.found"));
            request.setAttribute("numberPicturesByQuery", null);
        }
        return PassManager.PASS_PAGE_MAIN.getPass();
    }
}
