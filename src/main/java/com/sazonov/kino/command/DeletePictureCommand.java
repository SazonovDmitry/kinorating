package com.sazonov.kino.command;

import com.sazonov.kino.logic.PictureLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import com.sazonov.kino.validator.PictureValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class {@code DeletePictureCommand} is realisation of {@code Command} interface.
 * It provides the deleting of picture information from database.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */

public class DeletePictureCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(DeletePictureCommand.class);
    private static final String PARAM_PICTURE_ID = "pictureId";

    private PictureLogic logic;

    /** Constructs DeletePictureCommand object and initialize private PictureLogic field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as PictureLogic object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.PictureLogic
     */
    DeletePictureCommand(PictureLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the deleting of picture information from database
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return admin JSP  address as String in any case.
     *
     * @see com.sazonov.kino.entity.Picture
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("cratedPictureId");
        session.removeAttribute("newPictureIdFound");

        int pictureId;
        String picture = request.getParameter(PARAM_PICTURE_ID);
        try {
            pictureId = Integer.parseInt(picture);
        } catch (NumberFormatException exception) {
            request.setAttribute("pictureDeleteFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.picture.delete.failed"));
            return PassManager.PASS_PAGE_ADMIN.getPass();
        }

        if (logic.deletePicture(pictureId)) {
            LOGGER.log(Level.DEBUG, "Picture " + pictureId + " deleted");
            request.setAttribute("pictureSuccessfullyDeleted", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.picture.successfully.deleted"));
        } else {
            request.setAttribute("pictureDeleteFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.picture.delete.failed"));
        }
        return PassManager.PASS_PAGE_ADMIN.getPass();
    }
}
