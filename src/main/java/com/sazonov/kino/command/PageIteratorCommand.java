package com.sazonov.kino.command;

import com.sazonov.kino.logic.PictureLogic;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Class {@code PageIteratorCommand} is realisation of {@code Command} interface.
 * It provides multi-page reading of large lists.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */

public class PageIteratorCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(PageIteratorCommand.class);
    private static final String PARAM_PAGE_COUNT = "pageCount";
    private static final String PARAM_PAGE_ADDRESS = "address";
    private static final String MAIN_PAGE = "main";
    private static final String PICTURE_PAGE = "picture";
    private static final int STEP = 11;
    private PictureLogic logic;

    /** Constructs DeleteDirectorCommand object and initialize private PictureLogic field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as PictureLogic object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.PictureLogic
     */
    PageIteratorCommand(PictureLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides multi-page reading of large lists.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return main or picture JSP address as String, depends of the request page.
     *
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = request.getParameter(PARAM_PAGE_COUNT);
        String address = request.getParameter(PARAM_PAGE_ADDRESS);

        int min = logic.extractMinPage(page);
        int max = min + STEP;
        request.getSession().setAttribute("min", min);
        request.getSession().setAttribute("max", max);
        LOGGER.log(Level.DEBUG, "min " + min + ",max " + max);

        try {
            switch (address) {
                case MAIN_PAGE:
                    request.getSession().setAttribute("min", min);
                    request.getSession().setAttribute("max", max);
                    return PassManager.PASS_PAGE_MAIN.getPass();
                case PICTURE_PAGE:
                    request.setAttribute("min", min);
                    request.setAttribute("max", max);
                    return PassManager.PASS_PAGE_PICTURE.getPass();
                default:
                    return PassManager.PASS_PAGE_MAIN.getPass();
            }
        } catch (IllegalArgumentException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return PassManager.PASS_PAGE_MAIN.getPass();
    }
}
