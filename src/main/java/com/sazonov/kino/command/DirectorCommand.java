package com.sazonov.kino.command;

import com.sazonov.kino.entity.Director;
import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.logic.DirectorLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class {@code DirectorCommand} is realisation of {@code Command} interface.
 * It provides the extracting of director information from database and building Director object.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */

public class DirectorCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(DirectorCommand.class);
    private static final String PARAM_DIRECTOR = "directorId";
    private static final String ATTR_PICTURE= "currentPicture";
    private DirectorLogic logic;

    /** Constructs DeleteDirectorCommand object and initialize private DirectorLogic field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as DirectorLogic object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.DirectorLogic
     */
    DirectorCommand(DirectorLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the extracting of director information from database
     * and building Director object and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return picture JSP  address as String in any case.
     *
     * @see com.sazonov.kino.entity.Director
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        try {
            int directorId = Integer.parseInt(request.getParameter(PARAM_DIRECTOR));
            Picture picture = (Picture)session.getAttribute(ATTR_PICTURE);
            Director currentDirector = logic.extractDirectorFromPicture(picture, directorId);

            if (currentDirector != null) {
                session.setAttribute("currentDirector", currentDirector);
            }
        } catch (ClassCastException|NumberFormatException exception) {
            request.setAttribute("directorNotFound",  MessageManager.valueOf(request.getSession().getAttribute("currentLocale")
                    .toString()).getMessage("picture.director.failed"));
            LOGGER.log(Level.ERROR, exception);
        }
        return PassManager.PASS_PAGE_PICTURE.getPass();
    }
}
