package com.sazonov.kino.command;

import com.sazonov.kino.entity.UserStatus;
import com.sazonov.kino.logic.UserLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
/**
 * Class {@code UserStatusCommand} is realisation of {@code Command} interface.
 * It provides user status setting by admin.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class UserStatusCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(UserStatusCommand.class);
    private static final String PARAM_STATUS = "status";
    private static final String PARAM_USER_ID = "userId";
    private UserLogic logic;
    /** Constructs {@code UserStatusCommand} object and initialize private {@code UserLogic} field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as {@code UserLogic} object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.UserLogic
     */
    UserStatusCommand(UserLogic logic) {
        this.logic = logic;
    }
    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides user status setting by admin
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return management JSP address as String in any case.
     *
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {

        String status = request.getParameter(PARAM_STATUS);
        String user = request.getParameter(PARAM_USER_ID);
        int userId;
        UserStatus userStatus;
        try {
            userStatus = UserStatus.valueOf(status.toUpperCase().replace("-","_"));
            userId = Integer.parseInt(user);
        } catch (IllegalArgumentException exception) {
            request.setAttribute("userStatusNotChanged", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.user.status.not.changed"));
            return PassManager.PASS_ADMIN_MANAGEMENT.getPass();
        }

        if (logic.setUserStatus(userId, userStatus)) {
            LOGGER.log(Level.DEBUG, "User status changed");
            request.setAttribute("userStatusChanged", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.user.status.changed"));
        } else {
            request.setAttribute("userStatusNotChanged", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.user.status.not.changed"));
        }

        return PassManager.PASS_ADMIN_MANAGEMENT.getPass();
    }
}
