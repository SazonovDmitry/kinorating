package com.sazonov.kino.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Interface {@code Command} is a base command group interface. Obliges classes
 * to implement a method contract for processing an incoming request with an address
 * return for a further transition.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public interface Command {
    /** Processes income request parameter and returns JSP page address.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return JSP page address as String.
     *
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    String execute(HttpServletRequest request);
}
