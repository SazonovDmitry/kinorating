package com.sazonov.kino.command;

import com.sazonov.kino.entity.*;
import com.sazonov.kino.logic.PictureLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import com.sazonov.kino.validator.PictureValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code CreatePictureCommand} is realisation of {@code Command} interface.
 * It provides the addition of new picture to database.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK9.0
 * @version 1.0
 */

public class CreatePictureCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(CreatePictureCommand.class);
    private static final String PARAM_NAME = "name";
    private static final String PARAM_TYPE = "type";
    private static final String PARAM_PARTS = "parts";
    private static final String PARAM_SLOGAN = "slogan";
    private static final String PARAM_BUDGET = "budget";
    private static final String PARAM_RATING = "rating";
    private static final String PARAM_YEAR = "year";
    private static final String PARAM_AGE_LIMIT = "age";
    private static final String PARAM_PREMIERE_DATE = "premiere";
    private static final String PARAM_DURATION = "duration";
    private static final String PARAM_IMAGE = "image";
    private static final String PARAM_COUNTRIES = "countries";
    private static final String PARAM_GENRES = "genres";
    private static final String PARAM_DIRECTORS = "directors";
    private static final String DATE_PATTERN = "yyyy-MM-dd";
    private PictureLogic logic;

    /** Constructs CreatePictureCommand object and initialize private Logic field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as PictureLogic object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.PictureLogic
     */
    CreatePictureCommand(PictureLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the addition of new picture to database after
     * validation of income data and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining and
     *                {@code Picture} object creation.
     * @return admin JSP address as String in any case.
     *
     * @see com.sazonov.kino.entity.Picture
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        PictureValidator validator = new PictureValidator();
        session.removeAttribute("cratedPictureId");
        session.removeAttribute("newPictureIdFound");

        String title = request.getParameter(PARAM_NAME);
        String type = request.getParameter(PARAM_TYPE);
        String parts = request.getParameter(PARAM_PARTS);
        String slogan = request.getParameter(PARAM_SLOGAN);
        String budget = request.getParameter(PARAM_BUDGET);
        String rating = request.getParameter(PARAM_RATING);
        String year = request.getParameter(PARAM_YEAR);
        String ageLimit = request.getParameter(PARAM_AGE_LIMIT);
        String premiereDate = request.getParameter(PARAM_PREMIERE_DATE);
        String duration = request.getParameter(PARAM_DURATION);
        List<Integer> countriesId = parseId(request.getParameterValues(PARAM_COUNTRIES));
        List<Integer> genresId = parseId(request.getParameterValues(PARAM_GENRES));
        List<Integer> directorsId = parseId(request.getParameterValues(PARAM_DIRECTORS));
        InputStream inputStream;
        Part filePart;
        byte[] image = null;
        try {
            filePart = request.getPart(PARAM_IMAGE);
            LOGGER.log(Level.DEBUG, filePart.getContentType());
            inputStream = filePart.getInputStream();
            image = inputStream.readAllBytes();
        } catch (IOException|ServletException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        Picture picture = new Picture();
        try {
            picture.setName(title);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
            LocalDate date = LocalDate.parse(premiereDate, formatter);
            picture.setPremiereDate(date);
            picture.setPictureType(PictureType.valueOf(type.toUpperCase()));
            picture.setImage(image);
            picture.setSlogan(slogan);
            picture.setRating(Double.parseDouble(rating));
            picture.setAgeLimit(Byte.parseByte(ageLimit));
            picture.setYear(Integer.parseInt(year));
            picture.setBudget(Long.parseLong(budget));
            picture.setDuration(Integer.parseInt(duration));
            picture.setParts(Integer.parseInt(parts));
        } catch (NumberFormatException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        if (countriesId != null && genresId != null && directorsId != null && validator.isPictureValid(picture, request)) {
            int newPictureId = logic.createNewPicture(picture);
            boolean addCountries = logic.writePictureCountries(countriesId, newPictureId);
            boolean addDirectors = logic.writePictureDirectors(directorsId, newPictureId);
            boolean addGenres = logic.writePictureGenres(genresId, newPictureId);
            if (newPictureId!=0 && addCountries && addDirectors && addGenres) {
                LOGGER.log(Level.DEBUG, newPictureId + "was set");
                session.setAttribute("createdPictureId", String.valueOf(newPictureId));
                session.setAttribute("newPictureIdFound", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("admin.new.picture.id"));
            } else {
                request.setAttribute("pictureCreationFailed", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("admin.picture.creation.failed"));
            }
        } else {
            request.setAttribute("pictureCreationFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.picture.creation.failed"));
        }
        return PassManager.PASS_PAGE_ADMIN.getPass();
    }

    /*Method build list of Integer id values from String array*/
    private List<Integer> parseId(String[] component) {
        List<Integer> componentId = null;
        if (component != null) {
            componentId = new ArrayList<>();
            int i = 0;
            try {
                for (i = 0; i < component.length; i++) {
                    int id = Integer.parseInt(component[i]);
                    componentId.add(id);
                }
            } catch (NumberFormatException exception) {
                LOGGER.log(Level.DEBUG, "Incorrect id " + component[i]);
            }
        }
        return componentId;
    }
}
