package com.sazonov.kino.command;

import com.sazonov.kino.logic.DirectorLogic;
import com.sazonov.kino.logic.PictureLogic;
import com.sazonov.kino.logic.ReviewLogic;
import com.sazonov.kino.logic.UserLogic;

/**
 * Enum {@code CommandType} is used for defining of command type.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */

public enum CommandType {
    LOGIN(new LoginCommand(new UserLogic())),
    LOGOUT(new LogoutCommand()),
    SEARCH(new SearchCommand(new PictureLogic())),
    LANGUAGE(new LanguageCommand()),
    REGISTER(new RegisterCommand(new UserLogic())),
    PICTURE(new PictureCommand(new PictureLogic())),
    TOP_PICTURES(new TopPicturesCommand(new PictureLogic())),
    REVIEW(new ReviewCommand(new ReviewLogic())),
    DIRECTOR(new DirectorCommand(new DirectorLogic())),
    RATING(new RatingCommand(new PictureLogic())),
    REESTABLISH(new ReestablishCommand(new UserLogic())),
    CREATE_PICTURE(new CreatePictureCommand(new PictureLogic())),
    CREATE_DIRECTOR(new CreateDirectorCommand(new DirectorLogic())),
    SEARCH_REVIEW(new SearchReviewCommand(new ReviewLogic())),
    DELETE_REVIEW(new DeleteReviewCommand(new ReviewLogic())),
    DELETE_PICTURE(new DeletePictureCommand(new PictureLogic())),
    DELETE_DIRECTOR(new DeleteDirectorCommand(new DirectorLogic())),
    BAN_USER(new UserBanCommand(new UserLogic())),
    UNBAN_USER(new UserUnbanCommand(new UserLogic())),
    SET_STATUS(new UserStatusCommand(new UserLogic())),
    SEARCH_FILTER(new SearchFilterCommand(new PictureLogic())),
    ADD_FAVORITE(new AddFavoriteCommand(new UserLogic())),
    REMOVE_FAVORITE(new RemoveFavoriteCommand(new UserLogic())),
    PASSWORD_RECOVERY(new PasswordRecoveryCommand(new UserLogic())),
    PAGE_ITERATOR(new PageIteratorCommand(new PictureLogic()));
    /**Command field which defined while initialization.*/
    private Command command;

    /** Constructs CommandType and initialize private command variable.
     * @param command income parameter, which is passed to the constructor during object initialization.
     *
     * @see com.sazonov.kino.command.Command
     */
    CommandType(Command command) {this.command = command;}

    /** Standard getter for private command object.
     * @return private {@code Command} object.
     *
     * @see com.sazonov.kino.command.Command
     */
    public Command getCommand() {return this.command;}
}
