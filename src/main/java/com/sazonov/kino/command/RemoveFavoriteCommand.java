package com.sazonov.kino.command;

import com.sazonov.kino.entity.User;
import com.sazonov.kino.logic.UserLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * Class {@code RemoveFavoriteCommand} is realisation of {@code Command} interface.
 * It provides deleting of picture from user's favorite list.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class RemoveFavoriteCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(RemoveFavoriteCommand.class);
    private static final String PARAM_PICTURE = "pictureId";
    private static final String ATTR_USER = "currentUser";
    private UserLogic logic;
    /** Constructs {@code RemoveFavoriteCommand} object and initialize private {@code UserLogic} field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as {@code UserLogic} object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.UserLogic
     */
    RemoveFavoriteCommand(UserLogic logic) {
        this.logic = logic;
    }
    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides  deleting of picture from user's favorite list
     * and writes the result of operation to the request attributes.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return admin JSP address as String in any case.
     *
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        HttpSession session = request.getSession();
        int pictureId = 0;
        User currentUser = null;

        try {
            String id = request.getParameter(PARAM_PICTURE);
            pictureId = Integer.parseInt(id);
            currentUser = (User)session.getAttribute(ATTR_USER);
        } catch (ClassCastException|NumberFormatException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        if (pictureId != 0 && currentUser != null) {
            if (logic.removeFavoritePicture(currentUser, pictureId)) {
                LOGGER.log(Level.DEBUG, "Picture " + pictureId + " removed from user " +
                        currentUser.getUserId() + " list.");
            } else {
                request.setAttribute("deleteFavoriteFailed", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("admin.failed.favorite"));
            }
        } else {
            request.setAttribute("deleteFavoriteFailed", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("admin.failed.favorite"));
        }
        return PassManager.PASS_PAGE_ADMIN.getPass();
    }
}
