package com.sazonov.kino.command;

import com.sazonov.kino.manager.PassManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Class {@code EmptyCommand} is realisation of {@code Command} interface.
 * It provides the transition to main JSP if command type was not defined.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class EmptyCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(EmptyCommand.class.getName());

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the transition to main JSP if command type was not defined.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return main JSP address as String in any case.
     *
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        return PassManager.PASS_PAGE_MAIN.getPass();
    }
}
