package com.sazonov.kino.command;

import com.sazonov.kino.entity.User;
import com.sazonov.kino.logic.UserLogic;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import com.sazonov.kino.validator.LoginValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Class {@code LoginCommand} is realisation of {@code Command} interface.
 * It provides user authentication in system.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */

public class LoginCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(LoginCommand.class);
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private UserLogic logic;

    /** Constructs LoginCommand object and initialize private UserLogic field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as UserLogic object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.UserLogic
     */

    LoginCommand(UserLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides the user authentication in system.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return main JSP address as String if correct login and password,
     * otherwise returns login JSP address.
     *
     * @see com.sazonov.kino.entity.User
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        String loginValue = request.getParameter(PARAM_LOGIN);
        String passwordValue = request.getParameter(PARAM_PASSWORD);
        User user;
        HttpSession session = request.getSession();

        LoginValidator validator = new LoginValidator();
        if (validator.isLoginValid(loginValue) && validator.isPasswordValid(passwordValue)) {
            user = logic.checkAndBuildUser(loginValue, passwordValue);
            if (user!=null) {
                LOGGER.log(Level.DEBUG, user.toString());
                if (session != null) {
                    session.invalidate();
                }
                session = request.getSession(true);
                session.setAttribute("userName", user.getName());
                session.setAttribute("currentUser", user);
                session.setAttribute("userRole", user.getRole());
                session.setAttribute("currentLocale", "RU");
                page = PassManager.PASS_PAGE_MAIN.getPass();
            } else {
                request.setAttribute("errorLoginPassMessage", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("error.login.pass.message"));
                page = PassManager.PASS_PAGE_LOGIN.getPass();
            }
        } else {
            request.setAttribute("errorLoginPassMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.login.pass.message"));
            page = PassManager.PASS_PAGE_LOGIN.getPass();
        }
        return page;
    }
}
