package com.sazonov.kino.command;

import com.sazonov.kino.logic.UserLogic;
import com.sazonov.kino.mail.MailThread;
import com.sazonov.kino.manager.MessageManager;
import com.sazonov.kino.manager.PassManager;
import com.sazonov.kino.util.LoginPassGenerator;
import com.sazonov.kino.util.PropertiesReader;
import com.sazonov.kino.validator.LoginValidator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Properties;

/**
 * Class {@code PasswordRecoveryCommand} is realisation of {@code Command} interface.
 * It provides changing of current user login/password, defined with user email address, and
 * sending notification about the result of this operation.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class PasswordRecoveryCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(PasswordRecoveryCommand.class);
    private static final String PARAM_EMAIL = "email";
    private static final String MAIL_SUBJECT = "Password recovery";
    private UserLogic logic;

    /** Constructs PasswordRecoveryCommand object and initialize private UserLogic field.
     * @param logic income parameter, which is passed to the constructor during object initialization
     *              as UserLogic object.
     *
     * @see com.sazonov.kino.command.Command
     * @see com.sazonov.kino.logic.UserLogic
     */
    PasswordRecoveryCommand(UserLogic logic) {
        this.logic = logic;
    }

    /** Override {@code Command} interface method, which processes income request parameter and
     * returns JSP page address. It provides changing of current user login/password, defined with user email address, and
     * sending notification about the result of this operation.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @return mail JSP address as String in any case.
     *
     * @see com.sazonov.kino.controller.ProjectServlet
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        PropertiesReader reader = new PropertiesReader();
        Properties mailConfiguration = reader.createMailProperties();

        String email = request.getParameter(PARAM_EMAIL);

        LoginValidator validator = new LoginValidator();
        if (validator.isEmailValid(email)) {
            if (!logic.isEmailFree(email)) {
                String newLogin;
                String newPassword;
                do {
                    newLogin = LoginPassGenerator.buildLogin();
                    newPassword = LoginPassGenerator.buildPassword();
                } while (!logic.isLoginFree(newLogin));

                if (logic.setNewLoginPass(newLogin, newPassword, email)) {
                    String mailBody = "Your new login: " + newLogin + ", new password: " + newPassword + ". Please, " +
                            "after authentication change your login/password in personal office. KinoRating command.";

                    MailThread mailOperator = new MailThread(email, MAIL_SUBJECT, mailBody, mailConfiguration);
                    mailOperator.start();
                    request.setAttribute("mailSuccessfullySent", MessageManager.valueOf(request.getSession()
                            .getAttribute("currentLocale").toString()).getMessage("mail.check.email"));
                } else {
                    request.setAttribute("renameError", MessageManager.valueOf(request.getSession()
                            .getAttribute("currentLocale").toString()).getMessage("mail.error.rename"));
                }
            } else {
                request.setAttribute("unknownEmail", MessageManager.valueOf(request.getSession()
                        .getAttribute("currentLocale").toString()).getMessage("mail.unknown.email"));
            }
        } else {
            request.setAttribute("unknownEmail", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("mail.unknown.email"));
        }

        return PassManager.PASS_PAGE_MAIL.getPass();
    }
}
