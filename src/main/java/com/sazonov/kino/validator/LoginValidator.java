package com.sazonov.kino.validator;

/**
 * Class {@code LoginValidator} validates user registration data.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public class LoginValidator {
    private static final String LOGIN_DEFINER = "^\\w{5,30}$";
    private static final String PASSWORD_DEFINER = "^.{5,40}$";
    private static final String EMAIL_DEFINER = "^([\\w|\\.]{6,})@(\\w+\\.)(\\w{2,4})$";
    private static final String NAME_DEFINER = "^([a-zA-Zа-яА-ЯіўІЎ]+(-?)){2,40}$";

    /** Validate income login value.
     * @param login user login value.
     * @return true if login valid, false otherwise.
     */
    public boolean isLoginValid(String login) {
        return (login != null&& !login.isEmpty()&&login.matches(LOGIN_DEFINER));
    }

    /** Validate income password value.
     * @param password user password value.
     * @return true if password valid, false otherwise.
     */
    public boolean isPasswordValid(String password) {
        return password != null&& !password.isEmpty()&&password.matches(PASSWORD_DEFINER);
    }

    /** Validate income name value.
     * @param name user name value.
     * @return true if name valid, false otherwise.
     */
    public boolean isNameValid(String name) {
        return (name != null&& !name.isEmpty()&&name.matches(NAME_DEFINER));
    }

    /** Validate income email value.
     * @param email user email value.
     * @return true if email valid, false otherwise.
     */
    public boolean isEmailValid(String email) {
        return (email != null&& !email.isEmpty()&&email.matches(EMAIL_DEFINER));
    }
}
