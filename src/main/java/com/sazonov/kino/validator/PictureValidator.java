package com.sazonov.kino.validator;

import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

/**
 * Class {@code PictureValidator} validates {@code Picture} object.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Picture
 * @since JDK7.0
 * @version 1.0
 */
public class PictureValidator {
    private static final String PICTURE_NAME_PATTERN = "^([\\wа-яА-ЯіўІЎ]+[-\\+\\s]?){1,130}$";
    private static final String SLOGAN_PATTERN = "^([\\wа-яА-ЯіўІЎ]+[-\\+\\s]?){1,300}";
    private static final int FIRST_PICTURE_ISSUE = 1885;
    private static final int PLANNED_FOR_PRODUCTION = 2;
    private static final int MAX_PICTURE_SIZE = 614_400;
    private static final int MAX_SLOGAN_SIZE = 300;
    private static final double MAX_RATING = 10;
    private static final double MIN_RATING = 0;
    private static final int MAX_DURATION = 15000;
    private static final String DEFAULT_AGE = "11";

    /** Validate income {@code Picture} object.
     * @param picture picture object for validation.
     * @param request contains result of validation.
     * @return true if object valid, false otherwise.
     *
     * @see javax.servlet.http.HttpServletRequest
     */
    public boolean isPictureValid(Picture picture, HttpServletRequest request) {
        boolean result = true;
        if (picture.getName() == null || !picture.getName().matches(PICTURE_NAME_PATTERN)) {
            request.setAttribute("errorPictureNameMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.picture.name.message"));
            result = false;
        }

        LocalDate currentDate = LocalDate.now();
        int year = currentDate.getYear() + PLANNED_FOR_PRODUCTION;
        if (picture.getYear() < FIRST_PICTURE_ISSUE || picture.getYear() > year) {
            request.setAttribute("errorPictureYearMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.picture.year.message"));
            result = false;
        }

        if (picture.getPremiereDate() == null || picture.getPremiereDate().getYear() < FIRST_PICTURE_ISSUE ||
                picture.getPremiereDate().isAfter(currentDate.plusYears(PLANNED_FOR_PRODUCTION))) {
            request.setAttribute("errorPicturePremiereMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.picture.premiere.message"));
            result = false;
        }

        if (picture.getImage() == null || picture.getImage().length == 0 || picture.getImage().length > MAX_PICTURE_SIZE) {
            request.setAttribute("errorPictureImageMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.picture.image.message"));
            result = false;
        }

        if ((picture.getSlogan() != null && (picture.getSlogan().length() > MAX_SLOGAN_SIZE)||!picture.getSlogan()
                .matches(SLOGAN_PATTERN))) { request.setAttribute("errorPictureSloganMessage",
                MessageManager.valueOf(request.getSession().getAttribute("currentLocale")
                        .toString()).getMessage("error.picture.slogan.message"));
            result = false;
        }

        if (picture.getRating() == 0 || picture.getRating() < MIN_RATING || picture.getRating() > MAX_RATING) {
            request.setAttribute("errorPictureRatingMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.picture.rating.message"));
            result = false;
        }

        if (picture.getDuration() == 0 || picture.getDuration() > MAX_DURATION) {
            request.setAttribute("errorPictureDurationMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.picture.duration.message"));
            result = false;
        }

        return result;
    }

    /** Validate income query for emptiness.
     * @param fromDate from what date to filter.
     * @param toDate by what date to filter.
     * @param type picture type.
     * @param countries list of countries.
     * @param genres list of genres.
     * @param directors list of directors.
     * @return true if query valid, false if query is empty.
     *
     */
    public boolean isEmptyQuery(String fromDate, String toDate, String type, String ageLimit,
                                      String[] countries, String[] genres, String[] directors) {
        return fromDate.isEmpty() && toDate.isEmpty() && type == null && ageLimit.equals(DEFAULT_AGE) && countries == null &&
                genres == null && directors == null;
    }
}
