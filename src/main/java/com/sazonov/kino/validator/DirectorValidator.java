package com.sazonov.kino.validator;

import com.sazonov.kino.entity.Director;
import com.sazonov.kino.manager.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

/**
 * Class {@code DirectorValidator} validates {@code Director} object.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Director
 * @since JDK7.0
 * @version 1.0
 */
public class DirectorValidator {
    private static final String DIRECTOR_NAME_PATTERN = "^([a-zA-Zа-яА-ЯіўІЎ]+(-?)){2,40}$";
    private static final int FIRST_DIRECTOR_BIRTHDAY = 1850;
    private static final int MAX_PICTURE_SIZE = 307_200;

    /** Validate income {@code Director} object.
     * @param director director object for validation.
     * @param request contains result of validation.
     * @return true if object valid, false otherwise.
     *
     * @see javax.servlet.http.HttpServletRequest
     */
    public boolean isDirectorValid(Director director, HttpServletRequest request) {
        boolean result = true;
        if (director.getName() == null || !director.getName().matches(DIRECTOR_NAME_PATTERN)) {
            request.setAttribute("errorDirectorNameMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.director.name.message"));
            result = false;
        }

        if (director.getLastName() == null || !director.getLastName().matches(DIRECTOR_NAME_PATTERN)) {
            request.setAttribute("errorDirectorSurnnameMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.director.surnname.message"));
            result = false;
        }

        LocalDate currentDate = LocalDate.now();
        if (director.getBirthDate() == null || director.getBirthDate().isAfter(currentDate) ||
                director.getBirthDate().getYear() < FIRST_DIRECTOR_BIRTHDAY) {
            request.setAttribute("errorDirectorBirthdayMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.director.birthday.message"));
            result = false;
        }

        if (director.getPhoto() == null || director.getPhoto().length == 0 || director.getPhoto().length > MAX_PICTURE_SIZE) {
            request.setAttribute("errorDirectorImageMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.director.image.message"));
            result = false;
        }

        if (director.getCountry().getCountryId() == 0){
            request.setAttribute("errorDirectorCountryMessage", MessageManager.valueOf(request.getSession()
                    .getAttribute("currentLocale").toString()).getMessage("error.director.country.message"));
            result = false;
        }
        return result;
    }
}
