package com.sazonov.kino.connection;

import com.mysql.cj.jdbc.Driver;
import com.sazonov.kino.exception.ConnectorCreatorException;
import com.sazonov.kino.util.PropertiesReader;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;
/**
 * Class {@code ConnectionPool} is thread sage singleton.
 * It provides loading of pool with database connection after initialisation and
 * organizes issuance, return and closing of connections.
 *
 * @see com.sazonov.kino.connection.WrapperConnection
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public final class ConnectionPool {
    private final static Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
    private final static int DEFAULT_POOL_SIZE = 30;
    private static int poolSize;
    private static ConnectionPool instance;
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
    private static ReentrantLock lock = new ReentrantLock();
    private static ArrayDeque<WrapperConnection> connectionQueue = new ArrayDeque<>();
    /*Private ConnectionPool constructor that supplies reflection defence, driver registration and
    * loading of pool with new connections. Throws RuntimeException if db drivers were not registered.*/
    private ConnectionPool() {
        if (instance != null) {
            throw new RuntimeException();
        }

        try {
            DriverManager.registerDriver(new Driver());
        } catch (SQLException exception) {
            throw new RuntimeException(exception);
        }
        loadPool();
        LOGGER.log(Level.DEBUG, "ConnectionPool was initialised.");
    }
    /** Method provides thread safe {@code ConnectionPool} initialisation and access to the
     * only {@code ConnectionPool} object.
     * @return single {@code ConnectionPool} object.
     */
    public static ConnectionPool getInstance() {
        if (!instanceCreated.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    instanceCreated.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }
    /** Method without parameters, provides thread sage issuance of {@code WrapperConnection} from
     * the inner queue.
     * @return {@code WrapperConnection} object from inner queue.
     *
     * @see com.sazonov.kino.connection.WrapperConnection
     */
    public WrapperConnection getConnection() {
        WrapperConnection connection = null;
        while (connection == null) {
            try {
                if (lock.tryLock(5, TimeUnit.MILLISECONDS)){
                    connection = connectionQueue.pollFirst();
                    lock.unlock();
                }
            } catch (InterruptedException exception) {
                LOGGER.log(Level.ERROR, exception);
            }
        }
        return connection;
    }
    /** Method provides thread safe returning of {@code WrapperConnection} object
     * to inner queue.
     * @param connection income parameter that contains the object that will be returned to the
     *                   conneciton pool.
     * @see com.sazonov.kino.connection.WrapperConnection*/
    public void releaseConnection(WrapperConnection connection) {
        if (!connection.getAutoCommit()) {
            connection.setAutoCommit(true);
        }
        try {
            int count = 0;
            while(count == 0) {
                if (lock.tryLock(5, TimeUnit.MILLISECONDS)) {
                    connectionQueue.addLast(connection);
                     count++;
                    lock.unlock();
                }
            }
        } catch (InterruptedException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
    }
    /** Method provides clothing of all connections, that inner queue contains, and
     * deregister of all drivers that provides connections.
     @see com.sazonov.kino.connection.WrapperConnection
     */
    public void closeConnections() {
        int count = 0;
        try {
            while(count == 0) {
                if (connectionQueue.size() == poolSize) {
                    if (lock.tryLock(5, TimeUnit.MILLISECONDS)) {
                        for (int i = 0; i < poolSize; i++) {
                            connectionQueue.pollFirst().closeConnection();
                        }
                        count++;
                        lock.unlock();
                    }
                }
            }
        } catch (InterruptedException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        try {
            Enumeration<java.sql.Driver> drivers = DriverManager.getDrivers();
            while (drivers.hasMoreElements()) {
                java.sql.Driver driver = drivers.nextElement();
                DriverManager.deregisterDriver(driver);
            }
        } catch (SQLException exception) {
            LOGGER.log(Level.ERROR, "Drivers deregistration failed", exception);
        }
        LOGGER.log(Level.DEBUG, "Drivers are deregistered.");
    }
    /** Override {@code Object} clone() method, which in order to protect singleton
     * from clone operation
     * @throws java.lang.CloneNotSupportedException throws {@code CloneNotSupportedException} in case of
     * attempt to clone the singleton.
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        if (instance != null) {
            throw new CloneNotSupportedException("ConnectionPool clone defence");
        }
        return super.clone();
    }
    /*Method provides loading of connection pool with new connections, limited by the pool size,
    * which defined in properties file or */
    private void loadPool() {
        PropertiesReader reader = new PropertiesReader();
        Properties properties = reader.createPoolSizeProperties();
        try {
            poolSize = Integer.parseInt(properties.getProperty("poolSize", String.valueOf(DEFAULT_POOL_SIZE)));
        } catch (NumberFormatException exception) {
            LOGGER.log(Level.ERROR, "poolSize value in properties file is not number " + exception);
            poolSize = DEFAULT_POOL_SIZE;
        }

        try {
            for (int i = 0; i < poolSize; i++) {
                connectionQueue.add(new WrapperConnection());
            }
            LOGGER.log(Level.DEBUG, "ConnectionPool loaded");
        } catch (ConnectorCreatorException exception) {
            LOGGER.log(Level.ERROR, "Connection creation failed, connection is null");
        }
    }
}
