package com.sazonov.kino.connection;

import com.sazonov.kino.exception.ConnectorCreatorException;
import com.sazonov.kino.util.PropertiesReader;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
/**
 * Class {@code ConnectionCreator} creates the mysql database connection.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
class ConnectionCreator {
    private final static Logger LOGGER = LogManager.getLogger(ConnectionCreator.class);
    private final static String DEFAULT_DB_URL = "jdbc:mysql://localhost:3306/kinorating_db";
    private final static String DB_URL_PARAM = "url";
    /** Method provides the creation of new {@code Connection} object.
     * @return created MySql {@code Connection} object.
     */
    static Connection createConnection() throws ConnectorCreatorException{
        Connection connection;
        PropertiesReader reader = new PropertiesReader();
        Properties properties = reader.createConnectionProperties();

        if (properties.isEmpty()) {
            throw new ConnectorCreatorException("Empty connection settings file");
        }
        try {
            connection = DriverManager.getConnection(properties.getProperty(DB_URL_PARAM, DEFAULT_DB_URL), properties);
            LOGGER.log(Level.DEBUG, "Connection created.");
        } catch (SQLException exception) {
            throw new ConnectorCreatorException(exception);
        }
        return connection;
    }
}
