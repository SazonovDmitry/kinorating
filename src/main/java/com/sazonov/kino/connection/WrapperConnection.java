package com.sazonov.kino.connection;

import com.sazonov.kino.exception.ConnectorCreatorException;
import com.sazonov.kino.exception.WrapperConnectionException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

/**
 * Class {@code WrapperConnection} is realisation or wrapper
 * pattern for the {@code Connection} class.
 *
 * @author Sazonov
 * @see java.sql.Connection
 * @since JDK7.0
 * @version 1.0
 */

public class WrapperConnection {
    private final static Logger LOGGER = LogManager.getLogger(WrapperConnection.class);
    private Connection connection;

    /** Constructs {@code WrapperConnection} object and initialize private {@code Connection} field.
     *@throws com.sazonov.kino.exception.ConnectorCreatorException if connection creation is failed.
     * @see java.sql.Connection
     * @see com.sazonov.kino.connection.ConnectionCreator
     */
    WrapperConnection() throws ConnectorCreatorException {
        connection = ConnectionCreator.createConnection();
    }

    /** Method creates {@code PreparedStatement} object, derived from connection.
     * @param sqlQuery income parameter, contains the SQL request as string.
     * @return {@code PreparedStatement} object, derived from connection.
     *
     * @throws com.sazonov.kino.exception.WrapperConnectionException if some of variables are null or
     * SQLException was generated.
     * @see java.sql.Connection
     * @see java.sql.PreparedStatement
     */
    public PreparedStatement getPreparedStatement(String sqlQuery) throws WrapperConnectionException {

        try {
            if (connection != null && sqlQuery != null) {
                PreparedStatement statement = connection.prepareStatement(sqlQuery);
                if (statement != null) {
                    return statement;
                }
            }
            throw new WrapperConnectionException("Connection or statement is null");
        } catch (SQLException exception) {
            throw new WrapperConnectionException(exception);
        }
    }
    /** Method creates {@code CallableStatement} object, derived from connection.
     * @param sql income parameter, contains the SQL request as string.
     * @return {@code CallableStatement} object, derived from connection.
     *
     * @throws com.sazonov.kino.exception.WrapperConnectionException if some of variables is null or
     * SQLException was generated.
     * @see java.sql.Connection
     * @see java.sql.CallableStatement
     */
    public CallableStatement getCallableStatement(String sql) throws WrapperConnectionException {
        try {
            if (connection != null && sql != null) {
                CallableStatement statement = connection.prepareCall(sql);
                if (statement != null) {
                    return statement;
                }
            }
            throw new WrapperConnectionException("Connection or statement is null");
        } catch (SQLException exception) {
            throw new WrapperConnectionException(exception);
        }
    }

    /** Method sets the state of wrapped connection autocommit.
     * @param autoCommit income parameter, that private connection field accepts.
     * @see java.sql.Connection
     */
    public void setAutoCommit(boolean autoCommit) {
        try {
            connection.setAutoCommit(autoCommit);
        } catch (SQLException exception) {
            LOGGER.log(Level.ERROR, "Error while setting autocommit " + exception);
        }
    }
    /** Method returns the current connection autocommit state.
     * @see java.sql.Connection
     */
    public boolean getAutoCommit() {
        try {
            return connection.getAutoCommit();
        } catch (SQLException exception) {
            LOGGER.log(Level.ERROR, "Error while getting autocommit " + exception);
        }
        return false;
    }
    /** Method releases the commit method on the current connection object.
     * @see java.sql.Connection
     */
    public void releaseCommit() {
        try {
           connection.commit();
        } catch (SQLException exception) {
            LOGGER.log(Level.ERROR, "Error while commit " + exception);
        }
    }
    /** Method releases the rollback method on the current connection object.
     * @see java.sql.Connection
     */
    public void releaseRollBack() {
        try {
            connection.rollback();
        } catch (SQLException exception) {
            LOGGER.log(Level.ERROR, "Error while rollback " + exception);
        }
    }
    /** Method invokes method close() on given {@code Statement} object.
     * @param statement income parameter as a {@code Statement} object, on which the close() method is invoked.
     * @see java.sql.Statement
     */
    public void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException exception) {
                LOGGER.log(Level.ERROR, exception);
            }
        }
    }
    /** Method returns current {@code WrapperConnection} object to the connection pool.
     * @see com.sazonov.kino.connection.WrapperConnection
     */
    public void close() {
        ConnectionPool.getInstance().releaseConnection(this);
    }
    /** Method invokes method close() on current object connection field.
     * @see java.sql.Connection
     */
    void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
                LOGGER.log(Level.DEBUG, "Connection closed.");
            } catch (SQLException exception) {
                LOGGER.log(Level.ERROR, exception);
            }
        }
    }
}
