package com.sazonov.kino.exception;

/**
 * Class {@code ConnectorCreatorException} inherits of {@code Exception} class.
 * Represents type of custom exception, thrown while connection creation.
 *
 * @author Sazonov
 * @see com.sazonov.kino.connection.ConnectionCreator
 * @since JDK7.0
 * @version 1.0
 */
public class ConnectorCreatorException extends Exception{

    /** Constructs {@code ConnectorCreatorException} object with income message.
     * @param message income message about the exception.
     *
     * @see com.sazonov.kino.connection.ConnectionCreator
     */
    public ConnectorCreatorException(String message) {
        super(message);
    }

    /** Constructs {@code ConnectorCreatorException} object with income cause.
     * @param cause object of cause exception.
     *
     * @see com.sazonov.kino.connection.ConnectionCreator
     */
    public ConnectorCreatorException(Throwable cause) {
        super(cause);
    }
}
