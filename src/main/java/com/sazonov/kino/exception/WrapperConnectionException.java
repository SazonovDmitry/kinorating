package com.sazonov.kino.exception;

/**
 * Class {@code WrapperConnectionException} inherits of {@code Exception} class.
 * Represents type of custom exception, thrown is some exception situation happens
 * while getting statement object.
 *
 * @author Sazonov
 * @see com.sazonov.kino.connection.WrapperConnection
 * @since JDK7.0
 * @version 1.0
 */
public class WrapperConnectionException extends Exception{

    /** Constructs {@code WrapperConnectionException} object with income message.
     * @param message income message about the exception.
     *
     * @see com.sazonov.kino.connection.WrapperConnection
     */
    public WrapperConnectionException(String message) {
        super(message);
    }

    /** Constructs {@code WrapperConnectionException} object with income cause.
     * @param cause object of cause exception.
     *
     * @see com.sazonov.kino.connection.WrapperConnection
     */
    public WrapperConnectionException(Throwable cause) {
        super(cause);
    }
}
