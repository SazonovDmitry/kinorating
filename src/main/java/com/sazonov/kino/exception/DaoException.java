package com.sazonov.kino.exception;


/**
 * Class {@code DaoException} inherits of {@code Exception} class.
 * Represents type of custom exception, thrown while dao operations.
 *
 * @author Sazonov
 * @see com.sazonov.kino.dao
 * @since JDK7.0
 * @version 1.0
 */
public class DaoException extends Exception {

    /** Constructs {@code DaoException} object by default.
     * @see com.sazonov.kino.dao
     */
    public DaoException() { }

    /** Constructs {@code DaoException} object with income message.
     * @param message income message about the exception.
     *
     * @see com.sazonov.kino.dao
     */
    public DaoException(String message) {
        super(message);
    }

    /** Constructs {@code DaoException} object with income message and cause object.
     * @param message income message about the exception.
     * @param cause exception object which is cause of current exception.
     * @see com.sazonov.kino.dao
     */
    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    /** Constructs {@code DaoException} object with income cause object.
     * @param cause exception object which is cause of current exception.
     * @see com.sazonov.kino.dao
     */
    public DaoException(Throwable cause) {
        super(cause);
    }
}
