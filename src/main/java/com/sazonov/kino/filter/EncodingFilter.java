package com.sazonov.kino.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import java.io.IOException;

/**
 * Class {@code EncodingFilter} is realisation of {@code Filter} interface.
 * It provides default UTF-8 encoding installation.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
@WebFilter(urlPatterns = { "/*" },
        initParams = {
                @WebInitParam(name = "encoding", value = "UTF-8", description = "Encoding Param") })
public class EncodingFilter implements Filter {
    private String code;

    /** Override {@code Filter} interface method, initialize code variable.
     * @param fConfig FilterConfig object, which gives access to external environment.
     *
     */
    @Override
    public void init(FilterConfig fConfig){
        code = fConfig.getInitParameter("encoding");
    }

    /** Override {@code Filter} interface method, which processes analysis of request.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @param response income parameter, passed from project controller and
     *                contains all necessary information for changing output.
     * @param chain link to chain object.
     * @see javax.servlet.http.HttpServletResponse
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        String codeRequest = request.getCharacterEncoding();

        if (code != null && !code.equalsIgnoreCase(codeRequest)) {
            request.setCharacterEncoding(code);
            response.setCharacterEncoding(code);
        }
        chain.doFilter(request, response);
    }

    /** Override {@code Filter} interface method, destroy code variable.
     *
     */
    @Override
    public void destroy() {
        code = null;
    }
}
