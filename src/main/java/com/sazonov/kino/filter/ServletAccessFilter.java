package com.sazonov.kino.filter;

import com.sazonov.kino.entity.UserRole;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;


/**
 * Class {@code ServletAccessFilter} is realisation of {@code Filter} interface.
 * It provides default locale and user role installation.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
@WebFilter(urlPatterns = { "/*" }, servletNames = { "controller" })
public class ServletAccessFilter implements Filter {

    /** Override {@code Filter} interface method, initialize code variable.
     * @param fConfig FilterConfig object, which gives access to external environment.
     *
     */
    @Override
    public void init(FilterConfig fConfig){
    }

    /** Override {@code Filter} interface method, which processes analysis of request.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @param response income parameter, passed from project controller and
     *                contains all necessary information for changing output.
     * @param chain link to chain object.
     * @see javax.servlet.http.HttpServletResponse
     * @see javax.servlet.http.HttpServletRequest
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;

        HttpSession session = req.getSession();
        UserRole type = (UserRole) session.getAttribute("userRole");

        if (type == null) {
            type = UserRole.GUEST;
            session.setAttribute("userRole", type);
            session.setAttribute("currentLocale", "RU");
        }
        chain.doFilter(request, response);
    }

    /** Override {@code Filter} interface method, destroy code variable.
     *
     */
    @Override
    public void destroy() {
    }
}
