package com.sazonov.kino.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class {@code EncodingFilter} is realisation of {@code Filter} interface.
 * It provides default UTF-8 encoding installation.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
@WebFilter(urlPatterns = { "/*" }, servletNames = { "controller" })
public class NoCacheFilter implements Filter {
    /** Override {@code Filter} interface method.
     * @param filterConfig FilterConfig object, which gives access to external environment.
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
    /** Override {@code Filter} interface method, destroy code variable.
     */
    @Override
    public void destroy() {
    }

    /** Override {@code Filter} interface method, which processes analysis of request.
     * @param req income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @param res income parameter, passed from project controller and
     *                contains all necessary information for changing output.
     * @param chain link to chain object.
     * @see javax.servlet.http.HttpServletResponse
     * @see javax.servlet.http.HttpServletRequest
     */
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;

        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0); // Proxies.

        chain.doFilter(req, res);
    }
}
