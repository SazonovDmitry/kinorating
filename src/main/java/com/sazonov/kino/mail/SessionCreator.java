package com.sazonov.kino.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;

/**
 * Class {@code SessionCreator} creates new email sending session.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
class SessionCreator {
    private static final String HOST = "mail.smtp.host";
    private static final String PORT = "mail.smtp.port";
    private static final String NAME = "mail.user.name";
    private static final String PASSWORD = "mail.user.password";
    private static final String SMTP = "smtp";
    private static final String TRUE = "true";
    private static final String FALSE = "false";
    private static final String FACTORY_CLASS = "javax.net.ssl.SSLSocketFactory";
    private String smtpHost;
    private String smtpPort;
    private String userName;
    private String userPassword;
    private Properties sessionProperties;

    /** Constructs {@code SessionCreator} object and initialize private fields.
     * @param configProperties properties object, contains all initialization information.
     *
     */
    SessionCreator(Properties configProperties) {
        smtpHost = configProperties.getProperty(HOST);
        smtpPort = configProperties.getProperty(PORT);
        userName = configProperties.getProperty(NAME);
        userPassword = configProperties.getProperty(PASSWORD);

        sessionProperties = new Properties();
        sessionProperties.setProperty("mail.transport.protocol", SMTP);
        sessionProperties.setProperty("mail.host", smtpHost);
        sessionProperties.put("mail.smtp.auth", TRUE);
        sessionProperties.put("mail.smtp.port", smtpPort);
        sessionProperties.put("mail.smtp.socketFactory.port", smtpPort);
        sessionProperties.put("mail.smtp.socketFactory.class", FACTORY_CLASS);
        sessionProperties.put("mail.smtp.socketFactory.fallback", FALSE);
        sessionProperties.setProperty("mail.smtp.quitwait", FALSE);
    }

    /** create new mail session.
     * @return created session object.
     */
    public Session createSession() {
        return Session.getDefaultInstance(sessionProperties,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(userName, userPassword);
                    }
                });
    }
}
