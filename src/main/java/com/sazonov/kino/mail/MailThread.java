package com.sazonov.kino.mail;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Class {@code MailThread} is inherit of {@code Thread} class.
 * It provides mail sending process.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public class MailThread extends Thread {
    private static final Logger LOGGER = LogManager.getLogger(MailThread.class);
    private static final String MESSAGE_CONTENT_TYPE = "text/html";
    private MimeMessage message;
    private String sendToEmail;
    private String mailSubject;
    private String mailText;
    private Properties properties;

    /** Constructs {@code MailThread} object and initialize object fields.
     * @param sendToEmail delivery address.
     * @param mailSubject email topic.
     * @param mailText email content.
     * @param properties properties of email sending.
     */
    public MailThread(String sendToEmail, String mailSubject, String mailText, Properties properties) {
        this.sendToEmail = sendToEmail;
        this.mailSubject = mailSubject;
        this.mailText = mailText;
        this.properties = properties;
    }

    /** Method creates and initialize mail session.
     */
    private void init() {
        Session mailSession = (new SessionCreator(properties)).createSession();
        mailSession.setDebug(true);

        message = new MimeMessage(mailSession);
        try {
            message.setSubject(mailSubject);
            message.setContent(mailText, MESSAGE_CONTENT_TYPE);
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(sendToEmail));
        } catch (AddressException e) {
            LOGGER.log(Level.ERROR, "Incorrect address:" + sendToEmail + " " + e);
        } catch (MessagingException e) {
            LOGGER.log(Level.ERROR,"Message creating error " + e);
        }
    }

    /** Override {@code Thread} interface method, describes new thread
     * actions of sending an email.
     */
    public void run() {
        init();
        try {
            Transport.send(message);
        } catch (MessagingException e) {
            LOGGER.log(Level.ERROR,"Message sending error: " + e);
        }
    }
}
