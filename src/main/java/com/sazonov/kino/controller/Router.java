package com.sazonov.kino.controller;

/**
 * Class {@code Router} contains Enum for defining further steps executing
 * protection from page refresh.
 *
 * @author Sazonov
 * @see com.sazonov.kino.command.Command
 * @since JDK7.0
 * @version 1.0
 */
public class Router {
    public enum RouteType {
        FORWARD, REDIRECT
    }
    private String pagePath;

    private RouteType route = RouteType.FORWARD;

    /** Method returns saved request content hash as String.
     * @return saved request content hash.
     * @see javax.servlet.http.HttpServletRequest
     */
    public String getPagePath() {
        return pagePath;
    }

    /** Method returns saved request content hash as String.
     * @param pagePath saves request content hash as String to {@code Router} object
     * @see javax.servlet.http.HttpServletRequest
     */
    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    /** Method returns inner enum, which defines further forward or redirect pass.
     * @return inner enum type.
     */
    public RouteType getRoute() {
        return route;
    }
    /** Method defines inner enum, which defines further forward or redirect pass.
     * @param route enum type.
     */
    public void setRoute(RouteType route) {
        if (route == null) {
            this.route = RouteType.FORWARD;
        }
        this.route = route;
    }
}

