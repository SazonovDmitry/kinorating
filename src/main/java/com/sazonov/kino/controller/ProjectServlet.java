package com.sazonov.kino.controller;

import com.sazonov.kino.command.ActionFactory;
import com.sazonov.kino.command.Command;
import com.sazonov.kino.command.EmptyCommand;
import com.sazonov.kino.connection.ConnectionPool;
import com.sazonov.kino.manager.PassManager;
import com.sazonov.kino.util.HashCreator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.util.Optional;
/**
 * Class {@code ProjectServlet} is a project controller.
 *Extends {@code HttpServlet} class.
 * @author Sazonov
 * @see javax.servlet.http.HttpServlet
 * @since JDK8.0
 * @version 1.0
 */
@WebServlet(name = "controller", urlPatterns = {"/controller"} )
@MultipartConfig(maxFileSize = 614400)
public class ProjectServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(ProjectServlet.class);
    private static final String REQUEST_PARAM = "command";
    private static final String FROM_PARAM = "fromPage";

    /** Constructs {@code ProjectServlet} object.
     *
     * @see javax.servlet.http.HttpServlet
     */
    public ProjectServlet() {
    }

    /** Overrides {@code HttpServlet} doGet method, which processes HTTP get requests.
     * Method redirects request and response to processRequest() method for further processing.
     * @param request income parameter, represented by {@code HttpServletRequest} object.
     * @param response income parameter, represented by {@code HttpServletResponse} object.
     *
     * @see javax.servlet.http.HttpServletRequest
     * @see javax.servlet.http.HttpServletResponse
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /** Overrides {@code HttpServlet} doPost method, which processes HTTP get requests.
     * Method redirects request and response to processRequest() method for further processing.
     * Also provides duplicate request defence.
     * @param request income parameter, represented by {@code HttpServletRequest} object.
     * @param response income parameter, represented by {@code HttpServletResponse} object.
     *
     * @see javax.servlet.http.HttpServletRequest
     * @see javax.servlet.http.HttpServletResponse
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getParameter(FROM_PARAM);
        HashCreator hashCreator = new HashCreator();
        String pageDefiner = hashCreator.mapHashDefiner(request.getParameterMap());
        try {
            Router router = (Router)request.getSession().getAttribute("router");
            if (router == null) {
                router = new Router();
                request.getSession().setAttribute("router", router);
            }

            if (path != null) {
                if (pageDefiner.equals(router.getPagePath())) {
                    router.setRoute(Router.RouteType.REDIRECT);
                } else {
                    router.setPagePath(pageDefiner);
                }
                switch (router.getRoute()) {
                    case FORWARD:
                        processRequest(request, response);
                        break;
                    case REDIRECT:
                        router.setRoute(Router.RouteType.FORWARD);
                        response.sendRedirect(request.getContextPath() + path);
                        break;
                }
            } else {
                processRequest(request, response);
            }
        } catch (ClassCastException exception) {
            LOGGER.log(Level.ERROR, exception);
            processRequest(request, response);
        }
    }

    /** Method processes the transmitted request, defines command type and the back page
     * for further forward or redirect.
     * @param request income parameter, represented by {@code HttpServletRequest} object.
     * @param response income parameter, represented by {@code HttpServletResponse} object.
     *
     * @see javax.servlet.http.HttpServletRequest
     * @see javax.servlet.http.HttpServletResponse
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Optional<Command> optionalCommand = ActionFactory.defineCommand(request.getParameter(REQUEST_PARAM));
        Command command = optionalCommand.orElse(new EmptyCommand());
        request.getSession().setAttribute("currentDirector", null);

        String page = command.execute(request);
        if (page != null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher(page);
            dispatcher.forward(request, response);
        } else {
            response.sendRedirect(request.getContextPath() + PassManager.PASS_PAGE_INDEX.getPass());
        }
    }

    /** Overrides {@code HttpServlet} destroy() method. Close all connections in
     * connection pool.
     * @see com.sazonov.kino.connection.ConnectionPool
     */
    @Override
    public void destroy() {
        ConnectionPool.getInstance().closeConnections();
    }
}
