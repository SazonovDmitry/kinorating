package com.sazonov.kino.logic;

import com.sazonov.kino.dao.ReviewDAO;
import com.sazonov.kino.entity.Review;
import com.sazonov.kino.exception.DaoException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code ReviewLogic} implementation of {@code Logic} interface.
 * It logic MVC layer for review entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Review
 * @since JDK8.0
 * @version 1.0
 */
public class ReviewLogic implements Logic {
    private static final Logger LOGGER = LogManager.getLogger(ReviewLogic.class);

    /**Add new review record to database.
     * @param content new review content.
     * @param userId user id.
     * @param pictureId picture id.
     * @return true is operation is succeed and false otherwise.
     *
     * @see com.sazonov.kino.entity.Review
     */
    public boolean addReview(String content, int userId, int pictureId) {
        Review review = new Review();
        review.setUserId(userId);
        review.setContent(content);
        review.setPictureId(pictureId);

        ReviewDAO dao = new ReviewDAO();
        try {
            return dao.create(review);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /**Find reviews by searching query.
     * @param from from what date to search.
     * @param to to what date to search.
     * @return list fo reviews.
     *
     * @see com.sazonov.kino.entity.Review
     */
    public List<Review> reviewsByQuery(LocalDate from, LocalDate to) {
        ReviewDAO dao = new ReviewDAO();

        List<Review> reviews = new ArrayList<>();
        try {
            reviews = dao.findReviewsByQuery(from, to);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return reviews;
    }

    /**Delete review record from database.
     * @param reviewId review id.
     * @return true is operation is succeed and false otherwise.
     *
     * @see com.sazonov.kino.entity.Review
     */
    public boolean deleteReview(int reviewId) {
        ReviewDAO dao = new ReviewDAO();
        try {
            return dao.delete(reviewId);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }
}
