package com.sazonov.kino.logic;

import com.sazonov.kino.dao.*;
import com.sazonov.kino.entity.Country;
import com.sazonov.kino.entity.Director;
import com.sazonov.kino.entity.Genre;
import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.exception.DaoException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class {@code PictureLogic} implementation of {@code Logic} interface.
 * It logic MVC layer for picture entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Picture
 * @since JDK8.0
 * @version 1.0
 */
public class PictureLogic implements Logic {
    private static final Logger LOGGER = LogManager.getLogger(PictureLogic.class);
    private static final String DEFAULT_AGE_LIMIT = "11";
    private static final int STEP = 10;
    private static final int TOP_CEIL = 20;
    private static final int TOP_FLOOR = 0;

    /**Provides searching of pictures by user's query.
     * @param query String symbols or words to find pictures.
     * @return list of pictures which contains query symbols.
     *
     * @see com.sazonov.kino.entity.Picture
     */
    public List<Picture> searchPicturesByName(String query) {
        PictureDAO dao = new PictureDAO();
        List<Picture> pictures = new ArrayList<>();
        try {
            pictures = dao.findAllPictures();
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        pictures.removeIf((s) -> !s.getName().contains(query));
        pictures.sort(Comparator.comparing(Picture::getRating).reversed());
        return  pictures;
    }

    /**Provides searching of picture by id.
     * @param pictureId is picture id.
     * @return {@code Picture} object with given id.
     *
     * @see com.sazonov.kino.entity.Picture
     */
    public Picture searchPicturesById(String pictureId) {
        Picture picture = null;
        PictureDAO dao = new PictureDAO();
        try {
            picture = dao.findEntityById(Integer.parseInt(pictureId));
        } catch (DaoException|NumberFormatException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        return picture;
    }

    /**Provides searching of top 20 pictures.
     * @return list of top pictures.
     *
     * @see com.sazonov.kino.entity.Picture
     */
    public List<Picture> searchTopPictures() {

        PictureDAO dao = new PictureDAO();
        List<Picture> allPictures = new ArrayList<>();
        try {
            allPictures = dao.findAllPictures();
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        allPictures.sort(Comparator.comparing(Picture::getRating).reversed());
        return allPictures.subList(TOP_FLOOR, TOP_CEIL);
    }

    /**Provides rating of picture.
     * @param userId user id.
     * @param pictureId picture id.
     * @param rating rating value.
     * @return true is operation is succeed and false otherwise.
     *
     * @see com.sazonov.kino.entity.Picture
     */
    public boolean ratePicture(int userId, int pictureId, int rating) {
        PictureDAO dao = new PictureDAO();
        boolean pictureRated = false;
        try {
            pictureRated = dao.addRating(userId, pictureId, rating);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        return pictureRated;
    }

    /**Provides creating of new {@code Picture} object .
     * @param picture new {@code Picture} object for writing to database.
     * @return true is operation is succeed and false otherwise.
     *
     * @see com.sazonov.kino.entity.Picture
     */
    public int createNewPicture(Picture picture) {
        PictureDAO dao = new PictureDAO();
        int pictureId = 0;
        boolean pictureCreated;
        try {
            pictureId = dao.findPictureId(picture);
            if (pictureId == 0) {
                pictureCreated = dao.create(picture);
                if (pictureCreated) {
                    pictureId = dao.findPictureId(picture);
                }
            }
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        LOGGER.log(Level.DEBUG, pictureId);
        return pictureId;
    }

    /**Creates the list of existing countries.
     * @return list of countries or null.
     *
     * @see com.sazonov.kino.entity.Country
     */
    public List<Country> createCountryList() {
        CountryDAO dao = new CountryDAO();
        List<Country> countries = null;
        try {
            countries = dao.findAll();
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        return countries;
    }

    /**Creates the list of existing genres.
     * @return list of genres or null.
     *
     * @see com.sazonov.kino.entity.Genre
     */
    public List<Genre> createGenreList() {
        GenreDAO dao = new GenreDAO();
        List<Genre> genres = null;

        try {
            genres = dao.findAll();
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        return genres;
    }

    /**Creates the list of existing directors.
     * @return list of directors or null.
     *
     * @see com.sazonov.kino.entity.Director
     */
    public List<Director> createDirectorList() {
        DirectorDAO dao = new DirectorDAO();
        List<Director> directors = null;

        try {
            directors = dao.findAll();
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        return directors;
    }

    /**Add database records about countries, related to current picture.
     * @param countriesId list of countries.
     * @param pictureId picture id value.
     * @return true is operation is succeed and false otherwise.
     *
     * @see com.sazonov.kino.entity.Country
     */
    public boolean writePictureCountries(List<Integer> countriesId, int pictureId) {
        CountryDAO dao = new CountryDAO();
        try {
            return dao.insertPictureCountries(countriesId, pictureId);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
            return false;
        }
    }

    /**Add database records about directors, related to current picture.
     * @param directorsId list of directors.
     * @param pictureId picture id value.
     * @return true is operation is succeed and false otherwise.
     *
     * @see com.sazonov.kino.entity.Director
     */
    public boolean writePictureDirectors(List<Integer> directorsId, int pictureId) {
        DirectorDAO dao = new DirectorDAO();
        try {
            return dao.insertPictureDirectors(directorsId, pictureId);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
            return false;
        }
    }

    /**Add database records about genres, related to current picture.
     * @param genresId list of genres.
     * @param pictureId picture id value.
     * @return true is operation is succeed and false otherwise.
     *
     * @see com.sazonov.kino.entity.Genre
     */
    public boolean writePictureGenres(List<Integer> genresId, int pictureId) {
        GenreDAO dao = new GenreDAO();
        try {
            return dao.insertPictureGenres(genresId, pictureId);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
            return false;
        }
    }

    /**Delete database record about picture.
     * @param pictureId picture id.
     * @return true is operation is succeed and false otherwise.
     *
     * @see com.sazonov.kino.entity.Genre
     */
    public boolean deletePicture(int pictureId) {
        PictureDAO dao = new PictureDAO();
        try {
            return dao.delete(pictureId);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /**Search list of pictures using custom filter.
     * @param fromDate from what date to start search.
     * @param toDate by what date to search.
     * @param type picture type.
     * @param ageLimit age limit search parameter.
     * @param countries countries where the movie was made.
     * @param genres in what genres movies are presented.
     * @param directors what directors were making the movie.
     * @return list of filtered and sorted pictures.
     *
     * @see com.sazonov.kino.entity.Picture
     */
    public List<Picture> searchPicturesByQuery(String fromDate, String toDate, String type, String ageLimit,
                                               String[] countries, String[] genres, String[] directors) {
        PictureDAO dao = new PictureDAO();
        List<Picture> allPictures = new ArrayList<>();
        try {
            allPictures = dao.findAllPictures();
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        if (!fromDate.isEmpty()) {
            try {
                int fromYear = Integer.parseInt(fromDate);
                allPictures.removeIf((s) -> s.getYear() < fromYear);
            } catch (NumberFormatException exception) {
                LOGGER.log(Level.ERROR, exception);
            }
        }
        LOGGER.log(Level.DEBUG, allPictures.size());
        if (!toDate.isEmpty()) {
            try {
                int toYear = Integer.parseInt(toDate);
                allPictures.removeIf((s) -> s.getYear() > toYear);
            } catch (NumberFormatException exception) {
                LOGGER.log(Level.ERROR, exception);
            }
        }

        if (type != null) {
            allPictures.removeIf((s) -> !s.getPictureType().toString().equalsIgnoreCase(type));
        }

        if (!ageLimit.equals(DEFAULT_AGE_LIMIT)) {
            try {
                byte limit = Byte.parseByte(ageLimit);
                allPictures.removeIf((s) -> s.getAgeLimit() < limit);
            } catch (NumberFormatException exception) {
                LOGGER.log(Level.ERROR, exception);
            }
        }

        if (countries != null) {
            try {
                List<Integer> idList = Stream.of(countries).map(Integer::parseInt).collect(Collectors.toList());
                allPictures.removeIf((s) -> Collections.disjoint(extractCountriesId(s.getCountries()), idList));
            } catch (NumberFormatException exception) {
                LOGGER.log(Level.ERROR, exception);
            }
        }

        if (genres != null) {
            try {
                List<Integer> idList = Stream.of(genres).map(Integer::parseInt).collect(Collectors.toList());
                allPictures.removeIf((s) -> Collections.disjoint(extractGenresId(s.getGenres()), idList));
            } catch (NumberFormatException exception) {
                LOGGER.log(Level.ERROR, exception);
            }
        }

        if (directors != null) {
            try {
                List<Integer> idList = Stream.of(directors).map(Integer::parseInt).collect(Collectors.toList());
                allPictures.removeIf((s) -> Collections.disjoint(extractDirectorsId(s.getDirectors()), idList));
            } catch (NumberFormatException exception) {
                LOGGER.log(Level.ERROR, exception);
            }
        }
        LOGGER.log(Level.DEBUG, allPictures.size());
        allPictures.sort(Comparator.comparing(Picture::getRating).reversed());
        return allPictures;
    }

    /**Counts the number of page for multy page reading.
     * @param page current page value.
     * @return page number.
     *
     * @see com.sazonov.kino.entity.Picture
     */
    public Integer extractMinPage(String page) {
        int min = 0;
        try {
            int count = Integer.parseInt(page);
            min = (count - 1) * STEP;
        } catch (NumberFormatException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return min;
    }

    /**Extracts id from list of countries.
     * @param countries list of countries.
     * @return list of id.
     *
     * @see com.sazonov.kino.entity.Country
     */
    private List<Integer> extractCountriesId(List<Country> countries) {
        List<Integer> countriesId = new ArrayList<>();
        for (Country country: countries) {
            countriesId.add(country.getCountryId());
        }
        return countriesId;
    }

    /**Extracts id from list of genres.
     * @param genres list of genres.
     * @return list of id.
     *
     * @see com.sazonov.kino.entity.Genre
     */
    private List<Integer> extractGenresId(List<Genre> genres) {
        List<Integer> genresId = new ArrayList<>();
        for (Genre genre: genres) {
            genresId.add(genre.getGenreId());
        }
        return genresId;
    }

    /**Extracts id from list of directors.
     * @param directors list of directors.
     * @return list of id.
     *
     * @see com.sazonov.kino.entity.Director
     */
    private List<Integer> extractDirectorsId(List<Director> directors) {
        List<Integer> directorsId = new ArrayList<>();
        for (Director director: directors) {
            directorsId.add(director.getDirectorId());
        }
        return directorsId;
    }
}
