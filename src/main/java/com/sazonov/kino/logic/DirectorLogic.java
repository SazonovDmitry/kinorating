package com.sazonov.kino.logic;

import com.sazonov.kino.dao.DirectorDAO;
import com.sazonov.kino.entity.Director;
import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.exception.DaoException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Class {@code DirectorLogic} implementation of {@code Logic} interface.
 * It logic MVC layer for director entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Director
 * @since JDK7.0
 * @version 1.0
 */
public class DirectorLogic implements Logic{
    private static final Logger LOGGER = LogManager.getLogger(DirectorLogic.class);

    /** executes {@code Director} object by id from picture's director list.
     * @param picture contains director list.
     * @param directorId is int value of director id for comparing.
     * @return extracts {@code Director} object from Picture director list by id.
     *
     * @see com.sazonov.kino.entity.Director
     */
    public Director extractDirectorFromPicture(Picture picture, int directorId) {
        List<Director> directors = picture.getDirectors();
        Director currentDirector = null;
        for (Director director: directors) {
            if (director.getDirectorId() == directorId) {
                currentDirector = director;
            }
        }
        return currentDirector;
    }

    /**Provides creating of new {@code Director} object .
     * @param director new {@code Director} object for writing to database.
     * @return true is operation is succeed and false otherwise.
     *
     * @see com.sazonov.kino.entity.Director
     */
    public boolean createDirector(Director director) {
        DirectorDAO dao = new DirectorDAO();
        try {
            return dao.create(director);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /**Updates the list of existing directors.
     * @return list of directors or null.
     *
     * @see com.sazonov.kino.entity.Director
     */
    public List<Director> updateDirectorList() {
        DirectorDAO dao = new DirectorDAO();
        List<Director> directors = null;

        try {
            directors = dao.findAll();
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        return directors;
    }

    /**Provides deleting of director from database.
     * @param directorId id of director to delete.
     * @return true is operation is succeed and false otherwise.
     *
     * @see com.sazonov.kino.entity.Director
     */
    public boolean deleteDirector(int directorId) {
        DirectorDAO dao = new DirectorDAO();
        try {
            return dao.delete(directorId);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }
}
