package com.sazonov.kino.logic;

/**
 * interface {@code Logic} is used for logic classes defining.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public interface Logic {
}
