package com.sazonov.kino.logic;

import com.sazonov.kino.command.LoginCommand;
import com.sazonov.kino.dao.PictureDAO;
import com.sazonov.kino.dao.UserDAO;
import com.sazonov.kino.entity.User;
import com.sazonov.kino.entity.UserStatus;
import com.sazonov.kino.exception.DaoException;
import com.sazonov.kino.util.HashCreator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;

/**
 * Class {@code UserLogic} implementation of {@code Logic} interface.
 * It logic MVC layer for user entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Director
 * @since JDK8.0
 * @version 1.0
 */
public class UserLogic implements Logic {
    private static final Logger LOGGER = LogManager.getLogger(LoginCommand.class);
    private static final int BAN_DAY_BACK = 1;
    private static final int MIN_NUMBER_OF_RATES = 30;

    /** Creates user with given login and password, determines the status of the user.
     * @param login transferred login.
     * @param password transferred password.
     * @return extracts {@code User} object from form database and sets it's status if login
     * and password suits.
     *
     * @see com.sazonov.kino.entity.User
     */
    public User checkAndBuildUser(String login, String password) {
        User user = null;
        HashCreator hashCreator = new HashCreator();
        String hashLogin = hashCreator.createHash(login);
        String hashPassword = hashCreator.createHash(password);
        UserDAO dao = new UserDAO();
        PictureDAO pictureDAO = new PictureDAO();
        try {
            user = dao.checkUser(hashLogin, hashPassword);
            if (user != null) {
                double userRating = dao.getUserAverageRating(user.getUserId());
                double numberOfRates = pictureDAO.findNumberOfRates(user.getUserId());
                double picturesRating = pictureDAO.findAverageRatingByUser(user.getUserId());

                int average = Math.abs((int)(picturesRating - userRating));
                if (numberOfRates > MIN_NUMBER_OF_RATES) {
                    switch (average) {
                        case 0:
                            dao.setStatus(user.getUserId(), UserStatus.KINOMAN);
                            user.setStatus(UserStatus.KINOMAN);
                            break;
                        case 1:
                            dao.setStatus(user.getUserId(), UserStatus.KINO_EXPERT);
                            user.setStatus(UserStatus.KINO_EXPERT);
                            break;
                        default:
                            dao.setStatus(user.getUserId(), UserStatus.KINO_BEGINNER);
                            user.setStatus(UserStatus.KINO_BEGINNER);
                    }
                }
            }
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return user;
    }

    /** Creates new user with given parameters and makes a record in database.
     * @param login transferred login.
     * @param password transferred password.
     * @param email new user email.
     * @param name new user name.
     * @return new user or null.
     *
     * @see com.sazonov.kino.entity.User
     */
    public User createUser(String login, String password, String email, String name) {
        User user = null;
        HashCreator hashCreator = new HashCreator();
        String hashLogin = hashCreator.createHash(login);
        String hashPassword = hashCreator.createHash(password);
        UserDAO dao = new UserDAO();
        try {
            if (dao.createUser(hashLogin, hashPassword, email, name)) {
                user = dao.checkUser(hashLogin, hashPassword);
            }
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return user;
    }

    /**Checks if user's email is free or already used.
     * @param email new user email.
     * @return true if email is free, otherwise false.
     *
     * @see com.sazonov.kino.entity.User
     */
    public boolean isEmailFree(String email) {
        UserDAO dao = new UserDAO();
        try {
            return dao.isEmailAvailable(email);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /**Checks if user's login is free or already used.
     * @param login new user login.
     * @return true if login is free, otherwise false.
     *
     * @see com.sazonov.kino.entity.User
     */
    public boolean isLoginFree(String login) {
        HashCreator hashCreator = new HashCreator();
        String hashLogin = hashCreator.createHash(login);
        UserDAO dao = new UserDAO();
        try {
           return dao.isLoginAvailable(hashLogin);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /** Changes user personal data.
     * @param login transferred login.
     * @param password transferred password.
     * @param user user object which contains new personal data.
     * @return true if success, otherwise false.
     *
     * @see com.sazonov.kino.entity.User
     */
    public boolean changeUserData(User user, String login, String password) {
        UserDAO dao = new UserDAO();
        HashCreator hashCreator = new HashCreator();
        String hashLogin = null;
        if(!login.isEmpty()) {
            hashLogin = hashCreator.createHash(login);
        }
        String hashPassword = null;
        if (!password.isEmpty()) {
            hashPassword = hashCreator.createHash(password);
        }

        try {
            return dao.updateUserParams(user, hashLogin, hashPassword);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /** Changes user login/password.
     * @param login transferred login.
     * @param password transferred password.
     * @param email used to identify user.
     * @return true if success, otherwise false.
     *
     * @see com.sazonov.kino.entity.User
     */
    public boolean setNewLoginPass(String login, String password, String email) {
        UserDAO dao = new UserDAO();
        HashCreator hashCreator = new HashCreator();
        String hashLogin = null;
        if(login != null) {
            hashLogin = hashCreator.createHash(login);
        }
        String hashPassword = null;
        if (password != null) {
            hashPassword = hashCreator.createHash(password);
        }
        try {
            return dao.resetLoginPass(hashLogin, hashPassword, email);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /** Ban user till the given date.
     * @param userId user id.
     * @param banDate by what date to ban user.
     * @return true if success, otherwise false.
     *
     * @see com.sazonov.kino.entity.User
     */
    public boolean banUser(int userId, LocalDate banDate) {
        UserDAO dao = new UserDAO();
        try {
            return dao.banUser(userId, banDate);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /** Abolish the user ban.
     * @param userId user id.
     * @return true if success, otherwise false.
     *
     * @see com.sazonov.kino.entity.User
     */
    public boolean unBanUser(int userId) {
        UserDAO dao = new UserDAO();
        LocalDate current = LocalDate.now().minusDays(BAN_DAY_BACK);
        try {
            return dao.banUser(userId, current);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /** Set new user status
     * @param userId user id.
     * @param status new user status in system.
     * @return true if success, otherwise false.
     *
     * @see com.sazonov.kino.entity.User
     */
    public boolean setUserStatus(int userId, UserStatus status) {
        UserDAO dao = new UserDAO();
        try {
            return dao.setStatus(userId, status);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /** Add given picture to favorite list.
     * @param userId user id.
     * @param pictureId picture to be added to list.
     * @return true if success, otherwise false.
     *
     * @see com.sazonov.kino.entity.User
     */
    public boolean addFavoritePicture(int userId, int pictureId) {
        UserDAO dao = new UserDAO();
        try {
            return dao.addPictureToFavorite(userId, pictureId);
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }

    /** Remove picture from user's favorite list.
     * @param user current user.
     * @param pictureId picture to be removed from list.
     * @return true if success, otherwise false.
     *
     * @see com.sazonov.kino.entity.User
     */
    public boolean removeFavoritePicture(User user, final int pictureId) {
        UserDAO dao = new UserDAO();
        try {
            if (dao.removePictureFromFavorite(user.getUserId(), pictureId)) {
                user.getFavorite().removeIf((s) -> s.getPictureId() == pictureId);
            }
            return true;
        } catch (DaoException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return false;
    }
}
