package com.sazonov.kino.listener;

import com.sazonov.kino.entity.Country;
import com.sazonov.kino.entity.Director;
import com.sazonov.kino.entity.Genre;
import com.sazonov.kino.logic.PictureLogic;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.List;

/**
 * Class {@code ProjectStartListener} is realisation of {@code ServletContextListener} interface.
 * It provides project context initialization and destroy listening.
 *
 * @author Sazonov
 * @see javax.servlet.ServletContextListener
 * @since JDK7.0
 * @version 1.0
 */
@WebListener
public class ProjectStartListener implements ServletContextListener {
    private static final String COUNTRIES = "countriesDirectory";
    private static final String GENRES = "genresDirectory";
    private static final String DIRECTORS = "directorsDirectory";


    /** Override {@code ServletContextListener} interface method, which
     * is invoked on context initialization.
     * @param servletContextEvent income parameter, gives access to external environment.
     * @see javax.servlet.ServletContextEvent
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();
        PictureLogic logic = new PictureLogic();
        List<Country> countries = logic.createCountryList();
        List<Genre> genres = logic.createGenreList();
        List<Director> directors = logic.createDirectorList();
        context.setAttribute(COUNTRIES, countries);
        context.setAttribute(GENRES, genres);
        context.setAttribute(DIRECTORS, directors);
    }

    /** Override {@code ServletContextListener} interface method, which
     * is invoked on context destruction.
     * @param servletContextEvent income parameter, gives access to external environment.
     * @see javax.servlet.ServletContextEvent
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();
        context.removeAttribute(COUNTRIES);
        context.removeAttribute(GENRES);
        context.removeAttribute(DIRECTORS);
    }
}
