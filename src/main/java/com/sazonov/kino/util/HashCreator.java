package com.sazonov.kino.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.TreeMap;

/**
 * Class {@code HashCreator} is util class for MD5 hash creating.
 *
 * @author Sazonov
 * @see java.security.MessageDigest
 * @since JDK7.0
 * @version 1.0
 */
public class HashCreator {
    private static final Logger LOGGER = LogManager.getLogger(HashCreator.class);
    private static final String HASH_TYPE = "MD5";
    private static final String HASH_SALT = "movie";

    /** creates hash of input String value.
     * @param input input String for creating hash value.
     * @return MD5 hash value.
     */
    public String createHash(String input) {
        String hashOfInput = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance(HASH_TYPE);
            md5.update(StandardCharsets.UTF_8.encode(input+HASH_SALT));
            hashOfInput = String.format("%032x", new BigInteger(1, md5.digest()));
        } catch (NoSuchAlgorithmException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return hashOfInput;
    }

    /** creates hash of input map values by appending them to one String.
     * @param map input map of String values.
     * @return MD5 hash value.
     */
    public String mapHashDefiner(Map<String, String[]> map) {
        StringBuilder mapBuffer = new StringBuilder();
        Map<String, String[]> treeMap = new TreeMap<>(map);
        for (String key: treeMap.keySet()) {
            mapBuffer.append(key);
        }
        for (String[] value: treeMap.values()) {
            for(String val: value) {
                mapBuffer.append(val);
            }
        }
        return createHash(mapBuffer.toString());
    }
}
