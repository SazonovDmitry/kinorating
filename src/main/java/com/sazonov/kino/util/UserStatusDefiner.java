package com.sazonov.kino.util;

import com.sazonov.kino.entity.UserStatus;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class {@code UserStatusDefiner} defines user status by input status index.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public class UserStatusDefiner {
    private static final Logger LOGGER = LogManager.getLogger(UserStatusDefiner.class);

    /** Method defines user status by index.
     * @param index user status index.
     * @return {@code UserStatus} object.
     */
    public UserStatus defineStatus(int index) {
        try {
            switch (index) {
                case 1:
                    return UserStatus.KINO_BEGINNER;
                case 2:
                    return UserStatus.KINO_EXPERT;
                case 3:
                    return UserStatus.KINOMAN;
                default:
                    LOGGER.log(Level.WARN, "Unknown user status " + index);
                    return UserStatus.KINO_BEGINNER;
            }
        } catch (IllegalArgumentException exception) {
            LOGGER.log(Level.ERROR, "Unknown user status " + index);
        }
        return UserStatus.KINO_BEGINNER;
    }
}
