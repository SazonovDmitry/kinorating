package com.sazonov.kino.util;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Class {@code LoginPassGenerator} is generator of login and
 * password values.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public class LoginPassGenerator {
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final int INCREMENT = 1;
    private static AtomicLong counter = new AtomicLong();

    /** generate login value.
     * @return login value.
     */
    public static String buildLogin() {
        return LOGIN + counter.getAndAdd(INCREMENT);
    }

    /** generate password value.
     * @return password value.
     */
    public static String buildPassword() {
        return PASSWORD + counter.getAndAdd(INCREMENT);
    }
}
