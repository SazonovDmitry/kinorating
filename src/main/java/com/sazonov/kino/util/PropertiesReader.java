package com.sazonov.kino.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class {@code PropertiesReader} read properties files and create
 * Properties objects.
 *
 * @author Sazonov
 * @see java.util.Properties
 * @since JDK7.0
 * @version 1.0
 */
public class PropertiesReader {
    private final static Logger LOGGER = LogManager.getLogger(PropertiesReader.class);
    private final static String RESOURCE_FOLDER = "properties/";
    private final static String POLL_SIZE_PROPERTIES_FILE = "pool_size.properties";
    private final static String CONNECTION_SETTINGS_FILE = "db_connection.properties";
    private final static String MAIL_SETTINGS_FILE = "mail.properties";

    /** Create database connection properties object.
     * @return {@code Properties} object with connection parameters.
     *
     * @see java.util.Properties
     * @throws java.lang.RuntimeException if properties file reading is failed.
     */
    public Properties createConnectionProperties() {
        return createProperties(CONNECTION_SETTINGS_FILE);
    }

    /** Create pool size properties object.
     * @return {@code Properties} object with connection parameters.
     *
     * @see java.util.Properties
     */
    public Properties createPoolSizeProperties() {
        Properties properties = new Properties();
        try {
            properties = createProperties(POLL_SIZE_PROPERTIES_FILE);
        } catch (RuntimeException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return properties;
    }

    /** Create mail connection properties object.
     * @return {@code Properties} object with connection parameters.
     *
     * @see java.util.Properties
     */
    public Properties createMailProperties() {
        Properties properties = new Properties();
        try {
            properties = createProperties(MAIL_SETTINGS_FILE);
        } catch (RuntimeException exception) {
            LOGGER.log(Level.ERROR, exception);
        }
        return properties;
    }

    /** Create database connection properties object.
     * @return {@code Properties} object with connection parameters.
     *
     * @see java.util.Properties
     */
    private Properties createProperties(String fileName) {
        Properties properties = new Properties();
        try {
            InputStream stream = getClass().getClassLoader().getResourceAsStream(RESOURCE_FOLDER + fileName);
            if (stream != null) {
                properties.load(stream);
            } else {
                throw new RuntimeException();
            }
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        return properties;
    }
}
