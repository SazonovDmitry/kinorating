package com.sazonov.kino.tag;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.taglibs.standard.tag.common.core.Util;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;

/**
 * Class {@code DateFormatTag} is inherit of {@code TagSupport} class.
 * Custom tag for LocalDateTime output.
 *
 * @author Sazonov
 * @see java.time.temporal.Temporal
 * @since JDK8.0
 * @version 1.0
 */
@SuppressWarnings("serial")
public class DateFormatTag extends TagSupport {
    private static final Logger LOGGER = LogManager.getLogger(DateFormatTag.class);
    private static final String EN_DATE_PATTERN = "MM-dd-yyyy";
    private static final String EN_DATE_TIME_PATTERN = "MM-dd-yyyy HH:mm:ss";
    private static final String RU_DATE_PATTERN = "dd.MM.yyyy";
    private static final String RU_DATE_TIME_PATTERN = "dd.MM.yyyy HH:mm:ss";
    private static final String INTERNATIONAL_DATE_PATTERN = "dd-MM-yyyy";
    private static final String INTERNATIONAL_DATE_TIME_PATTERN = "dd-MM-yyyy HH:mm:ss";
    private Temporal value;
    private String pattern;
    private String var;
    private int scope;

    /** Constructs {@code DateFormatTag} object and call init() method.
     */
    public DateFormatTag() {
        init ();
    }

    /** Standard set method for var variable.
     * @param var String variable value to be set.
     *
     */
    public void setVar(final String var) {
        this.var = var;
    }

    /** Standard set method for scope variable.
     * @param scope String scope value to be set.
     *
     */
    public void setScope(final String scope) {
        this.scope = Util.getScope (scope);
    }

    /** Standard set method for scope dateTime variable.
     * @param value Temporal value to be set.
     *
     */
    public void setValue(final Temporal value) {
        this.value = value;
    }

    /** Standard set method for pattern variable.
     * @param pattern String pattern value to be set.
     */
    public void setPattern(final String pattern) {
        this.pattern = pattern;
    }

    /** Override {@code TagSupport} class method, and
     * format income date according ro set locale.
     * @return the number of further action.
     */
    @Override
    public int doStartTag() {
        String formatted;

        if (value == null) {
            if (var != null) {
                pageContext.removeAttribute(var, scope);
            }
            return EVAL_PAGE;
        }

        if (pattern != null) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern (pattern);
            formatted = formatter.format (value);
        } else {
            try {
                String locale = String.valueOf(pageContext.getSession().getAttribute("currentLocale"));
                switch (locale) {
                    case "EN":
                        DateTimeFormatter enFormatter;
                        if (value instanceof LocalDate) {
                            enFormatter = DateTimeFormatter.ofPattern (EN_DATE_PATTERN);
                        } else {
                            enFormatter = DateTimeFormatter.ofPattern (EN_DATE_TIME_PATTERN);
                        }
                        formatted = enFormatter.format(value);
                        break;
                    case "RU":
                        DateTimeFormatter ruFormatter;
                        if (value instanceof LocalDate) {
                            ruFormatter = DateTimeFormatter.ofPattern (RU_DATE_PATTERN);
                        } else {
                            ruFormatter = DateTimeFormatter.ofPattern (RU_DATE_TIME_PATTERN);
                        }
                        formatted = ruFormatter.format (value);
                        break;
                    default:
                        DateTimeFormatter defFormatter;
                        if (value instanceof LocalDate) {
                            defFormatter = DateTimeFormatter.ofPattern (INTERNATIONAL_DATE_PATTERN);
                        } else {
                            defFormatter = DateTimeFormatter.ofPattern (INTERNATIONAL_DATE_TIME_PATTERN);
                        }
                        formatted = defFormatter.format(value);
                }
            } catch (ClassCastException exception) {
                LOGGER.log(Level.ERROR, exception);
                formatted = value.toString();
            }
        }

        if (var != null) {
            pageContext.setAttribute (var, formatted, scope);
        } else {
            try {
                pageContext.getOut().print(formatted);
            } catch (final IOException exception) {
                LOGGER.log(Level.ERROR, exception);
            }
        }

        return SKIP_BODY;
    }

    /** action after reaching of the edn of tag body.
     */
    @Override
    public int doEndTag() {
        return EVAL_PAGE;
    }

    /** calls init() method to initialize object fields with default values
     */
    @Override
    public void release() {
        init ();
    }

    /** initialize object fields with default values
     */
    private void init() {
        pattern = null;
        var = null;
        value = null;
        scope = PageContext.REQUEST_SCOPE;
    }
}
