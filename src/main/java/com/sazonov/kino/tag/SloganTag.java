package com.sazonov.kino.tag;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Class {@code SloganTag} is inherit of {@code TagSupport} class.
 * Custom tag for slogan output.
 *
 * @author Sazonov
 * @since JDK8.0
 * @version 1.0
 */
@SuppressWarnings("serial")
public class SloganTag extends TagSupport {
    private static final Logger LOGGER = LogManager.getLogger(SloganTag.class);
    private static final String BRACKETS_OPEN = "«";
    private static final String BRACKETS_CLOSE = "»";
    private static final String BRACKETS = "\"";
    private static final String EMPTY_CHAR = "";
    private static final String DEFAULT_VALUE = "-";
    private String value;

    /** Constructs {@code SloganTag} object and call init() method.
     */
    public SloganTag() {
        init ();
    }

    /** Standard set method for value variable.
     * @param value String variable value to be set.
     *
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /** Override {@code TagSupport} class method, and
     * format income slogan according output format.
     * @return the number of further action.
     */
    @Override
    public int doStartTag() {
        String slogan;

        if (value == null || value.isEmpty()) {
            slogan = DEFAULT_VALUE;
        } else {
            slogan = value.trim().replace(BRACKETS, EMPTY_CHAR);
            String firstSymbol = String.valueOf(slogan.charAt(0));
            String lastSymbol = String.valueOf(slogan.charAt(slogan.length()-1));
            if (!BRACKETS_OPEN.equals(firstSymbol)) {
                slogan = BRACKETS_OPEN + slogan;
            }
            if (!BRACKETS_CLOSE.equals(lastSymbol)) {
                slogan = slogan + BRACKETS_CLOSE;
            }
        }

        try {
            pageContext.getOut().print(slogan);
        } catch (final IOException exception) {
            LOGGER.log(Level.ERROR, exception);
        }

        return SKIP_BODY;
    }

    /** action after reaching of the edn of tag body.
     */
    @Override
    public int doEndTag() {
        return EVAL_PAGE;
    }

    /** calls init() method to initialize object fields with default values
     */
    @Override
    public void release() {
        init ();
    }

    /** initialize object fields with default values
     */
    private void init() {
        value = null;
    }

}
