package com.sazonov.kino.tag;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.Locale;

/**
 * Class {@code LocaleTag} is inherit of {@code TagSupport} class.
 * Custom tag for locale setting.
 *
 * @author Sazonov
 * @since JDK8.0
 * @version 1.0
 */
public class LocaleTag extends TagSupport {
    private static final String LOCALE_RU = "RU";
    private static final String LOCALE_EN = "EN";
    private static final String LOCALE_BY = "BY";
    private static final String US_COUNTRY = "US";
    private static final String LANG_RU = "ru";
    private static final String LANG_EN = "en";
    private static final String LANG_BY = "be";
    private static final String CURRENT_LOCALE = "currentLocale";
    private String value;

    /** Constructs {@code LocaleTag} object and call init() method.
     */
    public LocaleTag() {
        init ();
    }

    /** Standard set method for value variable.
     * @param value String variable value to be set.
     *
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /** Override {@code TagSupport} class method, and
     * set new locale for JSP.
     * @return the number of further action.
     */
    @Override
    public int doStartTag() {
        HttpSession session = pageContext.getSession();
       if (value == null) {
           session.setAttribute(CURRENT_LOCALE, LOCALE_RU);
       } else {
           switch (value) {
               case LOCALE_RU:
                   Config.set(session, Config.FMT_LOCALE, new Locale(LANG_RU, LOCALE_RU));
                   break;
               case LOCALE_BY:
                   Config.set(session, Config.FMT_LOCALE, new Locale(LANG_BY, LOCALE_BY));
                   break;
               case LOCALE_EN:
                   Config.set(session, Config.FMT_LOCALE, new Locale(LANG_EN, US_COUNTRY));
                   break;
                   default:
                       Config.set(session, Config.FMT_LOCALE, new Locale(LANG_RU, LOCALE_RU));
           }
       }

        return SKIP_BODY;
    }

    /** action after reaching of the edn of tag body.
     */
    @Override
    public int doEndTag() {
        return EVAL_PAGE;
    }

    /** calls init() method to initialize object fields with default values
     */
    @Override
    public void release() {
        init ();
    }

    /** initialize object fields with default values
     */
    private void init() {
        value = null;
    }

}
