package com.sazonov.kino.servlet;

import com.sazonov.kino.entity.Director;
import com.sazonov.kino.entity.Picture;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Class {@code ImageViewer} is inherit of {@code HttpServlet} class.
 * It provides displaying pictures on the screen.
 *
 * @author Sazonov
 * @see javax.servlet.http.HttpServlet
 * @since JDK7.0
 * @version 1.0
 */
@WebServlet(name = "imageViewer", urlPatterns = {"/imageViewer"})
public class ImageViewer extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(ImageViewer.class);
    private static final String TYPE_PARAMETER = "type";
    private static final String TYPE_FILM = "film";
    private static final String TYPE_DIRECTOR = "director";

    /** Constructs {@code ImageViewer} object by default.
     */
    public ImageViewer() {
    }

    /** Override {@code HttpServlet} class method, which processes income request
     * and writes image information to response object.
     * @param request income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @param response income parameter, passed from project controller and
     *                contains all necessary information for output page.
     * @see javax.servlet.http.HttpServletRequest
     * @see javax.servlet.http.HttpServletResponse
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("image/jpeg");
        HttpSession session = request.getSession();

        byte[] image = null;
        String type = request.getParameter(TYPE_PARAMETER);
        try {
            switch (type) {
                case TYPE_FILM:
                    Picture picture = (Picture)session.getAttribute("currentPicture");
                    image = picture.getImage();
                    break;
                case TYPE_DIRECTOR:
                    Director director = (Director)session.getAttribute("currentDirector");
                    image = director.getPhoto();
                    break;
                default:
                    LOGGER.log(Level.DEBUG, "Can't define image type.");
            }
        } catch (ClassCastException|IllegalArgumentException exception) {
            LOGGER.log(Level.ERROR, "Can't display image.", exception);
        }
        displayPicture(response, image);
    }

    /** writes image data to response.
     * @param response income parameter, passed from project controller and
     *                contains all necessary information for output page defining.
     * @param image output image as a byte array.
     * @see javax.servlet.http.HttpServletResponse
     */
    private void displayPicture(HttpServletResponse response, byte[] image) {
        if (image!=null) {
            try (OutputStream outputStream = response.getOutputStream()){
                response.setContentLength(image.length);
                outputStream.write(image);
                outputStream.flush();
            } catch (IOException|ClassCastException exception) {
                LOGGER.log(Level.ERROR, "Can't display image.", exception);
            }
        }
    }
}
