package com.sazonov.kino.dao;

import com.sazonov.kino.connection.ConnectionPool;
import com.sazonov.kino.entity.*;
import com.sazonov.kino.exception.DaoException;
import com.sazonov.kino.exception.WrapperConnectionException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code CountryDAO} is realisation of {@code AbstractDAO} abstract class.
 * It provides user database actions on {@code Country} entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.dao.AbstractDAO
 * @see com.sazonov.kino.entity.Country
 * @since JDK8.0
 * @version 1.0
 */
public class CountryDAO extends AbstractDAO<Integer, Country> {
    private static final Logger LOGGER = LogManager.getLogger(CountryDAO.class);
    private static final String FIND_COUNTRY_BY_ID = "SELECT c_uid, c_name FROM country WHERE c_uid = ?;";
    private static final String DELETE_COUNTRY_BY_ID = "DELETE FROM country WHERE c_uid = ?;";
    private static final String FIND_COUNTRY_BY_PICTURE = "SELECT c_uid, c_name FROM country JOIN picture_m2m_country " +
            "ON picture_p_uid = ? AND c_uid = country_c_uid;";
    private static final String FIND_ALL_COUNTRIES = "SELECT c_uid, c_name FROM country;";
    private static final String CREATE_COUNTRY = "INSERT INTO country(c_uid, c_name) VALUES (?, ?);";
    private static final String UPDATE_COUNTRY_PARAMS = "UPDATE country SET c_name = ? WHERE c_uid = ?;";
    private static final String INSERT_PICTURE_COUNTRIES = "INSERT INTO picture_m2m_country(picture_p_uid, country_c_uid) " +
            "VALUES (?, ?);";

    /** Constructs {@code CountryDAO} object and initialize connection field by getting connection from
     * connection pool.
     *
     *@see com.sazonov.kino.dao.AbstractDAO
     * @see com.sazonov.kino.entity.Country
     */
    public CountryDAO() { this.connection = ConnectionPool.getInstance().getConnection();}

    /**
     * Creates country entity using id parameter.
     * @param countryId uniquely identifies entity.
     * @return created object of {@code Country} type with given id.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity
     */
    @Override
    public Country findEntityById(Integer countryId) throws DaoException {
        Country country = null;

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_COUNTRY_BY_ID);

            statement.setInt(1, countryId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No country with id " + countryId + " found.");
                return country;
            }

            country = new Country();
            country.setCountryId(countryId);
            if (resultSet.next()){
                country.setName(resultSet.getString("c_name"));
            }
        } catch (WrapperConnectionException |SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return country;
    }

    /**
     * Deletes country information using id parameter.
     * @param countryId uniquely identifies country.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Country
     */
    @Override
    public boolean delete(Integer countryId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(DELETE_COUNTRY_BY_ID);

            statement.setInt(1, countryId);
            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "Country with id " + countryId + " wasn't deleted.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException | SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Creates list of countries using picture id parameter.
     * @param pictureId uniquely identifies picture for selection.
     * @return list of objects of class {@code Country}.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Country
     */
    public List<Country> findCountriesByPicture(int pictureId) throws DaoException {
        List<Country> countries = new ArrayList<>();

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_COUNTRY_BY_PICTURE);

            statement.setInt(1, pictureId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No countries on picture with id " + pictureId + " found.");
                return countries;
            }

            while(resultSet.next()) {
                Country country = new Country();
                country.setCountryId(resultSet.getInt("c_uid"));
                country.setName(resultSet.getString("c_name"));

                countries.add(country);
            }
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return countries;
    }

    /**
     * Writes entity information to database.
     * @param country object of {@code Country} type.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Country
     */
    @Override
    public boolean create(Country country) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(CREATE_COUNTRY);

            statement.setInt(1, country.getCountryId());
            statement.setString(2, country.getName());

            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "Country " + country.getName() + " wasn't created.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Updates entity information in database.
     * @param country object of {@code Country} type.
     * @return updated {@code Country} object.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Country
     */
    @Override
    public Country update(Country country) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(UPDATE_COUNTRY_PARAMS);

            statement.setString(1, country.getName());
            statement.setInt(2, country.getCountryId());

            int result = statement.executeUpdate();

            if (result != 1) {
                throw new DaoException("Country with id " + country.getCountryId() + " parameters wasn't changed.");
            }
            return country;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Creates list of all countries represented in database.
     * @return list of objects of class {@code Country}.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Country
     */
    public List<Country> findAll() throws DaoException {
        List<Country> countries = new ArrayList<>();

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_ALL_COUNTRIES);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No countries found.");
                return countries;
            }

            while(resultSet.next()) {
                Country country = new Country();
                country.setName(resultSet.getString("c_name"));
                country.setCountryId(resultSet.getInt("c_uid"));
                countries.add(country);
            }
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return countries;
    }

    /**
     * Inserts to database information about countries, added to certain picture.
     * @return true if action succeed and false if failed.
     * @param countriesId list of objects of type {@code Country}.
     * @param pictureId uniquely identifies picture.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Country
     */
    public boolean insertPictureCountries(List<Integer> countriesId, int pictureId) throws DaoException {
        PreparedStatement statement = null;
        try {
            if (connection.getAutoCommit()) {
                connection.setAutoCommit(false);
            }

            for (Integer countryId: countriesId) {
                statement = connection.getPreparedStatement(INSERT_PICTURE_COUNTRIES);
                statement.setInt(1, pictureId);
                statement.setInt(2, countryId);

                int result = statement.executeUpdate();

                if (result != 1) {
                    LOGGER.log(Level.DEBUG, "Countries wasn't added.");
                    connection.releaseRollBack();
                    return false;
                }
            }
            connection.releaseCommit();
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            connection.releaseRollBack();
            throw new DaoException(exception);
        } finally {
            connection.setAutoCommit(true);
            closeStatement(statement);
            releaseConnection();
        }
    }
}
