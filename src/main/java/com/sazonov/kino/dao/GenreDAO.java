package com.sazonov.kino.dao;


import com.sazonov.kino.connection.ConnectionPool;
import com.sazonov.kino.entity.Genre;
import com.sazonov.kino.exception.DaoException;
import com.sazonov.kino.exception.WrapperConnectionException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code GenreDAO} is realisation of {@code AbstractDAO} abstract class.
 * It provides user database actions on {@code Genre} entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.dao.AbstractDAO
 * @see com.sazonov.kino.entity.Genre
 * @since JDK8.0
 * @version 1.0
 */
public class GenreDAO extends AbstractDAO<Integer, Genre> {
    private static final Logger LOGGER = LogManager.getLogger(GenreDAO.class);
    private static final String FIND_GENRE_BY_ID = "SELECT g_uid, g_name FROM genre WHERE g_uid = ?;";
    private static final String DELETE_GENRE_BY_ID = "DELETE FROM genre WHERE g_uid = ?;";
    private static final String FIND_GENRES_BY_PICTURE = "SELECT g_uid, g_name FROM genre JOIN picture_m2m_genre " +
            "ON picture_p_uid = ? AND g_uid = genre_g_uid;";
    private static final String FIND_ALL_GENRES = "SELECT g_uid, g_name FROM genre;";
    private static final String CREATE_GENRE = "INSERT INTO genre(g_uid, g_name) VALUES (null, ?);";
    private static final String UPDATE_GENRE = "UPDATE genre SET g_name = ? WHERE g_uid = ?;";
    private static final String INSERT_PICTURE_GENRES = "INSERT INTO picture_m2m_genre(picture_p_uid, genre_g_uid) " +
            "VALUES (?, ?);";

    /** Constructs {@code GenreDAO} object and initialize connection field by getting connection from
     * connection pool.
     *
     * @see com.sazonov.kino.dao.AbstractDAO
     * @see com.sazonov.kino.entity.Genre
     */
    public GenreDAO() { this.connection = ConnectionPool.getInstance().getConnection();
    }

    /**
     * Creates genre entity using id parameter.
     * @param genreId uniquely identifies director.
     * @return created object of {@code Genre} type with given id.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Genre
     */
    @Override
    public Genre findEntityById(Integer genreId) throws DaoException {
        Genre genre = null;

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_GENRE_BY_ID);

            statement.setInt(1, genreId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No genre with id " + genreId + " found.");
                return genre;
            }

            genre = new Genre();
            genre.setGenreId(genreId);
            if (resultSet.next()){
                genre.setName(resultSet.getString("g_name"));
            }
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return genre;
    }

    /**
     * Deletes genre information using id parameter.
     * @param genreId uniquely identifies director.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Genre
     */
    @Override
    public boolean delete(Integer genreId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(DELETE_GENRE_BY_ID);

            statement.setInt(1, genreId);
            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "Genre with id " + genreId + " wasn't deleted.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException | SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Creates list of genres using picture id parameter.
     * @param pictureId uniquely identifies picture for selection.
     * @return list of objects of class {@code Genre}.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Genre
     */
    public List<Genre> findGenresByPicture(int pictureId) throws DaoException {
        List<Genre> genres = new ArrayList<>();

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_GENRES_BY_PICTURE);

            statement.setInt(1, pictureId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No genres on picture with id " + pictureId + " found.");
                return genres;
            }

            while(resultSet.next()) {
                Genre genre = new Genre();
                genre.setGenreId(resultSet.getInt("g_uid"));
                genre.setName(resultSet.getString("g_name"));

                genres.add(genre);
            }
        } catch (WrapperConnectionException |SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return genres;
    }

    /**
     * Writes entity information to database.
     * @param genre object of {@code Genre} type.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Genre
     */
    @Override
    public boolean create(Genre genre) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(CREATE_GENRE);
            statement.setString(1, genre.getName());

            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "Country " + genre.getName() + " wasn't created.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Updates entity information in database.
     * @param genre object of {@code Genre} type.
     * @return updated {@code Genre} object.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Genre
     */
    @Override
    public Genre update(Genre genre) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(UPDATE_GENRE);

            statement.setString(1, genre.getName());
            statement.setInt(2, genre.getGenreId());

            int result = statement.executeUpdate();

            if (result != 1) {
                throw new DaoException("Genre with id " + genre.getGenreId() + " parameters wasn't changed.");
            }
            return genre;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Creates list of all genres represented in database.
     * @return list of objects of class {@code Genre}.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Genre
     */
    public List<Genre> findAll() throws DaoException {
        List<Genre> genres = new ArrayList<>();

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_ALL_GENRES);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No genres found.");
                return genres;
            }

            while(resultSet.next()) {
                Genre genre = new Genre();
                genre.setName(resultSet.getString("g_name"));
                genre.setGenreId(resultSet.getInt("g_uid"));
                genres.add(genre);
            }
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return genres;
    }

    /**
     * Inserts to database information about genres, added to certain picture.
     * @return true if action succeed and false if failed.
     * @param genresId list of objects of type {@code Genre}.
     * @param pictureId uniquely identifies picture.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Genre
     */
    public boolean insertPictureGenres(List<Integer> genresId, int pictureId) throws DaoException {
        PreparedStatement statement = null;
        try {
            if (connection.getAutoCommit()) {
                connection.setAutoCommit(false);
            }
            for (Integer genreId: genresId) {
                statement = connection.getPreparedStatement(INSERT_PICTURE_GENRES);
                statement.setInt(1, pictureId);
                statement.setInt(2, genreId);
                int result = statement.executeUpdate();

                if (result != 1) {
                    LOGGER.log(Level.DEBUG, "Genres wasn't added.");
                    connection.releaseRollBack();
                    return false;
                }
            }
            connection.releaseCommit();

            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            connection.releaseRollBack();
            throw new DaoException(exception);
        } finally {
            connection.setAutoCommit(true);
            closeStatement(statement);
            releaseConnection();
        }
    }

}
