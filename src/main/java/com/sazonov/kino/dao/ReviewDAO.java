package com.sazonov.kino.dao;

import com.sazonov.kino.connection.ConnectionPool;
import com.sazonov.kino.entity.Review;
import com.sazonov.kino.entity.UserStatus;
import com.sazonov.kino.exception.DaoException;
import com.sazonov.kino.exception.WrapperConnectionException;
import com.sazonov.kino.util.UserStatusDefiner;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code ReviewDAO} is realisation of {@code AbstractDAO} abstract class.
 * It provides user database actions on {@code Review} entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.dao.AbstractDAO
 * @see com.sazonov.kino.entity.Review
 * @since JDK8.0
 * @version 1.0
 */
public class ReviewDAO extends AbstractDAO<Integer, Review> {
    private static final Logger LOGGER = LogManager.getLogger(ReviewDAO.class);
    private static final String FIND_REVIEW_BY_ID = "SELECT rw_uid, rw_content, rw_date, picture_p_uid, u_name, user_u_uid, " +
            "u_status FROM review JOIN user ON rw_uid = ? AND user_u_uid = u_uid;";
    private static final String DELETE_REVIEW_BY_ID = "DELETE FROM review WHERE rw_uid = ?;";
    private static final String FIND_REVIEW_BY_PICTURE = "SELECT rw_uid, rw_content, rw_date, picture_p_uid, u_name, user_u_uid, " +
            "u_status FROM review JOIN user ON picture_p_uid = ? AND user_u_uid = u_uid;";
    private static final String FIND_REVIEWS_BY_DATE = "SELECT rw_uid, rw_content, rw_date, picture_p_uid, u_name, user_u_uid, " +
            "u_status FROM review JOIN user ON user_u_uid = u_uid AND rw_date >= ? and rw_date <= ?;";
    private static final String CREATE_REVIEW = "INSERT INTO review(rw_uid, rw_content, rw_date, user_u_uid, picture_p_uid) " +
            "VALUES (null, ?, ?, ?, ?);";
    private static final String UPDATE_REVIEW = "UPDATE review SET rw_content = ?, rw_date = ?, user_u_uid = ?, picture_p_uid = ? " +
            "WHERE rw_uid = ?;";
    private static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss.S";
    private static final String DATETIME_WRITE_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final int DATETIME_DISPLACEMENT = 3;

    /** Constructs {@code ReviewDAO} object and initialize connection field by getting connection from
     * connection pool.
     *
     * @see com.sazonov.kino.dao.AbstractDAO
     * @see com.sazonov.kino.entity.Review
     */
    public ReviewDAO() { this.connection = ConnectionPool.getInstance().getConnection(); }

    /**
     * Creates review entity using id parameter.
     * @param reviewId uniquely identifies review.
     * @return created object of {@code Review} type with given id.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Review
     */
    @Override
    public Review findEntityById(Integer reviewId) throws DaoException {
        Review review = null;

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_REVIEW_BY_ID);

            statement.setInt(1, reviewId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No review with id " + reviewId + " found.");
                return review;
            }

            review = new Review();
            review.setReviewId(reviewId);
            if (resultSet.next()){
                review.setContent(resultSet.getString("rw_content"));
                review.setPictureId(resultSet.getInt("picture_p_uid"));
                review.setUserName(resultSet.getString("u_name"));
                review.setUserId(resultSet.getInt("user_u_uid"));
                int statusIndex = resultSet.getInt("u_status");
                UserStatusDefiner statusDefiner = new UserStatusDefiner();
                UserStatus status = statusDefiner.defineStatus(statusIndex);
                if (status != null) {
                    review.setStatus(status);
                }

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_PATTERN);
                LocalDateTime time = LocalDateTime.parse((resultSet.getTimestamp("rw_date"))
                        .toString(), formatter).minusHours(DATETIME_DISPLACEMENT);
                review.setDateTime(time);
            }
        } catch (WrapperConnectionException |SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return review;
    }

    /**
     * Deletes review information using id parameter.
     * @param reviewId uniquely identifies review.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Review
     */
    @Override
    public boolean delete(Integer reviewId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(DELETE_REVIEW_BY_ID);

            statement.setInt(1, reviewId);
            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "Review with id " + reviewId + " wasn't deleted.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Creates list of reviews using picture id parameter.
     * @param pictureId uniquely identifies picture for selection.
     * @return list of objects of class {@code Review}.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Review
     */
    public List<Review> findReviewsByPicture(int pictureId) throws DaoException {
        List<Review> reviews = new ArrayList<>();

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_REVIEW_BY_PICTURE);

            statement.setInt(1, pictureId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No reviews on picture with id " + pictureId + " found.");
                return reviews;
            }

            while(resultSet.next()) {
                Review review = new Review();
                review.setReviewId(resultSet.getInt("rw_uid"));
                review.setContent(resultSet.getString("rw_content"));
                review.setPictureId(resultSet.getInt("picture_p_uid"));
                review.setUserName(resultSet.getString("u_name"));
                review.setUserId(resultSet.getInt("user_u_uid"));
                int statusIndex = resultSet.getInt("u_status");
                UserStatusDefiner statusDefiner = new UserStatusDefiner();
                UserStatus status = statusDefiner.defineStatus(statusIndex);
                if (status != null) {
                    review.setStatus(status);
                }

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_PATTERN);
                LocalDateTime time = LocalDateTime.parse((resultSet.getTimestamp("rw_date"))
                        .toString(), formatter).minusHours(DATETIME_DISPLACEMENT);
                review.setDateTime(time);

                reviews.add(review);
            }
        } catch (WrapperConnectionException |SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return reviews;
    }

    /**
     * Writes entity information to database.
     * @param review object of {@code Review} type.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Review
     */
    @Override
    public boolean create(Review review) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(CREATE_REVIEW);

            statement.setString(1, review.getContent());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_WRITE_PATTERN);
            LocalDateTime dateTime = LocalDateTime.now();
            String currentDateTime = dateTime.format(formatter);
            statement.setString(2, currentDateTime);

            statement.setInt(3, review.getUserId());
            statement.setInt(4, review.getPictureId());

            int result = statement.executeUpdate();

                if (result != 1) {
                LOGGER.log(Level.DEBUG, "Review by" + review.getUserName() + " wasn't created.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Updates entity information in database.
     * @param review object of {@code Review} type.
     * @return updated {@code Review} object.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Review
     */
    @Override
    public Review update(Review review) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(UPDATE_REVIEW);
            statement.setString(1, review.getContent());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_WRITE_PATTERN);
            LocalDateTime reviewDateTime = review.getDateTime();
            String dateTime = reviewDateTime.format(formatter);
            statement.setString(2, dateTime);

            statement.setInt(3, review.getUserId());
            statement.setInt(4, review.getPictureId());

            statement.setInt(5, review.getReviewId());

            int result = statement.executeUpdate();

            if (result != 1) {
                throw new DaoException("Review with id " + review.getReviewId() + " content wasn't changed.");
            }
            return review;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Creates list of reviews in given time frames.
     * @param from sets the start date frame.
     * @param to sets the end date frame.
     * @return list of objects of class {@code Review} in given time frames.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Review
     */
    public List<Review> findReviewsByQuery(LocalDate from, LocalDate to) throws DaoException {
        List<Review> reviews = new ArrayList<>();

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_REVIEWS_BY_DATE);

            LocalDateTime fromTime = LocalDateTime.of(from, LocalTime.MIN);
            LocalDateTime toTime = LocalDateTime.of(to, LocalTime.MAX);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_WRITE_PATTERN);
            String fromDateTime = fromTime.format(formatter);
            String toDateTime = toTime.format(formatter);
            statement.setString(1, fromDateTime);
            statement.setString(2, toDateTime);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No reviews found.");
                return reviews;
            }

            while(resultSet.next()) {
                Review review = new Review();
                review.setReviewId(resultSet.getInt("rw_uid"));
                review.setPictureId(resultSet.getInt("picture_p_uid"));
                review.setUserName(resultSet.getString("u_name"));
                review.setContent(resultSet.getString("rw_content"));
                review.setUserId(resultSet.getInt("user_u_uid"));
                int statusIndex = resultSet.getInt("u_status");
                UserStatusDefiner statusDefiner = new UserStatusDefiner();
                UserStatus status = statusDefiner.defineStatus(statusIndex);
                if (status != null) {
                    review.setStatus(status);
                }

                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATETIME_PATTERN);
                LocalDateTime time = LocalDateTime.parse((resultSet.getTimestamp("rw_date"))
                        .toString(), dateTimeFormatter).minusHours(DATETIME_DISPLACEMENT);
                review.setDateTime(time);

                reviews.add(review);
            }
        } catch (WrapperConnectionException |SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return reviews;
    }
}
