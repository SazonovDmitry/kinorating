package com.sazonov.kino.dao;

import com.sazonov.kino.connection.ConnectionPool;
import com.sazonov.kino.entity.Picture;
import com.sazonov.kino.entity.PictureType;
import com.sazonov.kino.exception.DaoException;
import com.sazonov.kino.exception.WrapperConnectionException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code PictureDAO} is realisation of {@code AbstractDAO} abstract class.
 * It provides user database actions on {@code Picture} entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.dao.AbstractDAO
 * @see com.sazonov.kino.entity.Picture
 * @since JDK8.0
 * @version 1.0
 */
public class PictureDAO extends AbstractDAO<Integer, Picture> {
    private static final Logger LOGGER = LogManager.getLogger(PictureDAO.class);
    private static final String FIND_PICTURE_BY_ID = "SELECT p_type, p_parts, p_name, p_slogan, p_duration, p_budget, p_image, " +
            "p_rating, p_issue_year, p_age_limit, p_premiere_date, (AVG(rating.r_value) + picture.p_rating)/2 AS rate FROM picture " +
            "LEFT JOIN rating ON picture.p_uid = rating.picture_p_uid WHERE p_uid = ?;";
    private static final String CREATE_PICTURE = "INSERT INTO picture(p_uid, p_type, p_parts, p_name, p_slogan, p_duration, " +
            "p_budget, p_image, p_rating, p_issue_year, p_age_limit, p_premiere_date) VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String DELETE_PICTURE = "DELETE FROM picture WHERE picture.p_uid = ?;";
    private static final String UPDATE_PICTURE = "UPDATE picture SET p_type = ?, p_parts = ?, p_name = ?, p_slogan = ?, p_duration = ?," +
            " p_budget = ?, p_image = ?, p_rating = ?, p_issue_year = ?, p_age_limit = ?, p_premiere_date = ? WHERE p_uid = ?;";
    private static final String FIND_ALL_PICTURES = "SELECT p_uid, p_type , p_parts, p_name, p_slogan, p_duration, p_budget, p_image, " +
            "p_rating, p_issue_year, p_age_limit, p_premiere_date, (AVG(r_value) + picture.p_rating)/2 AS rate FROM picture " +
            "LEFT JOIN rating ON picture.p_uid = rating.picture_p_uid GROUP BY picture.p_uid;";
    private static final String FIND_FAVORITE_LIST = "SELECT p_uid, p_type , p_parts, p_name, p_slogan, p_duration, p_budget, " +
            "p_image, p_rating, p_issue_year, p_age_limit, p_premiere_date, (AVG(r_value) + picture.p_rating)/2 AS rate " +
            "FROM picture LEFT JOIN rating ON picture.p_uid = rating.picture_p_uid GROUP BY picture.p_uid HAVING picture.p_uid " +
            "IN (SELECT picture_p_uid FROM picture_m2m_user WHERE user_u_uid = ?);";
    private static final String RATE_PICTURE = "INSERT INTO rating(picture_p_uid, user_u_uid, r_value) VALUES (?, ?, ?);";
    private static final String AVERAGE_PICTURE_RATE_USER = "SELECT AVG(r_value) AS average FROM rating WHERE picture_p_uid " +
            "IN (SELECT picture_p_uid FROM rating WHERE user_u_uid = ?);";
    private static final String NUMBER_OF_RATES = "SELECT COUNT(r_value) AS rates FROM rating WHERE picture_p_uid " +
            "IN (SELECT picture_p_uid FROM rating WHERE user_u_uid = ?);";
    private static final String FIND_PICTURE_ID = "SELECT p_uid FROM picture WHERE p_type = ? AND p_parts = ? AND p_name = ? AND p_slogan = ? "  +
            "AND p_duration = ? AND p_budget = ? AND p_image = ? AND p_issue_year = ? AND" +
            " p_age_limit = ? AND p_premiere_date = ?;";
    private static final String ISSUE_DATE_PATTERN = "yyyy-MM-dd";
    private static final int PRECISION = 2;

    /** Constructs {@code PictureDAO} object and initialize connection field by getting connection from
     * connection pool.
     *
     * @see com.sazonov.kino.dao.AbstractDAO
     * @see com.sazonov.kino.entity.Picture
     */
    public PictureDAO() { this.connection = ConnectionPool.getInstance().getConnection(); }

    /**
     * Creates picture entity using id parameter.
     * @param pictureId uniquely identifies director.
     * @return created object of {@code Picture} type with given id.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Picture
     */
    @Override
    public Picture findEntityById(Integer pictureId) throws DaoException {

        Picture picture = null;

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_PICTURE_BY_ID);

            statement.setInt(1, pictureId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No picture with id " + pictureId + " found.");
                return picture;
            }

            picture = new Picture();

            if (resultSet.next()){
                picture.setPictureId(pictureId);
                String pictureType = resultSet.getString("p_type");
                PictureType type = PictureType.valueOf(pictureType.toUpperCase());
                picture.setPictureType(type);
                picture.setParts(resultSet.getInt("p_parts"));
                picture.setName(resultSet.getString("p_name"));
                picture.setSlogan(resultSet.getString("p_slogan"));
                picture.setDuration(resultSet.getInt("p_duration"));
                picture.setBudget(resultSet.getLong("p_budget"));

                byte[] image = resultSet.getBytes("p_image");
                picture.setImage(image);

                double rating = resultSet.getDouble("rate");
                if (rating == 0) {
                    rating = resultSet.getDouble("p_rating");
                }

                picture.setRating(round(rating));

                picture.setYear((resultSet.getInt("p_issue_year")));

                picture.setAgeLimit(resultSet.getByte("p_age_limit"));

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ISSUE_DATE_PATTERN);
                LocalDate issueDate = LocalDate.parse((resultSet.getDate("p_premiere_date")).toString(), formatter);
                picture.setPremiereDate(issueDate);

                CountryDAO countryDAO = new CountryDAO();
                picture.setCountries(countryDAO.findCountriesByPicture(pictureId));
                DirectorDAO directorDAO = new DirectorDAO();
                picture.setDirectors(directorDAO.findDirectorByPicture(pictureId));
                GenreDAO genreDAO = new GenreDAO();
                picture.setGenres(genreDAO.findGenresByPicture(pictureId));
                ReviewDAO reviewDAO = new ReviewDAO();
                picture.setReviews(reviewDAO.findReviewsByPicture(pictureId));
            }

        } catch (WrapperConnectionException|SQLException|IllegalArgumentException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return picture;
    }

    /**
     * Writes entity information to database.
     * @param picture object of {@code Picture} type.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Picture
     */
    @Override
    public boolean create(Picture picture) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(CREATE_PICTURE);

            statement.setString(1, picture.getPictureType().toString().toLowerCase());
            statement.setInt(2, picture.getParts());
            statement.setString(3, picture.getName());
            statement.setString(4, picture.getSlogan());
            statement.setInt(5, picture.getDuration());
            statement.setLong(6, picture.getBudget());

            InputStream image = new ByteArrayInputStream(picture.getImage());
            statement.setBlob(7, image);

            statement.setDouble(8, picture.getRating());

            statement.setInt(9, picture.getYear());

            statement.setByte(10, picture.getAgeLimit());

            statement.setString(11, picture.getPremiereDate().toString());

            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "Picture " + picture.getName() + " wasn't created.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Deletes picture information using id parameter.
     * @param pictureId uniquely identifies picture.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Picture
     */
    @Override
    public boolean delete(Integer pictureId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(DELETE_PICTURE);

            statement.setInt(1, pictureId);
            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "Picture with id " + pictureId + " wasn't deleted.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException | SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Updates entity information in database.
     * @param picture object of {@code Picture} type.
     * @return updated {@code Picture} object.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Picture
     */
    @Override
    public Picture update(Picture picture) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(UPDATE_PICTURE);

            statement.setString(1, picture.getPictureType().toString().toLowerCase());
            statement.setInt(2, picture.getParts());
            statement.setString(3, picture.getName());
            statement.setString(4, picture.getSlogan());
            statement.setInt(5, picture.getDuration());
            statement.setLong(6, picture.getBudget());

            InputStream image = new ByteArrayInputStream(picture.getImage());
            statement.setBlob(7, image);

            statement.setDouble(8, picture.getRating());

            statement.setInt(9, picture.getYear());

            statement.setByte(10, picture.getAgeLimit());

            statement.setString(11, picture.getPremiereDate().toString());

            statement.setInt(12, picture.getPictureId());

            int result = statement.executeUpdate();

            if (result != 1) {
                throw new DaoException("Picture with id " + picture.getPictureId() + " content wasn't changed.");
            }
            return picture;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Creates list of all pictures represented in database.
     * @return list of objects of class {@code Picture}.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Picture
     */
    public List<Picture> findAllPictures() throws DaoException {
        List<Picture> pictures = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_ALL_PICTURES);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "Impossible to fulfill pictures collection.");
                return pictures;
            }

            while(resultSet.next()) {
                Picture picture = new Picture();

                picture.setPictureId(resultSet.getInt("p_uid"));
                String pictureType = resultSet.getString("p_type");
                PictureType type = PictureType.valueOf(pictureType.toUpperCase());
                picture.setPictureType(type);
                picture.setParts(resultSet.getInt("p_parts"));
                picture.setName(resultSet.getString("p_name"));
                picture.setSlogan(resultSet.getString("p_slogan"));
                picture.setDuration(resultSet.getInt("p_duration"));
                picture.setBudget(resultSet.getLong("p_budget"));

                byte[] image = resultSet.getBytes("p_image");
                picture.setImage(image);

                double rating = resultSet.getDouble("rate");
                if (rating == 0) {
                    rating = resultSet.getDouble("p_rating");
                }
                picture.setRating(round(rating));

                picture.setYear(resultSet.getInt("p_issue_year"));

                picture.setAgeLimit(resultSet.getByte("p_age_limit"));

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ISSUE_DATE_PATTERN);
                LocalDate issueDate = LocalDate.parse((resultSet.getDate("p_premiere_date")).toString(), formatter);
                picture.setPremiereDate(issueDate);

                CountryDAO countryDAO = new CountryDAO();
                picture.setCountries(countryDAO.findCountriesByPicture(picture.getPictureId()));
                DirectorDAO directorDAO = new DirectorDAO();
                picture.setDirectors(directorDAO.findDirectorByPicture(picture.getPictureId()));
                GenreDAO genreDAO = new GenreDAO();
                picture.setGenres(genreDAO.findGenresByPicture(picture.getPictureId()));
                ReviewDAO reviewDAO = new ReviewDAO();
                picture.setReviews(reviewDAO.findReviewsByPicture(picture.getPictureId()));

                pictures.add(picture);
            }
        } catch (WrapperConnectionException |SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return pictures;
    }

    /**
     * Creates list of certain user favorite picture.
     * @return list of objects of class {@code Picture} related to current user.
     * @param userId user which favorite list is been found id .
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Picture
     */
    public List<Picture> findFavoriteList(int userId) throws DaoException {
        List<Picture> pictures = new ArrayList<>();
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_FAVORITE_LIST);
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "Impossible to fulfill pictures collection.");
                return pictures;
            }

            while(resultSet.next()) {
                Picture picture = new Picture();

                picture.setPictureId(resultSet.getInt("p_uid"));
                String pictureType = resultSet.getString("p_type");
                PictureType type = PictureType.valueOf(pictureType.toUpperCase());
                picture.setPictureType(type);
                picture.setParts(resultSet.getInt("p_parts"));
                picture.setDuration(resultSet.getInt("p_duration"));
                picture.setName(resultSet.getString("p_name"));
                picture.setSlogan(resultSet.getString("p_slogan"));
                picture.setBudget(resultSet.getLong("p_budget"));

                byte[] image = resultSet.getBytes("p_image");
                picture.setImage(image);

                double rating = resultSet.getDouble("rate");
                if (rating == 0) {
                    rating = resultSet.getDouble("p_rating");
                }
                picture.setRating(round(rating));

                picture.setYear(resultSet.getInt("p_issue_year"));

                picture.setAgeLimit(resultSet.getByte("p_age_limit"));

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(ISSUE_DATE_PATTERN);
                LocalDate issueDate = LocalDate.parse((resultSet.getDate("p_premiere_date")).toString(), formatter);
                picture.setPremiereDate(issueDate);

                CountryDAO countryDAO = new CountryDAO();
                picture.setCountries(countryDAO.findCountriesByPicture(picture.getPictureId()));
                DirectorDAO directorDAO = new DirectorDAO();
                picture.setDirectors(directorDAO.findDirectorByPicture(picture.getPictureId()));
                GenreDAO genreDAO = new GenreDAO();
                picture.setGenres(genreDAO.findGenresByPicture(picture.getPictureId()));
                ReviewDAO reviewDAO = new ReviewDAO();
                picture.setReviews(reviewDAO.findReviewsByPicture(picture.getPictureId()));

                pictures.add(picture);
            }
        } catch (WrapperConnectionException |SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return pictures;
    }

    /**
     * Identifies picture with current parameters id.
     * @return picture id or 0.
     * @param picture user which favorite list is been found id.
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Picture
     */
    public int findPictureId(Picture picture) throws DaoException {
        PreparedStatement statement = null;
        int pictureId = 0;
        try {
            statement = connection.getPreparedStatement(FIND_PICTURE_ID);

            statement.setString(1, picture.getPictureType().toString().toLowerCase());
            statement.setInt(2, picture.getParts());
            statement.setString(3, picture.getName());
            statement.setString(4, picture.getSlogan());
            statement.setInt(5, picture.getDuration());
            statement.setLong(6, picture.getBudget());

            InputStream image = new ByteArrayInputStream(picture.getImage());
            statement.setBlob(7, image);

            statement.setInt(8, picture.getYear());
            statement.setByte(9, picture.getAgeLimit());

            statement.setString(10, picture.getPremiereDate().toString());
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "Can't find picture id.");
                return pictureId;
            }

            if (resultSet.next()){
                pictureId = resultSet.getInt("p_uid");
                LOGGER.log(Level.DEBUG, "Added picture id: " + pictureId);
            }
            return pictureId;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Calculates the average of all user ratings.
     * @return double value or 0.
     * @param userId user which average ratings are count.
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public double findAverageRatingByUser(int userId) throws DaoException {
        PreparedStatement statement = null;
        double averageRate = 0;

        try {
            statement = connection.getPreparedStatement(AVERAGE_PICTURE_RATE_USER);

            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "Can't find picture id.");
                return averageRate;
            }

            if (resultSet.next()){
                averageRate = resultSet.getDouble("average");
            }
            return averageRate;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * The total number of evaluations for the pictures assessed by the specified user is calculated.
     * @return int value or 0.
     * @param userId user which pictures assesses are count.
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public int findNumberOfRates(int userId) throws DaoException {
        PreparedStatement statement = null;
        int numberOfRates = 0;

        try {
            statement = connection.getPreparedStatement(NUMBER_OF_RATES);
            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                return numberOfRates;
            }

            if (resultSet.next()){
                numberOfRates = resultSet.getInt("rates");
            }
            return numberOfRates;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Method adds new rating to database.
     * @return true if action succeed and false if failed.
     * @param userId id of user who rated the picture.
     * @param pictureId id of picture which was rated.
     * @param rating rating mark value.
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Picture
     */
    public boolean addRating(int userId, int pictureId, int rating) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(RATE_PICTURE);

            statement.setInt(1, pictureId);
            statement.setInt(2, userId);
            statement.setInt(3, rating);

            int result = statement.executeUpdate();

            if (result == 1) {
                return true;
            } else {
                throw new DaoException("Can't add rating.");
            }
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /*Method rounds double value to gived precision*/
    private double round(double value) {
        int scale = (int) Math.pow(10, PRECISION);
        return (double) Math.round(value * scale) / scale;
    }
}
