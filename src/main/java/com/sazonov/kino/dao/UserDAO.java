package com.sazonov.kino.dao;

import com.sazonov.kino.connection.ConnectionPool;
import com.sazonov.kino.entity.User;
import com.sazonov.kino.entity.UserRole;
import com.sazonov.kino.entity.UserStatus;
import com.sazonov.kino.exception.DaoException;
import com.sazonov.kino.exception.WrapperConnectionException;
import com.sazonov.kino.util.UserStatusDefiner;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Class {@code UserDAO} is realisation of {@code AbstractDAO} abstract class.
 * It provides user database actions on {@code User} entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.dao.AbstractDAO
 * @see com.sazonov.kino.entity.User
 * @since JDK8.0
 * @version 1.0
 */
public class UserDAO extends AbstractDAO<Integer, User> {
    private static final Logger LOGGER = LogManager.getLogger(UserDAO.class);
    private static final String CHECK_USER = "SELECT u_uid, u_name, u_email, u_status, u_role, u_ban, u_ban_dt " +
            "FROM user WHERE u_login = ? AND u_password = ?;";
    private static final String FIND_USER_BY_ID = "SELECT u_uid, u_name, u_email, u_status, u_role, u_ban, u_ban_dt " +
            "FROM user WHERE u_uid = ?;";
    private static final String BAN_USER = "UPDATE user SET u_ban = 1, u_ban_dt = ? WHERE u_uid = ?;";
    private static final String SET_USER_STATUS = "UPDATE user SET u_status = ? WHERE u_uid = ?;";
    private static final String CREATE_USER = "INSERT INTO user(u_uid, u_name, u_email, u_login, u_password, u_status) " +
            "VALUES (null, ?, ?, ?, ?, 1);";
    private static final String DELETE_USER = "DELETE FROM user WHERE u_uid = ?;";
    private static final String UPDATE_USER_PARAMS = "UPDATE user SET u_name = ?, u_email = ?, u_login = ?, u_password = ? " +
            "WHERE u_uid = ?;";
    private static final String CHANGE_LOGIN_PASS = "UPDATE user SET u_login = ?, u_password = ? WHERE u_email = ?;";
    private static final String UPDATE_USER_LOGIN = "UPDATE user SET u_name = ?, u_email = ?, u_login = ?, " +
            "WHERE u_uid = ?;";
    private static final String UPDATE_USER_PASSWORD = "UPDATE user SET u_name = ?, u_email = ?, u_password = ? " +
            "WHERE u_uid = ?;";
    private static final String UPDATE_USER_ONLY = "UPDATE user SET u_name = ?, u_email = ? WHERE u_uid = ?;";
    private static final String CHECK_LOGIN = "SELECT u_login FROM user WHERE u_login = ?;";
    private static final String CHECK_EMAIL = "SELECT u_email FROM user WHERE u_email = ?;";
    private static final String USER_AVERAGE_RATE = "SELECT AVG(r_value) AS rate FROM rating WHERE user_u_uid = ?;";
    private static final String ADD_FAVORITE_PICTURE = "INSERT INTO picture_m2m_user(user_u_uid, picture_p_uid) " +
            "VALUES (?, ?);";
    private static final String REMOVE_FAVORITE_PICTURE = "DELETE FROM picture_m2m_user WHERE " +
            "user_u_uid = ? AND picture_p_uid = ?;";
    private static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss.S";
    private static final String DATETIME_WRITE_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final int DATETIME_DISPLACEMENT = 3;
    private static final int USER_STATUS_CORRECTOR = 1;

    /** Constructs {@code UserDAO} object and initialize connection field by getting connection from
     * connection pool.
     *
     * @see com.sazonov.kino.dao.AbstractDAO
     * @see com.sazonov.kino.entity.User
     */
    public UserDAO() {
        this.connection = ConnectionPool.getInstance().getConnection();
    }

    /**
     * Creates user entity using login and password parameters.
     * @param login hash of user login.
     * @param password hash of user password.
     * @return created object of {@code User} type with given login and pass.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public User checkUser(String login, String password) throws DaoException {
        User user = null;

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(CHECK_USER);

            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "Empty result set after executing query.");
                return user;
            }

            user = new User();
            if (resultSet.next()){
                user.setUserId(resultSet.getInt("u_uid"));
                user.setName(resultSet.getString("u_name"));
                user.setEmail(resultSet.getString("u_email"));
                int statusIndex = resultSet.getInt("u_status");
                UserStatusDefiner statusDefiner = new UserStatusDefiner();
                UserStatus status = statusDefiner.defineStatus(statusIndex);
                if (status!=null) {
                    user.setStatus(status);
                }

                int roleIndex = resultSet.getInt("u_role");
                if (roleIndex == 1) {//1 в константу
                    user.setRole(UserRole.ADMINISTRATOR);
                }

                int banIndex = resultSet.getInt("u_ban");
                if (banIndex == 1) {//1 в константу
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_PATTERN);
                    LocalDateTime time = LocalDateTime.parse((resultSet.getTimestamp("u_ban_dt"))
                            .toString(), formatter).minusHours(DATETIME_DISPLACEMENT);
                    if (time.isAfter(LocalDateTime.now())) {
                        user.setBanned(true);
                        user.setBanTime(time);
                    }
                }
                PictureDAO dao = new PictureDAO();
                user.setFavorite(dao.findFavoriteList(user.getUserId()));
            }
        } catch (WrapperConnectionException|SQLException exception) {
            LOGGER.log(Level.ERROR, exception);
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return user;
    }

    /**
     * Creates user entity using id parameter.
     * @param userId uniquely identifies user.
     * @return created object of {@code User} type with given id.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    @Override
    public User findEntityById(Integer userId) throws DaoException {
        User user = null;

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_USER_BY_ID);

            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No user with id " + userId + " found.");
                return user;
            }

            user = new User();
            user.setUserId(userId);

            if (resultSet.next()){
                user.setName(resultSet.getString("u_name"));
                user.setEmail(resultSet.getString("u_email"));
                int statusIndex = resultSet.getInt("u_status");
                UserStatusDefiner statusDefiner = new UserStatusDefiner();
                UserStatus status = statusDefiner.defineStatus(statusIndex);
                if (status!=null) {
                    user.setStatus(status);
                }

                int roleIndex = resultSet.getInt("u_role");
                if (roleIndex == 1) {
                    user.setRole(UserRole.ADMINISTRATOR);
                }

                int banIndex = resultSet.getInt("u_ban");
                if (banIndex == 1) {//1 в константу
                    LocalDateTime now = LocalDateTime.now();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_PATTERN);
                    LocalDateTime time = LocalDateTime.parse((resultSet.getTimestamp("u_ban_dt"))
                            .toString(), formatter).minusHours(DATETIME_DISPLACEMENT);
                    if (time.isAfter(now)) {
                        user.setBanned(true);
                        user.setBanTime(time);
                    }
                }
                PictureDAO dao = new PictureDAO();
                user.setFavorite(dao.findFavoriteList(user.getUserId()));
            }
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return user;
    }

    /**
     * Deletes user information using id parameter.
     * @param userId uniquely identifies user.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    @Override
    boolean delete(Integer userId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(DELETE_USER);

            statement.setInt(1, userId);
            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "User with id " + userId + " wasn't deleted.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Sets user ban status as true.
     * @param userId uniquely identifies user.
     * @param banDate by what date was user banned.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public boolean banUser(int userId, LocalDate banDate) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(BAN_USER);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_WRITE_PATTERN);
            LocalDateTime banTime = LocalDateTime.of(banDate, LocalTime.NOON);
            String dateTime = banTime.format(formatter);

            statement.setString(1, dateTime);
            statement.setInt(2, userId);

            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "User with id " + userId + " wasn't banned.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Sets user status as according to the given parameter.
     * @param userId uniquely identifies user.
     * @param status status of {@code UserStatus} enum type.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public boolean setStatus(int userId, UserStatus status) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(SET_USER_STATUS);

            statement.setInt(1, (status.ordinal() + USER_STATUS_CORRECTOR));
            statement.setInt(2, userId);

            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "User with id " + userId + " status was changed.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Creates new user with the given parameters.
     * @param login hash of user login.
     * @param password hash of user password.
     * @param email new user email.
     * @param name new user name.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public boolean createUser(String login, String password, String email, String name) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(CREATE_USER);

            statement.setString(1, name);
            statement.setString(2, email);
            statement.setString(3, login);
            statement.setString(4, password);

            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "User " + name + " wasn't created in DAO.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Updates user parameters.
     * @param user object of class {@code User} containing changed parameters.
     * @param password hash of user password.
     * @param login new user login.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public boolean updateUserParams(User user, String login, String password) throws DaoException {
        PreparedStatement statement = null;
        try {

            if (login!=null&&password!=null) {
                statement = connection.getPreparedStatement(UPDATE_USER_PARAMS);
                statement.setString(1, user.getName());
                statement.setString(2, user.getEmail());
                statement.setString(3, login);
                statement.setString(4, password);
                statement.setInt(5, user.getUserId());
            } else if (login!=null) {
                statement = connection.getPreparedStatement(UPDATE_USER_LOGIN);
                statement.setString(1, user.getName());
                statement.setString(2, user.getEmail());
                statement.setString(3, login);
                statement.setInt(5, user.getUserId());
            } else if (password!=null) {
                statement = connection.getPreparedStatement(UPDATE_USER_PASSWORD);
                statement.setString(1, user.getName());
                statement.setString(2, user.getEmail());
                statement.setString(3, password);
                statement.setInt(4, user.getUserId());
            } else {
                statement = connection.getPreparedStatement(UPDATE_USER_ONLY);
                statement.setString(1, user.getName());
                statement.setString(2, user.getEmail());
                statement.setInt(3, user.getUserId());
            }

            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "User  with id " + user.getUserId() + " parameters wasn't changed.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Resets user with given email login and password.
     * @param password hash of new user password.
     * @param login hash of new user login.
     * @param email user email.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public boolean resetLoginPass(String login, String password, String email) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(CHANGE_LOGIN_PASS);
            statement.setString(1, login);
            statement.setString(2, password);
            statement.setString(3, email);

            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "User  with email " + email + " parameters wasn't changed.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Check out if the login is free to use.
     * @param login hash of checked login.
     * @return true if login is free, otherwise false.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public boolean isLoginAvailable(String login) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(CHECK_LOGIN);

            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "Login not found.");
                return true;
            }
            return false;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Check out if the email is free to use.
     * @param email checked email.
     * @return true if email is free, otherwise false.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public boolean isEmailAvailable(String email) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(CHECK_EMAIL);

            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "Email not found.");
                return true;
            }
            return false;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Counts avarage rating value, rated by user.
     * @param userId uniquely identifies user.
     * @return double value or 0.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public double getUserAverageRating (int userId) throws DaoException {
        PreparedStatement statement = null;
        double averageUserRating = 0;
        try {
            statement = connection.getPreparedStatement(USER_AVERAGE_RATE);

            statement.setInt(1, userId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "Login not found.");
                return averageUserRating;
            }

            if (resultSet.next()){
                averageUserRating = resultSet.getDouble("rate");
            }
            return averageUserRating;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Adds picture to user favorite list.
     * @param userId uniquely identifies user.
     * @param pictureId uniquely identifies picture.
     * @return true if , otherwise false.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public boolean addPictureToFavorite(int userId, int pictureId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(ADD_FAVORITE_PICTURE);

            statement.setInt(1, userId);
            statement.setInt(2, pictureId);
            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "User with id " + userId + " adding picture failed.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Removes picture to user favorite list.
     * @param userId uniquely identifies user.
     * @param pictureId uniquely identifies picture.
     * @return true if removed, otherwise false.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.User
     */
    public boolean removePictureFromFavorite(int userId, int pictureId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(REMOVE_FAVORITE_PICTURE);
            statement.setInt(1, userId);
            statement.setInt(2, pictureId);
            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "User with id " + userId + " can't remove picture.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Unsupported method.
     * @throws java.lang.UnsupportedOperationException if method is used.
     */
    @Override
    boolean create(User entity) {
        throw new UnsupportedOperationException();
    }

    /**
     * Unsupported method.
     * @throws java.lang.UnsupportedOperationException if method is used.
     */
    @Override
    User update(User entity) {
        throw new UnsupportedOperationException();
    }

}
