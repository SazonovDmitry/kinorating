package com.sazonov.kino.dao;

import com.sazonov.kino.connection.WrapperConnection;
import com.sazonov.kino.entity.Entity;
import com.sazonov.kino.exception.DaoException;

import java.sql.Statement;

/**
 * Class {@code AbstractDAO} is a basic class for DAO classes group.
 * It provides binding contract for classes-inheritors.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public abstract class AbstractDAO<K, T extends Entity> {
    protected WrapperConnection connection;
    /**
     * Creates parametrised Entity using id parameter.
     * @param id uniquely identifies entity.
     * @return created object of parameterized type.
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity
     */
    abstract T findEntityById(K id) throws DaoException;
    /**
     * Deletes entity information using id parameter.
     * @param id uniquely identifies entity.
     * @return true if action succeed and false if failed.
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity
     */
    abstract boolean delete(K id) throws DaoException;
    /**
     * Writes entity information to database.
     * @param entity object of parameterized type T.
     * @return true if action succeed and false if failed.
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity
     */
    abstract boolean create(T entity) throws DaoException;
    /**
     * Updates entity information in database.
     * @param entity object of parameterized type T.
     * @return true if action succeed and false if failed.
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity
     */
    abstract T update(T entity) throws DaoException;

    /**
     * Invokes method close() on {@code WrapperConnection} object field.
     * @see com.sazonov.kino.connection.ConnectionPool
     */
    void releaseConnection() {
        connection.close();
    }
    /**
     * Invokes method closeStatement() on {@code WrapperConnection} object field.
     * @param statement object of class {@code Statement}
     * @see java.sql.Statement
     */
    void closeStatement(Statement statement) {
        connection.closeStatement(statement);
    }
}
