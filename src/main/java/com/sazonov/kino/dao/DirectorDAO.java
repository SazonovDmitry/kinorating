package com.sazonov.kino.dao;

import com.sazonov.kino.connection.ConnectionPool;
import com.sazonov.kino.entity.Country;
import com.sazonov.kino.entity.Director;
import com.sazonov.kino.exception.DaoException;
import com.sazonov.kino.exception.WrapperConnectionException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Class {@code DirectorDAO} is realisation of {@code AbstractDAO} abstract class.
 * It provides user database actions on {@code Director} entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.dao.AbstractDAO
 * @see com.sazonov.kino.entity.Director
 * @since JDK8.0
 * @version 1.0
 */
public class DirectorDAO extends AbstractDAO<Integer, Director>{
    private static final Logger LOGGER = LogManager.getLogger(DirectorDAO.class);
    private static final String FIND_DIRECTOR_BY_ID = "SELECT d_name, d_lastname, d_birth_date, d_photo, c_uid, c_name " +
            "FROM director JOIN country ON country_c_uid = c_uid AND d_uid = ?;";
    private static final String DELETE_DIRECTOR_BY_ID = "DELETE FROM director WHERE d_uid = ?;";
    private static final String FIND_DIRECTOR_BY_PICTURE = "SELECT d_uid, d_name, d_lastname, d_birth_date, d_photo, c_uid, " +
            "c_name FROM director JOIN country ON country_c_uid = c_uid JOIN picture_m2m_director " +
            "ON d_uid = director_d_uid AND picture_p_uid = ?;";
    private static final String FIND_ALL_DIRECTORS = "SELECT d_uid, d_name, d_lastname, d_birth_date, d_photo, c_uid, " +
            "c_name FROM director JOIN country ON country_c_uid = c_uid ORDER BY d_name;";
    private static final String CREATE_DIRECTOR = "INSERT INTO director(d_uid, d_name, d_lastname, d_birth_date, d_photo, " +
            "country_c_uid) VALUES (null, ?, ?, ?, ?, ?);";
    private static final String UPDATE_DIRECTOR_PARAMS = "UPDATE director SET d_name = ?, d_lastname = ?, " +
            "d_birth_date = ?, d_photo = ?, country_c_uid = ? WHERE d_uid = ?;";
    private static final String INSERT_PICTURE_DIRECTORS = "INSERT INTO picture_m2m_director(picture_p_uid, director_d_uid) " +
            "VALUES (?, ?);";
    private static final String DATE_PATTERN = "yyyy-MM-dd";

    /** Constructs {@code DirectorDAO} object and initialize connection field by getting connection from
     * connection pool.
     *
     * @see com.sazonov.kino.dao.AbstractDAO
     * @see com.sazonov.kino.entity.Director
     */
    public DirectorDAO() { this.connection = ConnectionPool.getInstance().getConnection();
    }

    /**
     * Creates director entity using id parameter.
     * @param directorId uniquely identifies director.
     * @return created object of {@code Director} type with given id.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Director
     */
    @Override
    public Director findEntityById(Integer directorId) throws DaoException {
        Director director = null;

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_DIRECTOR_BY_ID);

            statement.setInt(1, directorId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No director with id " + directorId + " found.");
                return director;
            }

            director = new Director();
            Country country = new Country();
            if (resultSet.next()){
                director.setDirectorId(directorId);
                director.setName(resultSet.getString("d_name"));
                director.setLastName(resultSet.getString("d_lastname"));
                country.setCountryId(resultSet.getInt("c_uid"));
                country.setName(resultSet.getString("c_name"));
                director.setCountry(country);

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
                LocalDate date = LocalDate.parse((resultSet.getDate("d_birth_date")).toString(), formatter);
                director.setBirthDate(date);

                byte[] photo = resultSet.getBytes("d_photo");
                director.setPhoto(photo);
            }
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return director;
    }

    /**
     * Deletes director information using id parameter.
     * @param directorId uniquely identifies director.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Director
     */
    @Override
    public boolean delete(Integer directorId) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(DELETE_DIRECTOR_BY_ID);

            statement.setInt(1, directorId);
            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "Director with id " + directorId + " wasn't deleted.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException | SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Creates list of directors using picture id parameter.
     * @param pictureId uniquely identifies picture for selection.
     * @return list of objects of class {@code Director}.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Director
     */
    public List<Director> findDirectorByPicture(int pictureId) throws DaoException {
        List<Director> directors = new ArrayList<>();

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_DIRECTOR_BY_PICTURE);

            statement.setInt(1, pictureId);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No directors on picture with id " + pictureId + " found.");
                return directors;
            }

            while(resultSet.next()) {
                Director director = new Director();
                Country country = new Country();
                director.setDirectorId(resultSet.getInt("d_uid"));
                director.setName(resultSet.getString("d_name"));
                director.setLastName(resultSet.getString("d_lastname"));

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
                LocalDate date = LocalDate.parse((resultSet.getDate("d_birth_date")).toString(), formatter);
                director.setBirthDate(date);

                country.setCountryId(resultSet.getInt("c_uid"));
                country.setName(resultSet.getString("c_name"));
                director.setCountry(country);

                byte[] photo = resultSet.getBytes("d_photo");
                director.setPhoto(photo);

                directors.add(director);
            }
        } catch (WrapperConnectionException |SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return directors;
    }

    /**
     * Writes entity information to database.
     * @param director object of {@code Director} type.
     * @return true if action succeed and false if failed.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Director
     */
    @Override
    public boolean create(Director director) throws DaoException {
        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(CREATE_DIRECTOR);

            statement.setString(1, director.getName());
            statement.setString(2, director.getLastName());
            statement.setString(3, director.getBirthDate().toString());

            InputStream image = new ByteArrayInputStream(director.getPhoto());
            statement.setBlob(4, image);
            statement.setInt(5, director.getCountry().getCountryId());

            int result = statement.executeUpdate();

            if (result != 1) {
                LOGGER.log(Level.DEBUG, "Director with name " + director.getName() + " wasn't created.");
                return false;
            }
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Updates entity information in database.
     * @param director object of {@code Director} type.
     * @return updated {@code Director} object.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Director
     */
    @Override
    public Director update(Director director) throws DaoException {
        PreparedStatement statement = null;

        try {
            statement = connection.getPreparedStatement(UPDATE_DIRECTOR_PARAMS);

            statement.setString(1, director.getName());
            statement.setString(2, director.getLastName());
            statement.setString(3, director.getBirthDate().toString());

            InputStream inputStream = new ByteArrayInputStream(director.getPhoto());
            statement.setBlob(4, inputStream);

            statement.setInt(5, director.getCountry().getCountryId());
            statement.setInt(6, director.getDirectorId());

            int result = statement.executeUpdate();

            if (result != 1) {
                throw new DaoException("Director  with id " + director.getDirectorId() + " parameters wasn't changed.");
            }
            return director;
        } catch (WrapperConnectionException|SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
    }

    /**
     * Creates list of all directors represented in database.
     * @return list of objects of class {@code Director}.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Director
     */
    public List<Director> findAll() throws DaoException {
        List<Director> directors = new ArrayList<>();

        PreparedStatement statement = null;
        try {
            statement = connection.getPreparedStatement(FIND_ALL_DIRECTORS);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                LOGGER.log(Level.DEBUG, "No directors found.");
                return directors;
            }

            while(resultSet.next()) {
                Director director = new Director();
                Country country = new Country();
                director.setDirectorId(resultSet.getInt("d_uid"));
                director.setLastName(resultSet.getString("d_lastname"));
                director.setName(resultSet.getString("d_name"));
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
                LocalDate date = LocalDate.parse((resultSet.getDate("d_birth_date")).toString(), formatter);
                director.setBirthDate(date);

                country.setCountryId(resultSet.getInt("c_uid"));
                country.setName(resultSet.getString("c_name"));
                director.setCountry(country);

                byte[] photo = resultSet.getBytes("d_photo");
                director.setPhoto(photo);

                directors.add(director);
            }
        } catch (WrapperConnectionException |SQLException exception) {
            throw new DaoException(exception);
        } finally {
            closeStatement(statement);
            releaseConnection();
        }
        return directors;
    }

    /**
     * Inserts to database information about directors, added to certain picture.
     * @return true if action succeed and false if failed.
     * @param directorsId list of objects of type {@code Director}.
     * @param pictureId uniquely identifies picture.
     *
     * @throws com.sazonov.kino.exception.DaoException in case of any writing mistake.
     * @see com.sazonov.kino.entity.Director
     */
    public boolean insertPictureDirectors(List<Integer> directorsId, int pictureId) throws DaoException {
        PreparedStatement statement = null;
        try {
            if (connection.getAutoCommit()) {
                connection.setAutoCommit(false);
            }

            for (Integer directorId: directorsId) {
                statement = connection.getPreparedStatement(INSERT_PICTURE_DIRECTORS);
                statement.setInt(1, pictureId);
                statement.setInt(2, directorId);

                int result = statement.executeUpdate();

                if (result != 1) {
                    LOGGER.log(Level.DEBUG, "Directors wasn't added.");
                    connection.releaseRollBack();
                    return false;
                }
            }
            connection.releaseCommit();
            return true;
        } catch (WrapperConnectionException|SQLException exception) {
            connection.releaseRollBack();
            throw new DaoException(exception);
        } finally {
            connection.setAutoCommit(true);
            closeStatement(statement);
            releaseConnection();
        }
    }
}
