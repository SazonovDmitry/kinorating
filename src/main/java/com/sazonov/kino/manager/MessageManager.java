package com.sazonov.kino.manager;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class {@code MessageManager} is enumeration of project locales
 * with initialized resource bundles.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public enum MessageManager {
    EN(ResourceBundle.getBundle("properties.message", new Locale("en", "US"))),
    RU(ResourceBundle.getBundle("properties.message", new Locale("ru", "RU"))),
    BY(ResourceBundle.getBundle("properties.message", new Locale("be", "BY")));

    private ResourceBundle bundle;

    /** Constructs {@code MessageManager} object and initialize private {@code ResourceBundle} field.
     * @param bundle {@code ResourceBundle} object.
     *
     */
    MessageManager(ResourceBundle bundle) {
        this.bundle = bundle;
    }

    /** returns message from properties file according to current locale.
     * @param key message key.
     * @return String value of found message.
     *
     */
    public String getMessage(String key) {
        return bundle.getString(key);
    }
}