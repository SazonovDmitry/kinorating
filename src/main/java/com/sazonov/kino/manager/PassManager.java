package com.sazonov.kino.manager;

/**
 * Class {@code MessageManager} is enumeration of project paths.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public enum PassManager {
    PASS_PAGE_REGISTRATION("/jsp/registration.jsp"),
    PASS_PAGE_ADMIN("/jsp/admin/admin.jsp"),
    PASS_ADMIN_DIRECTOR("/jsp/admin/director.jsp"),
    PASS_ADMIN_MANAGEMENT("/jsp/admin/management.jsp"),
    PASS_PAGE_PICTURE("/jsp/picture.jsp"),
    PASS_PAGE_LOGIN("/jsp/login.jsp"),
    PASS_PAGE_MAIN("/jsp/main.jsp"),
    PASS_PAGE_INDEX("/index.jsp"),
    PASS_PAGE_MAIL("/jsp/mail.jsp");

    private String pass;

    /** Constructs {@code PassManager} object and initialize private field.
     * @param pass page pass.
     */
    PassManager(String pass) {this.pass = pass;}

    /** returns object page pass.
     * @return String value of page pass.
     *
     */
    public String getPass() {return this.pass;}
}
