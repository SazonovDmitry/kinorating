package com.sazonov.kino.entity;

import java.io.Serializable;
/**
 * Class abstract class, basic class for entity classes group.
 *
 * @author Sazonov
 * @since JDK7.0
 * @version 1.0
 */
public abstract class Entity implements Serializable, Cloneable{

    /** Constructs {@code Entity} as a default constructor.
     *
     */
    public Entity() {
    }
}
