package com.sazonov.kino.entity;


import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Class {@code Review} inherit from {@code Entity} abstract class.
 * Represents picture review entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Entity
 * @since JDK7.0
 * @version 1.0
 */
public class Review extends Entity {
    private int reviewId;
    private String content;
    private LocalDateTime dateTime;
    private int pictureId;
    private String userName;
    private UserStatus status;
    private int userId;

    /** Constructs {@code Review} as a default constructor.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public Review() {
    }

    /** Standard java Bean get method for review id.
     * @return int value of review id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getReviewId() {
        return reviewId;
    }

    /** Standard java Bean set method for review id.
     * @param reviewId int value of review id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    /** Standard java Bean get method for review content.
     * @return String value of review content.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public String getContent() {
        return content;
    }

    /** Standard java Bean set method for review content.
     * @param content String value of review content.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setContent(String content) {
        this.content = content;
    }

    /** Standard java Bean get method for review date and time.
     * @return LocalDateTime value of review date and time.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public LocalDateTime getDateTime() {
        return dateTime;
    }

    /** Standard java Bean set method for review date and time.
     * @param dateTime LocalDateTime value of review date and time.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    /** Standard java Bean get method for picture id.
     * @return int value of picture id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getPictureId() {
        return pictureId;
    }

    /** Standard java Bean set method for picture id.
     * @param pictureId int value of picture id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    /** Standard java Bean get method for user name.
     * @return String value of user name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public String getUserName() {
        return userName;
    }

    /** Standard java Bean set method for user name.
     * @param userName String value of user name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /** Standard java Bean get method for user id.
     * @return int value of user id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getUserId() {
        return userId;
    }

    /** Standard java Bean set method for user id.
     * @param userId int value of user id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /** Standard java Bean get method for user status.
     * @return UserStatus value of user status.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public UserStatus getStatus() {
        return status;
    }

    /** Standard java Bean set method for user status.
     * @param status UserStatus value of user status.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setStatus(UserStatus status) {
        this.status = status;
    }

    /** Overrides standard {@code Object} class method equals().
     * @param o object for comparing.
     * @return true if objects equals, otherwise false.
     *
     * @see java.lang.Object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Review review = (Review) o;
        return reviewId == review.reviewId &&
                pictureId == review.pictureId &&
                userId == review.userId &&
                Objects.equals(content, review.content) &&
                Objects.equals(dateTime, review.dateTime) &&
                Objects.equals(userName, review.userName) &&
                status == review.status;
    }

    /** Overrides standard {@code Object} class method hashCode().
     * @return int hashCode value of object.
     *
     * @see java.lang.Object
     */
    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), reviewId, content, dateTime, pictureId, userName, status, userId);
    }

    /** Overrides standard {@code Object} class method toString().
     * @return String object representation.
     *
     * @see java.lang.Object
     */
    @Override
    public String toString() {
        return "Review{" +
                "reviewId=" + reviewId +
                ", content='" + content + '\'' +
                ", dateTime=" + dateTime +
                ", pictureId=" + pictureId +
                ", userName='" + userName + '\'' +
                ", status=" + status +
                ", userId=" + userId +
                '}';
    }
}
