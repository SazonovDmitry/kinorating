package com.sazonov.kino.entity;

/**
 * Enum represents enumeration of picture types.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Picture
 * @since JDK7.0
 * @version 1.0
 */
public enum PictureType {
    FILM, SERIAL
}
