package com.sazonov.kino.entity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Class {@code User} inherit from {@code Entity} abstract class.
 * Represents user entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Entity
 * @since JDK7.0
 * @version 1.0
 */
public class User extends Entity{
    private int userId;
    private String name;
    private String email;
    private UserStatus status = UserStatus.KINO_BEGINNER;
    private UserRole role = UserRole.USER;
    private boolean isBanned;
    private LocalDateTime banTime;
    private List<Picture> favorite;


    /** Constructs {@code User} as a default constructor.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public User() {
    }

    /** Standard java Bean get method for user id.
     * @return int value of user id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getUserId() {
        return userId;
    }

    /** Standard java Bean get method for user name.
     * @return String value of user name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public String getName() {
        return name;
    }

    /** Standard java Bean get method for user email.
     * @return String value of user email.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public String getEmail() {
        return email;
    }

    /** Standard java Bean get method for user status.
     * @return UserStatus value of user status.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public UserStatus getStatus() {
        return status;
    }

    /** Standard java Bean get method for user role.
     * @return UserRole value of user role.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public UserRole getRole() {
        return role;
    }

    /** Standard java Bean get method for user ban status.
     * @return true if user has ban, otherwise false.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public boolean isBanned() {
        return isBanned;
    }

    /** Standard java Bean get method for user ban time.
     * @return LocalDateTime value of user ban time.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public LocalDateTime getBanTime() {
        return banTime;
    }

    /** Standard java Bean set method for user id.
     * @param userId int value of user id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /** Standard java Bean set method for user name.
     * @param name String value of user name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setName(String name) {
        this.name = name;
    }

    /** Standard java Bean set method for user email.
     * @param email String value of user email.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /** Standard java Bean set method for user status.
     * @param status UserStatus value of user status.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setStatus(UserStatus status) {
        this.status = status;
    }

    /** Standard java Bean set method for user role.
     * @param role UserRole value of user role.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setRole(UserRole role) {
        this.role = role;
    }

    /** Standard java Bean set method for user ban status.
     * @param banned set user ban value.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setBanned(boolean banned) {
        isBanned = banned;
    }

    /** Standard java Bean set method for user ban time.
     * @param banTime LocalDateTime value for user ban time.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setBanTime(LocalDateTime banTime) {
        this.banTime = banTime;
    }

    /** Standard java Bean get method for user favorite list.
     * @return list of user favorite pictures.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public List<Picture> getFavorite() {
        return favorite;
    }

    /** Standard java Bean set method for user favorite pictures list.
     * @param favorite list of user favorite pictures.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setFavorite(List<Picture> favorite) {
        this.favorite = favorite;
    }

    /** Overrides standard {@code Object} class method equals().
     * @param o object for comparing.
     * @return true if objects equals, otherwise false.
     *
     * @see java.lang.Object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return userId == user.userId &&
                isBanned == user.isBanned &&
                Objects.equals(name, user.name) &&
                Objects.equals(email, user.email) &&
                status == user.status &&
                role == user.role &&
                Objects.equals(banTime, user.banTime) &&
                Objects.equals(favorite, user.favorite);
    }

    /** Overrides standard {@code Object} class method hashCode().
     * @return int hashCode value of object.
     *
     * @see java.lang.Object
     */
    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), userId, name, email, status, role, isBanned, banTime, favorite);
    }

    /** Overrides standard {@code Object} class method toString().
     * @return String object representation.
     *
     * @see java.lang.Object
     */
    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", status=" + status +
                ", role=" + role +
                ", isBanned=" + isBanned +
                ", banTime=" + banTime +
                ", favorite=" + favorite +
                '}';
    }
}
