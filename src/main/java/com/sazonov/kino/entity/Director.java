package com.sazonov.kino.entity;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Objects;

/**
 * Class {@code Director} inherit from {@code Entity} abstract class.
 * Represents director entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Entity
 * @since JDK7.0
 * @version 1.0
 */
public class Director extends Entity{
    private int directorId;
    private String name;
    private String lastName;
    private LocalDate birthDate;
    private byte[] photo;
    private Country country;

    /** Constructs {@code Director} as a default constructor.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public Director() {
    }

    /** Standard java Bean get method for director id.
     * @return int value of director id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getDirectorId() {
        return directorId;
    }

    /** Standard java Bean set method for director id.
     * @param directorId int value of director id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setDirectorId(int directorId) {
        this.directorId = directorId;
    }

    /** Standard java Bean get method for director name.
     * @return String value of director name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public String getName() {
        return name;
    }

    /** Standard java Bean set method for director name.
     * @param name String value of director name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setName(String name) {
        this.name = name;
    }

    /** Standard java Bean get method for director last name.
     * @return String value of director last name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public String getLastName() {
        return lastName;
    }

    /** Standard java Bean set method for director last name.
     * @param lastName String value of director last name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /** Standard java Bean get method for director birthday.
     * @return LocalDate value of director birthday.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /** Standard java Bean set method for director birthday.
     * @param birthDate LocalDate value of director birthday.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    /** Standard java Bean get method for director photo.
     * @return byte array of director photo.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public byte[] getPhoto() {
        return photo;
    }

    /** Standard java Bean set method for director photo.
     * @param photo byte[] value of director photo.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    /** Standard java Bean get method for director country.
     * @return {@code Country} object value of director country.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public Country getCountry() {
        return country;
    }

    /** Standard java Bean set method for director country.
     * @param country {@code Country} object value of director country.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setCountry(Country country) {
        this.country = country;
    }

    /** Overrides standard {@code Object} class method equals().
     * @param o object for comparing.
     * @return true if objects equals, otherwise false.
     *
     * @see java.lang.Object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Director director = (Director) o;
        return directorId == director.directorId &&
                Objects.equals(name, director.name) &&
                Objects.equals(lastName, director.lastName) &&
                Objects.equals(birthDate, director.birthDate) &&
                Arrays.equals(photo, director.photo) &&
                Objects.equals(country, director.country);
    }

    /** Overrides standard {@code Object} class method hashCode().
     * @return int hashCode value of object.
     *
     * @see java.lang.Object
     */
    @Override
    public int hashCode() {

        int result = Objects.hash(super.hashCode(), directorId, name, lastName, birthDate, country);
        result = 31 * result + Arrays.hashCode(photo);
        return result;
    }
    /** Overrides standard {@code Object} class method toString().
     * @return String object representation.
     *
     * @see java.lang.Object
     */
    @Override
    public String toString() {
        return "Director{" +
                "directorId=" + directorId +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", photo=" + Arrays.toString(photo) +
                ", country=" + country +
                '}';
    }
}
