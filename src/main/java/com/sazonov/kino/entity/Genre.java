package com.sazonov.kino.entity;

import java.util.Objects;

/**
 * Class {@code Genre} inherit from {@code Entity} abstract class.
 * Represents genre entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Entity
 * @since JDK7.0
 * @version 1.0
 */
public class Genre extends Entity{
    private int genreId;
    private String name;

    /** Constructs {@code Genre} as a default constructor.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public Genre() {
    }

    /** Standard java Bean get method for genre id.
     * @return int value of genre id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getGenreId() {
        return genreId;
    }

    /** Standard java Bean set method for genre id.
     * @param genreId int value of genre id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    /** Standard java Bean get method for genre name.
     * @return String value of genre name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public String getName() {
        return name;
    }

    /** Standard java Bean set method for genre name.
     * @param name String value of genre name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setName(String name) {
        this.name = name;
    }

    /** Overrides standard {@code Object} class method equals().
     * @param o object for comparing.
     * @return true if objects equals, otherwise false.
     *
     * @see java.lang.Object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return genreId == genre.genreId &&
                Objects.equals(name, genre.name);
    }

    /** Overrides standard {@code Object} class method hashCode().
     * @return int hashCode value of object.
     *
     * @see java.lang.Object
     */
    @Override
    public int hashCode() {

        return Objects.hash(genreId, name);
    }

    /** Overrides standard {@code Object} class method toString().
     * @return String object representation.
     *
     * @see java.lang.Object
     */
    @Override
    public String toString() {
        return "Genre{" +
                "genreId=" + genreId +
                ", name='" + name + '\'' +
                '}';
    }
}
