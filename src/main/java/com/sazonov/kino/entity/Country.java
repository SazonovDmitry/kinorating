package com.sazonov.kino.entity;

import java.util.Objects;

/**
 * Class {@code Country} inherit from {@code Entity} abstract class.
 * Represents country entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Entity
 * @since JDK7.0
 * @version 1.0
 */
public class Country extends Entity{
    private int countryId;
    private String name;

    /** Constructs {@code Country} as a default constructor.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public Country() {
    }

    /** Standard java Bean get method for country id.
     * @return int value of country id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getCountryId() {
        return countryId;
    }

    /** Standard java Bean get method for country name.
     * @return String value of country name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public String getName() {
        return name;
    }

    /** Standard java Bean set method for country id.
     * @param countryId int value of country id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    /** Standard java Bean set method for country name.
     * @param name String value of country name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setName(String name) {

        this.name = name;
    }

    /** Overrides standard {@code Object} class method equals().
     * @param o object for comparing.
     * @return true if objects equals, otherwise false.
     *
     * @see java.lang.Object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return countryId == country.countryId &&
                Objects.equals(name, country.name);
    }
    /** Overrides standard {@code Object} class method hashCode().
     * @return int hashCode value of object.
     *
     * @see java.lang.Object
     */
    @Override
    public int hashCode() {
        return Objects.hash(countryId, name);
    }
    /** Overrides standard {@code Object} class method toString().
     * @return String object representation.
     *
     * @see java.lang.Object
     */
    @Override
    public String toString() {
        return "Country{" +
                "countryId=" + countryId +
                ", name='" + name + '\'' +
                '}';
    }
}
