package com.sazonov.kino.entity;

/**
 * Enum represents enumeration of user roles.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.User
 * @since JDK7.0
 * @version 1.0
 */
public enum UserRole {
    GUEST, USER, ADMINISTRATOR
}
