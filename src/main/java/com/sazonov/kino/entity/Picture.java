package com.sazonov.kino.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Class {@code Picture} inherit from {@code Entity} abstract class.
 * Represents country entity.
 *
 * @author Sazonov
 * @see com.sazonov.kino.entity.Entity
 * @since JDK7.0
 * @version 1.0
 */
public class Picture extends Entity{
    private int pictureId;
    private PictureType pictureType;
    private int parts;
    private String name;
    private String slogan;
    private int duration;
    private long budget;
    private byte[] image;
    private double rating;
    private int year;
    private byte ageLimit;
    private LocalDate premiereDate;

    private List<Country> countries = new ArrayList<>();
    private List<Director> directors = new ArrayList<>();
    private List<Genre> genres = new ArrayList<>();
    private List<Review> reviews = new ArrayList<>();

    /** Constructs {@code Picture} as a default constructor.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public Picture() {
    }

    /** Standard java Bean get method for picture id.
     * @return int value of picture id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getPictureId() {
        return pictureId;
    }

    /** Standard java Bean set method for picture id.
     * @param pictureId int value of picture id.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    /** Standard java Bean get method for picture type.
     * @return {@code PictureType} object value of picture type.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public PictureType getPictureType() {
        return pictureType;
    }

    /** Standard java Bean set method for picture type.
     * @param pictureType {@code PictureType} object value of picture type.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setPictureType(PictureType pictureType) {
        this.pictureType = pictureType;
    }

    /** Standard java Bean get method for picture number fo parts.
     * @return int value of picture number of parts.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getParts() {
        return parts;
    }

    /** Standard java Bean set method for picture number of parts.
     * @param parts int value of picture number of parts.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setParts(int parts) {
        this.parts = parts;
    }

    /** Standard java Bean get method for picture name.
     * @return String value of picture name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public String getName() {
        return name;
    }

    /** Standard java Bean set method for picture name.
     * @param name String value of picture name for setting.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setName(String name) {
        this.name = name;
    }

    /** Standard java Bean get method for picture slogan.
     * @return String value of picture name.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public String getSlogan() {
        return slogan;
    }

    /** Standard java Bean set method for picture slogan.
     * @param slogan String value of picture slogan for setting.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    /** Standard java Bean get method for picture duration.
     * @return int value of picture duration.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getDuration() {
        return duration;
    }

    /** Standard java Bean set method for picture duration.
     * @param duration int value of picture duration for setting.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setDuration(int duration) {
        this.duration = duration;
    }

    /** Standard java Bean get method for picture budget.
     * @return long value of picture budget.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public long getBudget() {
        return budget;
    }

    /** Standard java Bean set method for picture budget.
     * @param budget long value of picture budget for setting.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setBudget(long budget) {
        this.budget = budget;
    }

    /** Standard java Bean get method for picture image.
     * @return byte array of picture image.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public byte[] getImage() {
        return image;
    }

    /** Standard java Bean set method for picture image.
     * @param image byte[] value of picture image.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setImage(byte[] image) {
        this.image = image;
    }

    /** Standard java Bean get method for picture rating.
     * @return double value of picture rating.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public double getRating() {
        return rating;
    }

    /** Standard java Bean set method for picture rating.
     * @param rating double value of picture rating for setting.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setRating(double rating) {
        this.rating = rating;
    }

    /** Standard java Bean get method for picture year.
     * @return int value of picture year.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public int getYear() {
        return year;
    }

    /** Standard java Bean set method for picture year.
     * @param year int value of picture year for setting.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setYear(int year) {
        this.year = year;
    }

    /** Standard java Bean get method for picture age limit.
     * @return byte value of picture age limit.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public byte getAgeLimit() {
        return ageLimit;
    }

    /** Standard java Bean set method for picture age limit.
     * @param ageLimit byte value of picture age limit for setting.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setAgeLimit(byte ageLimit) {
        this.ageLimit = ageLimit;
    }

    /** Standard java Bean get method for picture premiere date.
     * @return LocalDate value of picture premiere date.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public LocalDate getPremiereDate() {
        return premiereDate;
    }

    /** Standard java Bean set method for picture premiere date.
     * @param premiereDate LocalDate value of picture premiere date for setting.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setPremiereDate(LocalDate premiereDate) {
        this.premiereDate = premiereDate;
    }

    /** Standard java Bean get method for picture countries.
     * @return list of pucture countries.
     *
     * @see com.sazonov.kino.entity.Country
     */
    public List<Country> getCountries() {
        return countries;
    }

    /** Standard java Bean set method for picture countries.
     * @param countries list of picture countries.
     *
     * @see com.sazonov.kino.entity.Entity
     */
    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

    /** Standard java Bean get method for picture directors list.
     * @return list of picture directors.
     *
     * @see com.sazonov.kino.entity.Director
     */
    public List<Director> getDirectors() {
        return directors;
    }

    /** Standard java Bean set method for picture directors.
     * @param directors list of picture directors.
     *
     * @see com.sazonov.kino.entity.Director
     */
    public void setDirectors(List<Director> directors) {
        this.directors = directors;
    }

    /** Standard java Bean get method for picture genres list.
     * @return list of picture genres.
     *
     * @see com.sazonov.kino.entity.Genre
     */
    public List<Genre> getGenres() {
        return genres;
    }

    /** Standard java Bean set method for picture genres.
     * @param genres list of picture genres.
     *
     * @see com.sazonov.kino.entity.Genre
     */
    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    /** Standard java Bean get method for picture reviews list.
     * @return list of picture reviews.
     *
     * @see com.sazonov.kino.entity.Review
     */
    public List<Review> getReviews() {
        return reviews;
    }

    /** Standard java Bean set method for picture reviews.
     * @param reviews list of picture reviews.
     *
     * @see com.sazonov.kino.entity.Review
     */
    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    /** Overrides standard {@code Object} class method equals().
     * @param o object for comparing.
     * @return true if objects equals, otherwise false.
     *
     * @see java.lang.Object
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Picture picture = (Picture) o;
        return pictureId == picture.pictureId &&
                parts == picture.parts &&
                duration == picture.duration &&
                budget == picture.budget &&
                Double.compare(picture.rating, rating) == 0 &&
                year == picture.year &&
                ageLimit == picture.ageLimit &&
                pictureType == picture.pictureType &&
                Objects.equals(name, picture.name) &&
                Objects.equals(slogan, picture.slogan) &&
                Arrays.equals(image, picture.image) &&
                Objects.equals(premiereDate, picture.premiereDate) &&
                Objects.equals(countries, picture.countries) &&
                Objects.equals(directors, picture.directors) &&
                Objects.equals(genres, picture.genres) &&
                Objects.equals(reviews, picture.reviews);
    }

    /** Overrides standard {@code Object} class method hashCode().
     * @return int hashCode value of object.
     *
     * @see java.lang.Object
     */
    @Override
    public int hashCode() {

        int result = Objects.hash(super.hashCode(), pictureId, pictureType, parts, name, slogan, duration, budget, rating, year, ageLimit, premiereDate, countries, directors, genres, reviews);
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }

    /** Overrides standard {@code Object} class method toString().
     * @return String object representation.
     *
     * @see java.lang.Object
     */
    @Override
    public String toString() {
        return "Picture{" +
                "pictureId=" + pictureId +
                ", pictureType=" + pictureType +
                ", parts=" + parts +
                ", name='" + name + '\'' +
                ", slogan='" + slogan + '\'' +
                ", duration=" + duration +
                ", budget=" + budget +
                ", image=" + Arrays.toString(image) +
                ", rating=" + rating +
                ", year=" + year +
                ", ageLimit=" + ageLimit +
                ", premiereDate=" + premiereDate +
                ", countries=" + countries +
                ", directors=" + directors +
                ", genres=" + genres +
                ", reviews=" + reviews +
                '}';
    }
}
