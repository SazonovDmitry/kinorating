<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctm" uri="customtags" %>
<ctm:locale value="${sessionScope.currentLocale}"/>
<fmt:setBundle basename="properties/pagecontent" var="rb"/>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><fmt:message key="admin.page" bundle="${ rb }" /></title>
    <link href="../../css/normalize.css" rel="stylesheet">
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/main.css" rel="stylesheet">
    <link href="../../css/background.css" rel="stylesheet">
    <link href="../../css/fonts/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <header>
        <c:import url="../common/header.jsp" />
    </header>


    <div class="container">
        <div class="row align-self-center col-md-12 ">
            <div class="col-md-4 "></div>
            <div class="col-md-8 ">
                <h4><fmt:message key="main.welcome" bundle="${ rb }" /></h4>
            </div>
        </div>
        <div class="row align-self-center col-md-12 ">
            <div class="col-md-1 "></div>
            <!--office-->
            <div class="col-md-11 block rounded">

                <!--Personal data-->
                <c:choose>
                    <c:when test="${ sessionScope.userRole eq 'USER'}" >
                        <h2 class="red-line"><fmt:message key="admin.welcome" bundle="${ rb }" /></h2>
                        <h4 class="red-line"><fmt:message key="admin.welcome.text" bundle="${ rb }" /></h4>
                        <br/>

                        <!-- Modal rools and terms-->
                        <div class="col-12">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                                <fmt:message key="admin.rools" bundle="${ rb }" />
                            </button>

                            <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle"><fmt:message key="admin.rools" bundle="${ rb }" /></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <h6 class="red-line"> <fmt:message key="admin.rools1" bundle="${ rb }" /></h6>
                                            <h6 class="red-line"> <fmt:message key="admin.rools2" bundle="${ rb }" /></h6>
                                            <h6 class="red-line"> <fmt:message key="admin.rools3" bundle="${ rb }" /></h6>
                                            <h6 class="red-line"> <fmt:message key="admin.rools4" bundle="${ rb }" /></h6>
                                            <h6 class="red-line"> <fmt:message key="admin.rools5" bundle="${ rb }" /></h6>
                                            <h6 class="red-line"> <fmt:message key="admin.rools6" bundle="${ rb }" /></h6>
                                            <h6 class="red-line"> <fmt:message key="admin.rools7" bundle="${ rb }" /></h6>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-5">
                                <h5 class="red-line"><fmt:message key="admin.personal.data" bundle="${ rb }" /></h5>
                                <br/>
                                <dl class="row">
                                    <dt class="col-sm-5">
                                        <fmt:message key="admin.personal.name" bundle="${ rb }" />
                                    </dt>
                                    <dd class="col-sm-7">
                                            ${sessionScope.currentUser.name}
                                    </dd>
                                    <dt class="col-sm-5">
                                        <fmt:message key="admin.personal.email" bundle="${ rb }" />
                                    </dt>
                                    <dd class="col-sm-7">
                                            ${sessionScope.currentUser.email}
                                    </dd>
                                    <dt class="col-sm-5">
                                        <fmt:message key="admin.personal.status" bundle="${ rb }" />
                                    </dt>
                                    <dd class="col-sm-7">
                                        <c:choose>
                                            <c:when test="${sessionScope.currentUser.status eq 'KINOMAN'}" >
                                                <fmt:message key="kinoman" bundle="${ rb }" />
                                            </c:when>
                                            <c:when test="${sessionScope.currentUser.status eq 'KINO_EXPERT'}" >
                                                <fmt:message key="expert" bundle="${ rb }" />
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:message key="beginner" bundle="${ rb }" />
                                            </c:otherwise>
                                        </c:choose>
                                    </dd>

                                        <%--Для забаненых пользователей--%>
                                    <c:if test="${sessionScope.currentUser.isBanned() eq true}">
                                        <dt class="col-sm-5">
                                            <fmt:message key="admin.personal.ban" bundle="${ rb }" />
                                        </dt>
                                        <dd class="col-sm-7">
                                                ${sessionScope.currentUser.banTime}
                                        </dd>
                                        <div class="col-12">
                                                ${sessionScope.bannedUserMessage}
                                        </div>
                                    </c:if>
                                </dl>
                            </div>
                            <div class="col-7">
                                <c:choose>
                                    <c:when test="${sessionScope.currentUser.status eq 'KINOMAN'}" >
                                        <img src="../../images/oscar.jpg" width="150" height="150">
                                    </c:when>
                                    <c:when test="${sessionScope.currentUser.status eq 'KINO_EXPERT'}" >
                                        <img src="../../images/globe.jpg" width="150" height="150">
                                    </c:when>
                                    <c:otherwise>
                                        <img src="../../images/razzie.jpg" width="150" height="150">
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>

                        <c:if test="${sessionScope.currentUser.isBanned() eq false}">
                            <div class="row m-1">
                                <div class="col-md-3">
                                    <button class="btn btn-primary" type="button" data-toggle="collapse"
                                            data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                        <fmt:message key="admin.personal.data.change" bundle="${ rb }" />
                                    </button>
                                </div>
                                <div class="col-md-9">
                                    <h5 class="text-success">${requestScope.newDataEstablished}</h5>
                                    <h5 class="text-danger">${requestScope.dataChangeFailed}</h5>
                                    <h5 class="text-danger">${requestScope.emptyUserFormFields}</h5>
                                </div>
                            </div>

                            <div class="collapse" id="collapseExample">
                                <div class="row m-2 rounded form-block">
                                    <div class="col-3">
                                        <!-- Icon -->
                                        <div >
                                            <img src="../../images/icon.svg" id="icon" alt="User Icon" />
                                        </div>
                                        <p class="red-line p-2">
                                            <fmt:message key="admin.user.message" bundle="${ rb }" />
                                        </p>
                                    </div>
                                    <div class="col-9">
                                        <!-- Login Form -->

                                        <form name="loginForm" method="POST" action="${ pageContext.servletContext.contextPath }/controller">
                                            <input type="hidden" name="command" value="reestablish" />
                                            <input type="hidden" name="fromPage" value="${pageContext.request.requestURI}" />
                                            <div class="row p-2">
                                                <div class="col-4">
                                                    <c:set var="nameph"><fmt:message key="admin.user.new.name" bundle="${ rb }" /></c:set>
                                                    <input type="text" name="name" id="name" class="form-control "
                                                           placeholder="${nameph}" value="" pattern="^([a-zA-Zа-яА-ЯіўІЎ]+(-?)){2,40}$"
                                                           data-toggle="tooltip" data-placement="right"
                                                           title="<fmt:message key="tooltip.name" bundle="${rb}"/>"
                                                           oninvalid="setCustomValidity('<fmt:message key="name.incorrect" bundle="${rb}"/>')"
                                                           onchange="setCustomValidity('')"
                                                           oninput="setCustomValidity('')"
                                                           onblur="setCustomValidity('')"/>
                                                </div>
                                                <div class="col-8">
                                                    <h5 class="text-danger">${requestScope.errorNameMessage}</h5>
                                                </div>
                                            </div>
                                            <div class="row p-2">
                                                <div class="col-4">
                                                    <c:set var="emailph"><fmt:message key="admin.user.new.email" bundle="${ rb }" /></c:set>
                                                    <input type="email" name="email" id="email" class="form-control "
                                                           placeholder="${emailph}" value="" pattern="^([\w|\.]{6,})@(\w+\.)(\w{2,4})$"
                                                           data-toggle="tooltip" data-placement="right"
                                                           title="<fmt:message key="tooltip.email" bundle="${rb}"/>"
                                                           oninvalid="setCustomValidity('<fmt:message key="email.incorrect" bundle="${rb}"/>')"
                                                           onchange="setCustomValidity('')"
                                                           oninput="setCustomValidity('')"
                                                           onblur="setCustomValidity('')"/>
                                                </div>
                                                <div class="col-8">
                                                    <h5 class="text-danger">${requestScope.errorEmailMessage}</h5>
                                                </div>
                                            </div>
                                            <div class="row p-2">
                                                <div class="col-4">
                                                    <c:set var="loginph"><fmt:message key="admin.user.new.login" bundle="${ rb }" /></c:set>
                                                    <input type="text" name="login" id="login" class="form-control "
                                                           placeholder="${loginph}" value="" pattern="^\w{5,30}$"
                                                           data-toggle="tooltip" data-placement="right"
                                                           title="<fmt:message key="tooltip.login" bundle="${rb}"/>"
                                                           oninvalid="setCustomValidity('<fmt:message key="login.incorrect" bundle="${rb}"/>')"
                                                           onchange="setCustomValidity('')"
                                                           oninput="setCustomValidity('')"
                                                           onblur="setCustomValidity('')"/>
                                                </div>
                                                <div class="col-8">
                                                    <h5 class="text-danger">${requestScope.errorLoginMessage}</h5>
                                                </div>
                                            </div>
                                            <div class="row p-2">
                                                <div class="col-4">
                                                    <c:set var="passph"><fmt:message key="admin.user.new.password" bundle="${ rb }" /></c:set>
                                                    <input type="password" name="password" id="password" class="form-control "
                                                           placeholder="${passph}" value="" pattern="^.{5,40}$"
                                                           data-toggle="tooltip" data-placement="right"
                                                           title="<fmt:message key="tooltip.password" bundle="${rb}"/>"
                                                           oninvalid="setCustomValidity('<fmt:message key="password.incorrect" bundle="${rb}"/>')"
                                                           onchange="setCustomValidity('')"
                                                           oninput="setCustomValidity('')"
                                                           onblur="setCustomValidity('')"/>
                                                </div>
                                                <div class="col-8">
                                                    <h5 class="text-danger">${requestScope.errorPasswordMessage}<br/></h5>
                                                </div>
                                            </div>
                                            <div class="row p-2">
                                                <div class="col-12">
                                                    <c:set var="submitSI"><fmt:message key="admin.send.user.form" bundle="${ rb }" /></c:set>
                                                    <input type="submit" class="btn btn-primary" value="${pageScope.submitSI}">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </c:if>

                        <%--Favorite list--%>
                        <c:set var="count" value="1" scope="page" />
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th> № </th>
                                <th><fmt:message key="main.picture.title" bundle="${ rb }" /></th>
                                <th><fmt:message key="main.picture.year" bundle="${ rb }" /></th>
                                <th><fmt:message key="main.picture.rating" bundle="${ rb }" /></th>
                                <th><fmt:message key="main.picture.remove" bundle="${ rb }" /></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="elem" items="${sessionScope.currentUser.favorite}" >
                                <tr>
                                    <th scope="row">
                                        <c:out value="${count}" /><c:set var="count" value="${count + 1}" scope="page"/>
                                    </th>
                                    <td>
                                        <form name="pictureForm" method="GET"
                                              action="${ pageContext.servletContext.contextPath }/controller">
                                            <input type="hidden" name="command" value="picture"/>
                                            <input type="hidden" name="pictureId" value="${elem.pictureId}"/>
                                            <c:set var="title" value="${ elem.name }"/>
                                            <input type="submit" class="film-button" value="${title}"/>
                                        </form>
                                    </td>
                                    <td><c:out value="${ elem.year }" /></td>
                                    <td><strong><c:out value="${ elem.rating }" /></strong></td>
                                    <td>
                                        <form name="pictureForm" method="GET"
                                              action="${ pageContext.servletContext.contextPath }/controller">
                                            <input type="hidden" name="command" value="remove-favorite"/>
                                            <input type="hidden" name="pictureId" value="${elem.pictureId}"/>
                                            <c:set var="title" value="${ elem.name }"/>
                                            <label for="trash" class="fa fa-trash-o fa-lg delete">
                                                <input type="submit"  id="trash" hidden/>
                                            </label>
                                        </form>
                                        <h6 class="text-danger">${requestScope.deleteFavoriteFailed}</h6>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>

                    </c:when>

                    <%--Manegment data administrator--%>
                    <c:when test="${sessionScope.userRole eq 'ADMINISTRATOR'}" >

                        <!-- Add director-->

                        <div class="row m-2 rounded form-block">

                            <div class="col-12 rounded ">
                                <div class="row">
                                    <div class="bookmarks-block rounded-top col-4">
                                        <h5><a class="custom-link" href="${pageContext.servletContext.contextPath}/jsp/admin/admin.jsp">
                                            <fmt:message key="admin.add.picture" bundle="${ rb }" />
                                        </a></h5>
                                    </div>
                                    <div class="form-block rounded-top col-4 border-bottom-0">
                                        <h3><a class="nav-link disabled" href="${pageContext.servletContext.contextPath}/jsp/admin/director.jsp">
                                            <fmt:message key="admin.add.director" bundle="${ rb }" />
                                        </a></h3>
                                    </div>
                                    <div class="bookmarks-block rounded-top col-4">
                                        <h5><a class="custom-link" href="${pageContext.servletContext.contextPath}/jsp/admin/management.jsp">
                                            <fmt:message key="admin.manage.entity" bundle="${ rb }" />
                                        </a></h5>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <h5 class="text-success"> ${requestScope.directorSuccessfullyDeleted}</h5>
                                <h5 class="text-danger">${requestScope.directorDeleteFailed}</h5>
                                <h5 class="text-success"> ${requestScope.newDirectorCreated}</h5>
                                <h5 class="text-danger">${requestScope.directorCreationFailed}</h5>
                            </div>

                            <div class="col-12">

                                <form name="directorForm" method="POST" enctype="multipart/form-data" action="${ pageContext.servletContext.contextPath }/controller">
                                    <input type="hidden" name="command" value="create-director" />
                                    <input type="hidden" name="fromPage" value="${pageContext.request.requestURI}" />
                                    <div class="row pt-5">
                                        <div class="form-group col-7 ">
                                            <div class="form-group row">
                                                <label for="name" class="col-3 col-form-label required"><fmt:message key="admin.placeholder.director.name" bundle="${ rb }" /></label>
                                                <div class="col-9">
                                                    <c:set var="nameph"><fmt:message key="admin.placeholder.director.name" bundle="${ rb }" /></c:set>
                                                    <input type="text" name="name" id="name" class="form-control"
                                                           placeholder="${nameph}" value="" required pattern="^([a-zA-Zа-яА-ЯіўІЎ]+(-?)){2,40}$"
                                                           title="<fmt:message key="admin.director.name.pattern" bundle="${rb}"/>"
                                                           oninvalid="setCustomValidity('<fmt:message key="admin.director.name.invalid" bundle="${rb}"/>')"
                                                           onchange="setCustomValidity('')"
                                                           oninput="setCustomValidity('')"
                                                           onblur="setCustomValidity('')"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <h5 class="text-danger">${requestScope.errorDirectorNameMessage}</h5>
                                        </div>
                                    </div>
                                    <div class="row pt-2">
                                        <div class="form-group col-7 ">
                                            <div class="form-group row">
                                                <label for="surnname" class="col-3 col-form-label required"><fmt:message key="admin.placeholder.director.surnname" bundle="${ rb }" /></label>
                                                <div class="col-9">
                                                    <c:set var="surnnameph"><fmt:message key="admin.placeholder.director.surnname" bundle="${ rb }" /></c:set>
                                                    <input type="text" name="surnname" id="surnname" class="form-control "
                                                           placeholder="${surnnameph}" value="" required pattern="^([a-zA-Zа-яА-ЯіўІЎ]+(-?)){2,40}$"
                                                           title="<fmt:message key="admin.director.name.pattern" bundle="${rb}"/>"
                                                           oninvalid="setCustomValidity('<fmt:message key="admin.director.name.invalid" bundle="${rb}"/>')"
                                                           onchange="setCustomValidity('')"
                                                           oninput="setCustomValidity('')"
                                                           onblur="setCustomValidity('')"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <h5 class="text-danger">${requestScope.errorDirectorSurnnameMessage}</h5>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="form-group col-7">
                                            <div class="form-group row">
                                                <label for="birthday" class="col-3 col-form-label required"><fmt:message key="admin.placeholder.director.birthday" bundle="${ rb }" /></label>
                                                <div class="col-9">
                                                    <c:set var="birthdayph"><fmt:message key="admin.placeholder.director.birthday" bundle="${ rb }" /></c:set>
                                                    <input type="date" name="birthday" id="birthday" class="form-control "
                                                           placeholder="${birthdayph}"  value="" required
                                                           title="<fmt:message key="admin.director.date.title" bundle="${rb}"/>"
                                                           oninvalid="setCustomValidity('<fmt:message key="admin.director.date.invalid" bundle="${rb}"/>')"
                                                           onchange="setCustomValidity('')"
                                                           oninput="setCustomValidity('')"
                                                           onblur="setCustomValidity('')"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <h5 class="text-danger">${requestScope.errorDirectorBirthdayMessage}</h5>
                                        </div>
                                    </div>

                                    <div class="row ">
                                        <label for="dropdownMenuButton1" class="col-2 col-form-label required">
                                            <fmt:message key="admin.placeholder.director.countryid" bundle="${ rb }" />
                                        </label>

                                        <div class="dropdown pb-4 col-9">
                                            <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton1"
                                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <fmt:message key="admin.button.countries" bundle="${ rb }" />
                                            </button>
                                            <div class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton1">
                                                <c:forEach var="countryElem" items="${applicationScope.countriesDirectory}" >
                                                    <div class="form-check form-check-inline col-12 p-1">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" name="countries" type="checkbox" id="inlineCheckbox1"
                                                                   value="${ countryElem.countryId }" >
                                                            <span><c:out value="${ countryElem.name }" /></span>
                                                        </label>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row ">
                                        <div class="form-group col-8">
                                            <div class="form-group row">
                                                <label for="image" class="col-3 col-form-label required">
                                                    <fmt:message key="admin.placeholder.picture.image" bundle="${ rb }" />
                                                </label>
                                                <div class="col-9">
                                                    <div class="row">
                                                        <div class="file-upload col-6">
                                                            <label>
                                                                <c:set var="imagetitle">
                                                                    <fmt:message key="admin.placeholder.picture.image.file" bundle="${ rb }" />
                                                                </c:set>
                                                                <input type="file" name="image" id="image"  accept="image/jpeg" title="${imagetitle}"
                                                                       value="" aria-describedby="fileHelp" size="307200" required />
                                                                <span><fmt:message key="admin.placeholder.picture.image.choose" bundle="${ rb }" /></span>
                                                            </label>
                                                        </div>
                                                        <div class=" col-6">
                                                            <label for="filename"></label>
                                                            <input type="text" id="filename" class="filename" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <h5 class="text-danger">${requestScope.errorDirectorImageMessage}</h5>
                                        </div>
                                    </div>
                                    <div class="row pb-2">
                                        <div class="col-12">
                                            <c:set var="submitSI">
                                                <fmt:message key="admin.send.director.form" bundle="${ rb }" />
                                            </c:set>
                                            <input type="submit" class="btn btn-success" value="${pageScope.submitSI}">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="col-12">
                            <!--Delete director-->
                            <form name="deletdDirectorForm" method="POST" action="${ pageContext.servletContext.contextPath }/controller">
                                <input type="hidden" name="command" value="delete-director" />
                                <input type="hidden" name="fromPage" value="${pageContext.request.requestURI}" />
                                <div class="row pt-2">
                                    <div class="col-12">
                                        <h5><fmt:message key="admin.delete.director" bundle="${ rb }" /></h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-7">
                                        <div class="form-group row">
                                            <label for="directorId" class="col-3 col-form-label required"><fmt:message key="admin.delete.director.id" bundle="${ rb }" /></label>
                                            <div class="col-9">
                                                <c:set var="directorph"><fmt:message key="admin.delete.director.id" bundle="${ rb }" /></c:set>
                                                <input type="number" name="directorId" id="directorId" class="form-control" placeholder="${directorph}"
                                                       value="" required min="1"
                                                       title="<fmt:message key="admin.picture.delete.title" bundle="${rb}"/>"
                                                       oninvalid="setCustomValidity('<fmt:message key="admin.picture.parts.invalid" bundle="${rb}"/>')"
                                                       onchange="setCustomValidity('')"
                                                       oninput="setCustomValidity('')"
                                                       onblur="setCustomValidity('')"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-5 ">
                                        <c:set var="submitSI"><fmt:message key="admin.delete.director.submit" bundle="${ rb }" /></c:set>
                                        <input type="submit" class="btn btn-primary" value="${pageScope.submitSI}">
                                    </div>
                                </div>
                            </form>
                        </div>

                    </c:when>
                    <%--guests--%>
                    <c:otherwise>
                        <h2 class="red-line"><fmt:message key="admin.welcome" bundle="${ rb }" /></h2>
                        <h4 class="red-line"><fmt:message key="admin.welcome.text" bundle="${ rb }" /></h4>
                        <h4 class="red-line"><fmt:message key="admin.welcome.text.before" bundle="${ rb }" /></h4>
                        <br/>

                        <!-- Modal rools and terms-->
                        <div class="col-12 pb-4">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong1">
                                <fmt:message key="admin.rools" bundle="${ rb }" />
                            </button>
                        </div>

                        <div class="modal fade" id="exampleModalLong1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle1" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle1"><fmt:message key="admin.rools" bundle="${ rb }" /></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <h6 class="red-line"> <fmt:message key="admin.rools1" bundle="${ rb }" /></h6>
                                        <h6 class="red-line"> <fmt:message key="admin.rools2" bundle="${ rb }" /></h6>
                                        <h6 class="red-line"> <fmt:message key="admin.rools3" bundle="${ rb }" /></h6>
                                        <h6 class="red-line"> <fmt:message key="admin.rools4" bundle="${ rb }" /></h6>
                                        <h6 class="red-line"> <fmt:message key="admin.rools5" bundle="${ rb }" /></h6>
                                        <h6 class="red-line"> <fmt:message key="admin.rools6" bundle="${ rb }" /></h6>
                                        <h6 class="red-line"> <fmt:message key="admin.rools7" bundle="${ rb }" /></h6>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a class="nav-link pb-4" href="${pageContext.servletContext.contextPath}/jsp/login.jsp">
                            <fmt:message key="header.login" bundle="${ rb }" />
                        </a>
                        <a class="nav-link pb-5" href="${pageContext.servletContext.contextPath}/jsp/registration.jsp">
                            <fmt:message key="admin.register" bundle="${ rb }" />
                        </a>
                    </c:otherwise>
                </c:choose>

            </div>
            <div class="row align-self-center col-md-12 ">
                <div class="col-md-4 "></div>
                <div class="col-md-8 ">
                    <h4 style="opacity: 0"><fmt:message key="main.welcome" bundle="${ rb }" /></h4>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <c:import url="../common/footer.jsp" charEncoding="UTF-8"/>
    </footer>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/popper.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/loadImage.js"></script>
</body>
</html>
