<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctm" uri="customtags" %>
<ctm:locale value="${sessionScope.currentLocale}"/>
<fmt:setBundle basename="properties/pagecontent" var="rb"/>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><fmt:message key="registration.page" bundle="${ rb }" /></title>
    <link rel="stylesheet" href="../css/normalize.css">
    <link href="../css/register.css" rel="stylesheet">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">

</head>
<body>
    <header>
        <c:import url="common/header.jsp" charEncoding="utf-8"/>
    </header>
    <div class="container-fluid p-wrapper login-parent">
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Icon -->
                <div class="fadeIn first">
                    <img src="../images/icon.svg" id="icon" alt="User Icon" />
                </div>
                <!-- Login Form -->

                <form name="loginForm" method="POST" action="${ pageContext.servletContext.contextPath }/controller">
                    <input type="hidden" name="command" value="register" />
                    <input type="hidden" name="fromPage" value="${pageContext.request.requestURI}" />
                    <c:set var="nameph"><fmt:message key="placeholder.name" bundle="${ rb }" /></c:set>
                    <input type="text" name="name" id="name" class="fadeIn first inputt"
                           placeholder="${nameph}" value=""
                           pattern="^([a-zA-Zа-яА-ЯіўІЎ]+(-?)){2,40}$" required
                           data-toggle="tooltip" data-placement="right"
                           title="<fmt:message key="tooltip.name" bundle="${rb}"/>"
                           oninvalid="setCustomValidity('<fmt:message key="name.incorrect" bundle="${rb}"/>')"
                           onchange="setCustomValidity('')"
                           oninput="setCustomValidity('')"
                           onblur="setCustomValidity('')"/>
                    <br/>
                    <h5 class="text-danger"><strong>${requestScope.errorNameMessage}</strong></h5>
                    <c:set var="emailph"><fmt:message key="placeholder.email" bundle="${ rb }" /></c:set>
                    <input type="text" name="email" id="email" class="fadeIn second inputt"
                           placeholder="${emailph}" value=""
                           pattern="^([\w|\.]{6,})@(\w+\.)(\w{2,4})$" required
                           data-toggle="tooltip" data-placement="right"
                           title="<fmt:message key="tooltip.email" bundle="${rb}"/>"
                           oninvalid="setCustomValidity('<fmt:message key="email.incorrect" bundle="${rb}"/>')"
                           onchange="setCustomValidity('')"
                           oninput="setCustomValidity('')"
                           onblur="setCustomValidity('')"/>
                    <br/>
                    <h5 class="text-danger"><strong>${requestScope.errorEmailMessage}</strong></h5>
                    <c:set var="loginph"><fmt:message key="placeholder.login" bundle="${ rb }" /></c:set>
                    <input type="text" name="login" id="login" class="fadeIn third inputt"
                           placeholder="${loginph}" value=""
                           pattern="^\w{5,30}$" required
                           data-toggle="tooltip" data-placement="right"
                           title="<fmt:message key="tooltip.login" bundle="${rb}"/>"
                           oninvalid="setCustomValidity('<fmt:message key="login.incorrect" bundle="${rb}"/>')"
                           onchange="setCustomValidity('')"
                           oninput="setCustomValidity('')"
                           onblur="setCustomValidity('')"/>
                    <br/>
                    <h5 class="text-danger"><strong>${requestScope.errorLoginMessage}</strong></h5>
                    <c:set var="passph"><fmt:message key="placeholder.password" bundle="${ rb }" /></c:set>
                    <input type="password" name="password" id="password" class="fadeIn fourth inputt"
                           placeholder="${passph}" value=""
                           pattern="^.{5,40}$" required
                           data-toggle="tooltip" data-placement="right"
                           title="<fmt:message key="tooltip.password" bundle="${rb}"/>"
                           oninvalid="setCustomValidity('<fmt:message key="password.incorrect" bundle="${rb}"/>')"
                           onchange="setCustomValidity('')"
                           oninput="setCustomValidity('')"
                           onblur="setCustomValidity('')"/>
                    <br/>
                    <h5 class="text-danger"><strong>${requestScope.errorPasswordMessage}</strong><br/></h5>
                    <h5 class="text-danger"><strong>${requestScope.registrationFailed}</strong></h5>
                    <c:set var="submitSI"><fmt:message key="button.sign.in" bundle="${ rb }" /></c:set>
                    <input type="submit" class="fadeIn fourth inputA" value="${pageScope.submitSI}"><br/>
                    <a class="underlineHover" href="${ pageContext.servletContext.contextPath }/jsp/login.jsp" >
                    <fmt:message key="ref.sign.in" bundle="${ rb }" /></a>
                </form>

                <div id="formFooter">
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <c:import url="common/footer.jsp" charEncoding="utf-8"/>
    </footer>

    <script src="../js/jquery.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
</body>
</html>
