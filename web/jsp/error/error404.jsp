<%@ page isErrorPage="true" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctm" uri="customtags" %>
<ctm:locale value="${sessionScope.currentLocale}"/>
<fmt:setBundle basename="properties/pagecontent" var="rb"/>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><fmt:message key="error.page" bundle="${ rb }" /></title>
    <link rel="stylesheet" href="../../css/normalize.css">
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/main.css" rel="stylesheet">
    <link href="../../css/background.css" rel="stylesheet">
    <link href="../../css/fonts/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <header>
        <c:import url="../common/header.jsp" charEncoding="UTF-8"/>
    </header>
    <div class="container-fluid p-wrapper ">
        <div class="col-md-8 align-self-center block">
            <h2>Something went wrong((</h2>
            Request from ${pageContext.errorData.requestURI} is failed
            <br/>
            Servlet name: ${pageContext.errorData.servletName}
            <br/>
            Status code: ${pageContext.errorData.statusCode}
            <br/>
            Exception: ${pageContext.exception}
        </div>
    </div>
    <footer class="footer">
        <c:import url="../common/footer.jsp" charEncoding="UTF-8"/>
    </footer>
</body>
</html>
