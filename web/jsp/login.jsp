<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctm" uri="customtags" %>
<ctm:locale value="${sessionScope.currentLocale}"/>
<fmt:setBundle basename="properties/pagecontent" var="rb"/>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><fmt:message key="login.page" bundle="${ rb }" /></title>
    <link rel="stylesheet" href="../css/normalize.css">
    <link href="../css/login.css" rel="stylesheet">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">
</head>
<body>
    <header>
        <c:import url="common/header.jsp" charEncoding="UTF-8"/>
    </header>

    <div class="container-fluid p-wrapper login-parent">

            <div class="wrapper fadeInDown">
                <div id="formContent">

                    <!-- Icon -->
                    <div class="fadeIn first">
                        <img src="../images/icon.svg" id="icon" alt="User Icon" />
                    </div>

                    <!-- Login Form -->
                    <form name="loginForm" method="GET" action="${ pageContext.servletContext.contextPath }/controller">
                        <input type="hidden" name="command" value="login" />
                        <c:set var="loginPH"><fmt:message key="placeholder.login" bundle="${ rb }" /></c:set>
                        <input type="text" name="login" id="login" class="fadeIn second inputt"
                               placeholder="${loginPH}" value=""
                               pattern="^\w{5,30}$" required
                               data-toggle="tooltip" data-placement="right"
                               title="<fmt:message key="tooltip.login" bundle="${rb}"/>"
                               oninvalid="setCustomValidity('<fmt:message key="login.incorrect" bundle="${rb}"/>')"
                               onchange="setCustomValidity('')"
                               oninput="setCustomValidity('')"
                               onblur="setCustomValidity('')"
                               onemptied="setCustomValidity('')"/>

                        <c:set var="passPH"><fmt:message key="placeholder.password" bundle="${ rb }" /></c:set>
                        <input type="password" name="password" id="password" class="fadeIn third inputt"
                               placeholder="${passPH}" value=""
                               pattern="^.{5,40}$" required
                               data-toggle="tooltip" data-placement="right"
                               title="<fmt:message key="tooltip.password" bundle="${rb}"/>"
                               oninvalid="setCustomValidity('<fmt:message key="password.incorrect" bundle="${rb}"/>')"
                               onchange="setCustomValidity('')"
                               oninput="setCustomValidity('')"
                               onblur="setCustomValidity('')"
                               onemptied="setCustomValidity('')"/>
                        <br/>
                        <p class="text-danger">${requestScope.errorLoginPassMessage}</p>
                        <br/>
                        <c:set var="submitSI"><fmt:message key="button.log.in" bundle="${ rb }" /></c:set>
                        <input type="submit" class="fadeIn fourth inputA" value="${submitSI}"/>
                    </form>

                    <!-- Remind Passowrd -->
                    <div id="formFooter">
                        <a class="underlineHover" href="${ pageContext.servletContext.contextPath }/jsp/registration.jsp">
                            <fmt:message key="login.footer.mess" bundle="${ rb }" />
                        </a>
                        <a class="underlineHover" href="${ pageContext.servletContext.contextPath }/jsp/mail.jsp">
                            <fmt:message key="login.footer.mess.pass" bundle="${ rb }" />
                        </a>
                    </div>
                </div>
            </div>
    </div>
    <footer class="footer">
        <c:import url="common/footer.jsp" charEncoding="UTF-8"/>
    </footer>

<script src="../js/jquery.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>
