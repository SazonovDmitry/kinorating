<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctm" uri="customtags" %>
<ctm:locale value="${sessionScope.currentLocale}"/>
<fmt:setBundle basename="properties/pagecontent" var="rb"/>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><fmt:message key="main.page" bundle="${ rb }" /></title>
    <link rel="stylesheet" href="../css/normalize.css">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">
    <link href="../css/fonts/font-awesome.min.css" rel="stylesheet">
</head>
<body >
    <header>
        <c:import url="common/header.jsp" charEncoding="UTF-8"/>
    </header>

    <div class="container">
        <div class="row align-self-center col-md-12 ">
            <div class="col-md-4 "></div>
            <div class="col-md-8 ">
                <h4><fmt:message key="main.welcome" bundle="${ rb }" /></h4>
            </div>
        </div>
        <div class="row align-self-center col-md-12 ">
            <div class="col-md-1 "></div>
            <div class="col-md-8 block rounded">
                <c:out value="${requestScope.pictureNotFound}"/>
                <h2>${sessionScope.currentPicture.name}<br/></h2>
                <!--Picture-->
                <div class="row justify-content-center">

                    <div class="col-3">
                        <!--Adding picture to custom list-->
                        <c:if test="${sessionScope.userRole eq 'USER'}">
                            <div class="col-12">
                                <form name="favoriteForm" method="POST"
                                      action="${ pageContext.servletContext.contextPath }/controller">
                                    <input type="hidden" name="command" value="add-favorite"/>
                                    <input type="hidden" name="fromPage" value="${pageContext.request.requestURI}" />
                                    <button type="submit" class="btn btn-info">
                                        <fmt:message key="picture.add.favorite" bundle="${ rb }" />
                                    </button>
                                </form>
                            </div>
                            <div class="col-12">
                                <p class="text-danger">${requestScope.favoriteFailed}</p>
                                <p class="text-success">${requestScope.successfulFavorite}</p>
                            </div>
                        </c:if>
                    </div>

                    <div class="col-6">
                        <img src="${ pageContext.servletContext.contextPath }/imageViewer?type=film" class="rounded" width="400"
                        height="600"/>
                    </div>
                    <div class="col-3">
                    </div>
                </div>
                <br/>
                <!--Rating button-->
                <c:if test="${sessionScope.userRole eq 'USER'&&sessionScope.currentUser.isBanned() eq false ||
                        sessionScope.userRole eq 'ADMINISTRATOR'}">
                    <div class="col-12">
                        <form name="ratingForm" method="POST"
                              action="${ pageContext.servletContext.contextPath }/controller">
                            <div class="row justify-content-center">
                                <input type="hidden" name="fromPage" value="${pageContext.request.requestURI}" />
                                <div class="col-6 ">
                                    <div class="star-rating">
                                        <input type="hidden" name="command" value="rating" />
                                        <span class="fa fa-star-o" data-rating="1"></span>
                                        <span class="fa fa-star-o" data-rating="2"></span>
                                        <span class="fa fa-star-o" data-rating="3"></span>
                                        <span class="fa fa-star-o" data-rating="4"></span>
                                        <span class="fa fa-star-o" data-rating="5"></span>
                                        <span class="fa fa-star-o" data-rating="6"></span>
                                        <span class="fa fa-star-o" data-rating="7"></span>
                                        <span class="fa fa-star-o" data-rating="8"></span>
                                        <span class="fa fa-star-o" data-rating="9"></span>
                                        <span class="fa fa-star-o" data-rating="10"></span>
                                        <input type="hidden" name="mark" class="rating-value" value="">
                                    </div>
                                </div>
                                <div class="col-2">
                                    <button type="submit" class="btn btn-primary">
                                        <fmt:message key="picture.rating.send" bundle="${ rb }" />
                                    </button>
                                </div>
                            </div>
                            <p class="text-danger">${requestScope.ratingFailed}</p>
                            <p class="text-success">${requestScope.successfulRating}</p>
                        </form>
                    </div>
                </c:if>

                <!--Picture description-->
                <dl class="row">

                    <dt class="col-sm-4"><fmt:message key="picture.type" bundle="${ rb }" /></dt>
                    <c:choose>
                        <c:when test="${sessionScope.currentPicture.pictureType eq 'FILM'}">
                            <dd class="col-sm-8"><fmt:message key="picture.type.movie" bundle="${ rb }" /></dd>
                        </c:when>
                        <c:otherwise><dd class="col-sm-8"><fmt:message key="picture.type.serial" bundle="${ rb }" /></dd></c:otherwise>
                    </c:choose>

                    <dt class="col-sm-4"><fmt:message key="picture.issue.year" bundle="${ rb }" /></dt>
                    <dd class="col-sm-8">${sessionScope.currentPicture.year}</dd>
                    <dt class="col-sm-4"><fmt:message key="picture.country" bundle="${ rb }" /></dt>
                    <dd class="col-sm-8">
                        <c:forEach var="country" items="${sessionScope.currentPicture.getCountries()}" >
                            <c:out value="${ country.name}"/>,
                        </c:forEach></dd>
                    <!--Directors-->
                    <dt class="col-sm-4"><fmt:message key="picture.director" bundle="${ rb }" /></dt>
                    <dd class="col-sm-8">
                        <c:forEach var="elem" items="${sessionScope.currentPicture.directors}" >
                            <form name="directorForm" method="GET" action="${ pageContext.servletContext.contextPath }/controller">
                                <input type="hidden" name="command" value="director"/>
                                <input type="hidden" name="directorId" value="${elem.directorId}"/>
                                <c:set var="title" value="${elem.name} ${elem.lastName},"/> ${requestScope.directorNotFound}
                                <input type="submit" class="film-button" value="${title}"/>
                            </form>
                        </c:forEach></dd>

                    <dt class="col-sm-4"><fmt:message key="picture.genre" bundle="${ rb }" /></dt>
                    <dd class="col-sm-8">
                        <c:forEach var="genre" items="${sessionScope.currentPicture.getGenres()}" >
                            <c:out value="${ genre.name}"/>,
                        </c:forEach>
                    </dd>
                    <dt class="col-sm-4"><fmt:message key="picture.parts" bundle="${ rb }" /></dt>
                    <dd class="col-sm-8">${sessionScope.currentPicture.parts}</dd>
                    <dt class="col-sm-4"><fmt:message key="picture.slogan" bundle="${ rb }" /></dt>
                    <dd class="col-sm-8">
                        <ctm:slogan value="${sessionScope.currentPicture.slogan}"/>
                    </dd>
                    <dt class="col-sm-4"><fmt:message key="picture.duration" bundle="${ rb }" /></dt>
                    <dd class="col-sm-8">${sessionScope.currentPicture.duration}
                        <fmt:message key="picture.min" bundle="${ rb }" /></dd>
                    <dt class="col-sm-4"><fmt:message key="picture.budget" bundle="${ rb }" /></dt>
                    <c:choose>
                        <c:when test="${sessionScope.currentPicture.budget > 0}">
                            <dd class="col-sm-8"><fmt:formatNumber value="${sessionScope.currentPicture.budget}"/>
                                <fmt:message key="picture.dollars" bundle="${ rb }" /></dd></dd>
                        </c:when>
                        <c:otherwise><dd class="col-sm-8">-</dd></c:otherwise>
                    </c:choose>
                    <dt class="col-sm-4"><fmt:message key="picture.rating" bundle="${ rb }" /></dt>
                    <dd class="col-sm-8">${sessionScope.currentPicture.rating}</dd>
                    <dt class="col-sm-4"><fmt:message key="picture.age.limit" bundle="${ rb }" /></dt>
                    <dd class="col-sm-8">${sessionScope.currentPicture.ageLimit} +</dd>
                    <dt class="col-sm-4"><fmt:message key="picture.premiere" bundle="${ rb }" /></dt>
                    <dd class="col-sm-8">
                        <ctm:localDateTime value="${sessionScope.currentPicture.premiereDate}" />
                    </dd>
                    <!--Reviews-->
                    <dt class="col-sm-9"><fmt:message key="picture.reviews" bundle="${ rb }" /></dt>
                    <!--Add review button-->
                    <dd class="col-sm-3">
                        <c:if test="${sessionScope.userRole eq 'USER'&&sessionScope.currentUser.isBanned() eq false ||
                        sessionScope.userRole eq 'ADMINISTRATOR'}">
                            <button type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#exampleModal" data-whatever="@mdo">
                                <fmt:message key="write.review.button" bundle="${ rb }" />
                            </button>

                            <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                <fmt:message key="picture.review.header" bundle="${ rb }" />
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form name="reviewForm" method="POST"
                                              action="${ pageContext.servletContext.contextPath }/controller">
                                            <div class="modal-body">
                                                    <input type="hidden" name="command" value="review" />
                                                    <input type="hidden" name="fromPage" value="${pageContext.request.requestURI}" />
                                                    <div class="form-group">
                                                        <label for="message-text" class="form-control-label">
                                                            <fmt:message key="picture.review.body" bundle="${ rb }" />
                                                        </label>
                                                        <textarea type="text" class="form-control" id="message-text"
                                                                  name="reviewContent"></textarea>
                                                    </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    <fmt:message key="picture.review.close" bundle="${ rb }" />
                                                </button>
                                                <button type="submit" class="btn btn-primary">
                                                    <fmt:message key="picture.review.send" bundle="${ rb }" />
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </dd>
                    <dd class="col-sm-12">
                        <!--Review adding result-->
                        <c:if test="${sessionScope.userRole eq 'ADMINISTRATOR' || sessionScope.userRole eq 'USER'}">
                            <p class="text-success">${requestScope.reviewAdded}</p>
                            <p class="text-danger">${requestScope.reviewFailed}</p>
                            <p class="text-danger">${requestScope.reviewEmpty}</p>
                        </c:if>
                    </dd>
                    <!--Reviews-->
                    <c:set var="listCount" value="${sessionScope.currentPicture.getReviews().size() / 10}" scope="page" />

                    <dd class="col-sm-12">
                        <c:forEach var="review" items="${sessionScope.currentPicture.getReviews()}" varStatus="status" >
                            <c:if test="${requestScope.min != null ? status.count > requestScope.min &&
                                  status.count < requestScope.max : status.count <= 10}">

                                    <c:choose>
                                        <c:when test="${ review.status eq 'KINOMAN'}" >
                                            <c:set var="color" value="gold"/>
                                        </c:when>
                                        <c:when test="${ review.status eq 'KINO_EXPERT'} " >
                                            <c:set var="color" value="green"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="color" value="white"/>
                                        </c:otherwise>
                                    </c:choose>

                                <div class="${color} rounded p-1 m-2 ">
                                    <c:out value="${review.userName }"/>  <ctm:localDateTime value="${review.dateTime}" />
                                    <c:if test="${sessionScope.userRole eq 'ADMINISTRATOR'}">
                                        <fmt:message key="picture.review.id" bundle="${ rb }" /><c:out value="${review.reviewId }"/>
                                        <fmt:message key="picture.user.id" bundle="${ rb }" /><c:out value="${review.userId }"/>
                                    </c:if>
                                    <hr/>
                                    <div class="rounded m-2">
                                        <p class="red-line"><c:out value="${review.content }"/></p>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>

                        <div class="col-12 row justify-content-center">
                            <div class="btn-toolbar col-7 justify-content-center pb-3" role="toolbar" aria-label="Toolbar with button groups">
                                <c:if test="${sessionScope.currentPicture.getReviews() != null && sessionScope.currentPicture.getReviews().size() > 10}">
                                    <c:forEach var="elem" items="${sessionScope.currentPicture.getReviews()}" varStatus="status">
                                        <c:if test="${(sessionScope.currentPicture.getReviews().size() % 10) != 0 ? status.count <= listCount + 1 :
                                            status.count <= listCount}">
                                            <form name="listForm" method="GET" action="${ pageContext.servletContext.contextPath }/controller">
                                                <input type="hidden" name="command" value="page-iterator"/>
                                                <input type="hidden" name="address" value="picture"/>
                                                <div class="btn-group mr-2" role="group" aria-label="First group">
                                                    <button type="submit" name="pageCount" class="btn btn-info" value="${status.count}">
                                                        <c:out value="${status.count}" />
                                                    </button>
                                                </div>
                                            </form>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </div>
                        </div>
                    </dd>
                </dl>
            </div>
            <!--Director block-->
            <div class="col-md-3">
                <c:if test="${sessionScope.currentDirector != null}">
                    <div class="col-md-12 block2 rounded " >
                        <h5><fmt:message key="picture.director.panel" bundle="${ rb }" /></h5>
                        <div class="row justify-content-center">
                            <div class="col-12">
                                <img src="${ pageContext.servletContext.contextPath }/imageViewer?type=director" class="rounded"
                                height="200" width="150"/>
                            </div>
                        </div>
                        <dl class="row">
                            <dt class="col-sm-12">
                                <h3>${sessionScope.currentDirector.name} ${sessionScope.currentDirector.lastName}</h3>
                            </dt>
                            <dt class="col-sm-4">
                                <fmt:message key="picture.director.country" bundle="${ rb }" />
                            </dt>
                            <dd class="col-sm-8">
                                ${sessionScope.currentDirector.country.name}
                            </dd>
                            <dt class="col-sm-4">
                                <fmt:message key="picture.director.birthday" bundle="${ rb }" />
                            </dt>
                            <dd class="col-sm-8">
                                <ctm:localDateTime value="${sessionScope.currentDirector.birthDate}" />
                            </dd>
                        </dl>
                    </div>
                </c:if>
            </div>
        </div>
        <div class="row align-self-center col-md-12 ">
            <div class="col-md-4 "></div>
            <div class="col-md-8 ">
                <h4 style="opacity: 0"><fmt:message key="main.welcome" bundle="${ rb }" /></h4>
                <br/>
            </div>
        </div>
    </div>

    <footer class="footer">
        <c:import url="common/footer.jsp" charEncoding="UTF-8"/>
    </footer>

<script src="../js/jquery.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/rating.js"></script>
</body>
</html>