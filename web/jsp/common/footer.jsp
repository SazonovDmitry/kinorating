<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctm" uri="customtags" %>
<ctm:locale value="${sessionScope.currentLocale}"/>
<fmt:setBundle basename="properties/pagecontent" var="rb"/>
<html >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/main.css" rel="stylesheet">
    <link href="../../css/fonts/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <div class="container-fluid block">
        <nav class="navbar navbar-expand-md navbar-light bg-faded">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto ">
                    <li class="nav-item ">
                        <a class="nav-link" href="${ pageContext.servletContext.contextPath }/jsp/main.jsp">
                            <fmt:message key="header.main" bundle="${ rb }" />
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${ pageContext.servletContext.contextPath }/jsp/login.jsp">
                            <fmt:message key="header.login" bundle="${ rb }" />
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="${ pageContext.servletContext.contextPath }/jsp/admin/admin.jsp">
                            <fmt:message key="header.office" bundle="${ rb }" />
                        </a>
                    </li>
                </ul>
            </div>

            <jsp:useBean id="currentDate" class="java.util.Date"/>
            <fmt:message key="footer.today" bundle="${ rb }" /> <fmt:formatDate value="${currentDate}" /> <br/>

        </nav>
        <div class="col-md-12 parent">
            <div class="child">
                <a class="navbar-brand " href="https://vk.com/sazonov327">
                    <span class="fa fa-vk vk" aria-hidden="true"></span>
                    <fmt:message key="footer.vk" bundle="${ rb }" />
                </a>
            </div>
            <div class="child">
                <a class="navbar-brand " href="https://www.facebook.com">
                    <span class="fa fa-facebook-official vk" aria-hidden="true"></span>
                    <fmt:message key="footer.fb" bundle="${ rb }" />
                </a>
            </div>
            <div class="child">
                <a class="navbar-brand " href="https://gmail.com">
                    <span class="fa fa-envelope-o vk" aria-hidden="true"></span>
                    <fmt:message key="footer.email" bundle="${ rb }" />
                </a> <fmt:message key="footer.email.address" bundle="${ rb }" />
            </div>
        </div>
    </div>
<script src="../../js/jquery.min.js"></script>
<script src="../../js/popper.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
</body>
</html>
