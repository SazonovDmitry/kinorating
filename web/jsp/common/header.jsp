<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctm" uri="customtags" %>
<ctm:locale value="${sessionScope.currentLocale}"/>
<fmt:setBundle basename="properties/pagecontent" var="rb"/>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/main.css" rel="stylesheet">
    <link href="../../css/fonts/font-awesome.min.css" rel="stylesheet">
</head>
<body>

<div class="container-fluid">
    <div class="language-parent ">
        <div class="row mt-md-3">

            <div class="col-md-10 child ">
                <form name="logoutForm" method="POST" action="${ pageContext.servletContext.contextPath }/controller">
                    <input type="hidden" name="command" value="logout" />
                    <c:set var="submitLO"><fmt:message key="header.logout" bundle="${ rb }" /></c:set>
                    <input type="submit" class="!important btn btn-outline-info" value="${submitLO}"/>
                </form>
            </div>
            <div class="col-md-2 child">
                <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                    <div class="btn-group mr-2" role="group" aria-label="First group">
                        <form name="langForm" method="GET" action="${ pageContext.servletContext.contextPath }/controller">
                            <input type="hidden" name="command" value="language" />
                            <input type="hidden" name="from" value="${pageContext.request.requestURI}" />
                            <c:set var="rus"><fmt:message key="language.russia" bundle="${ rb }" /></c:set>
                            <input type="submit" name="lang" class="btn btn-info rounded-circle ru-flag" value="RU" title="${rus}"/>
                            <c:set var="eng"><fmt:message key="language.english" bundle="${ rb }" /></c:set>
                            <input type="submit" name="lang" class="btn btn-info rounded-circle en-flag" value="EN" title="${eng}"/>
                            <c:set var="bel"><fmt:message key="language.belarus" bundle="${ rb }" /></c:set>
                            <input type="submit" name="lang" class="btn btn-info rounded-circle by-flag" value="BY" title="${bel}"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-expand-md navbar-light bg-faded">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="${pageContext.servletContext.contextPath}/jsp/main.jsp">
            <span class="fa fa-film fa-spin " aria-hidden="true"></span><strong>
            <fmt:message key="header.logo" bundle="${ rb }" /></strong></a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto ">
                <li class="nav-item ">
                    <a class="nav-link" href="${pageContext.servletContext.contextPath}/jsp/main.jsp">
                        <fmt:message key="header.main" bundle="${ rb }" />
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.servletContext.contextPath}/jsp/login.jsp">
                        <fmt:message key="header.login" bundle="${ rb }" />
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.servletContext.contextPath}/jsp/admin/admin.jsp">
                        <fmt:message key="header.office" bundle="${ rb }" />
                    </a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0" name="searchForm" method="GET"
                  action="${ pageContext.servletContext.contextPath }/controller">
                <input type="hidden" name="command" value="search" />
                <c:set var="search" scope="request"><fmt:message key="header.ph.search" bundle="${ rb }" /></c:set>
                <input name="searchQuery" class="form-control mr-sm-2" type="text" placeholder="${search}" value="">
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">
                    <fmt:message key="header.search" bundle="${ rb }" />
                </button>
            </form>
        </div>
    </nav>
</div>



<script src="../../js/jquery.min.js"></script>
<script src="../../js/popper.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/dropdown.js"></script>
</body>
</html>
