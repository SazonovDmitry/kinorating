<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctm" uri="customtags" %>
<ctm:locale value="${sessionScope.currentLocale}"/>
<fmt:setBundle basename="properties/pagecontent" var="rb"/>
<html >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><fmt:message key="main.page" bundle="${ rb }" /></title>
    <link rel="stylesheet" href="../css/normalize.css">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">
    <link href="../css/fonts/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <header>
        <c:import url="common/header.jsp" charEncoding="UTF-8"/>
    </header>

        <div class="container">
            <div class="row align-self-center col-md-12 ">
                <div class="col-md-4 "></div>
                <div class="col-md-8 ">
                    <h4><fmt:message key="main.welcome" bundle="${ rb }" /></h4>
                </div>
            </div>
            <div class="row align-self-center col-md-12 ">
                <div class="col-md-1 "></div>
                <div class="col-md-8 block rounded">

                    <div class="col-md-12 child">
                        <div class="row m-2" >
                            <div class="col-md-8 " >
                                <c:choose>
                                    <c:when test="${sessionScope.userRole eq 'GUEST'}" >
                                        <h5><fmt:message key="main.hello.quest" bundle="${ rb }" /></h5>
                                    </c:when>
                                    <c:otherwise>
                                        <h5><fmt:message key="main.hello.admin" bundle="${ rb }" />
                                                ${sessionScope.userName}! ${requestScope.congrOnSuccessRegister}
                                        </h5>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <div class="col-md-4">
                                <form name="topForm" method="GET" action="${ pageContext.servletContext.contextPath }/controller">
                                    <input type="hidden" name="command" value="top-pictures" />
                                    <c:set var="top"><fmt:message key="main.top" bundle="${ rb }" /></c:set>
                                    <input type="submit" class="film-button" value="${top}"/>
                                </form>
                            </div>
                        </div>
                    </div>

                    <br/>
                    ${requestScope.emptyQueryResult}
                    <br/>
                    <c:if test="${requestScope.numberPicturesByQuery != null}">
                        <fmt:message key="main.pictures.found" bundle="${ rb }" />
                        ${requestScope.numberPicturesByQuery} <fmt:message key="main.pictures.found.end" bundle="${ rb }" />
                    </c:if>
                    <br/>
                    <c:set var="listCount" value="${sessionScope.picturesByQuery.size() / 10}" scope="page" />

                    <div class="col-12">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th> № </th>
                                <th><fmt:message key="main.picture.title" bundle="${ rb }" /></th>
                                <th><fmt:message key="main.picture.year" bundle="${ rb }" /></th>
                                <th><fmt:message key="main.picture.rating" bundle="${ rb }" /></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="elem" items="${sessionScope.picturesByQuery}" varStatus="status" >
                                <c:if test="${sessionScope.min != null ? status.count > sessionScope.min &&
                              status.count < sessionScope.max : status.count <= 10}">
                                    <tr>
                                        <th scope="row">
                                            <c:out value="${status.count}" />
                                        </th>
                                        <td>
                                            <form name="pictureForm" method="GET"
                                                  action="${ pageContext.servletContext.contextPath }/controller">
                                                <input type="hidden" name="command" value="picture"/>
                                                <input type="hidden" name="pictureId" value="${elem.pictureId}"/>
                                                <c:set var="title" value="${ elem.name }"/>
                                                <input type="submit" class="film-button" value="${title}"/>
                                            </form>
                                        </td>
                                        <td><c:out value="${ elem.year }" /></td>
                                        <td><strong><c:out value="${ elem.rating }" /></strong></td>
                                    </tr>
                                </c:if>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-12 row justify-content-center">
                        <div class="btn-toolbar col-7 justify-content-center pb-3" role="toolbar" aria-label="Toolbar with button groups">
                            <c:if test="${sessionScope.picturesByQuery != null && sessionScope.picturesByQuery.size() > 10}">
                                <c:forEach var="elem" items="${sessionScope.picturesByQuery}" varStatus="status">
                                    <c:if test="${(sessionScope.picturesByQuery.size() % 10) != 0 ? status.count <= listCount + 1 :
                                        status.count <= listCount}">
                                        <form name="listForm" method="GET" action="${ pageContext.servletContext.contextPath }/controller">
                                            <input type="hidden" name="command" value="page-iterator"/>
                                            <input type="hidden" name="address" value="main"/>
                                            <div class="btn-group mr-2" role="group" aria-label="First group">
                                                <button type="submit" name="pageCount" class="btn btn-info" value="${status.count}">
                                                    <c:out value="${status.count}" />
                                                </button>
                                            </div>
                                        </form>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                        </div>
                    </div>


                    <div id="carouselExampleIndicators" class="carousel slide block2 rounded" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="d-block img-fluid" src="../images/avangers.jpg" alt="First slide" >
                                <div class="carousel-caption d-none d-md-block">
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block img-fluid" src="../images/hobbit.jpg" alt="Second slide" >
                                <div class="carousel-caption d-none d-md-block">
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block img-fluid" src="../images/league.jpg" alt="Third slide" >
                                <div class="carousel-caption d-none d-md-block">
                                </div>
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>

                </div>

                <!--Find picture-->
                <div class="col-md-3">
                    <div class="col-md-12 block2 rounded " data-spy="affix" data-offset-top="0">
                        <h3 class="p-3"><fmt:message key="main.find.film" bundle="${ rb }" /></h3>

                        <div class="col-12">

                            <form name="searchForm" method="GET" action="${ pageContext.servletContext.contextPath }/controller">
                                <input type="hidden" name="command" value="search-filter" />
                                <div class="row ">
                                    <div class="col-12">
                                        <strong><fmt:message key="main.insert.year" bundle="${ rb }" /></strong>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12">
                                        <div class="form-group row">
                                            <label for="from" class="col-3 col-form-label"><fmt:message key="admin.review.date.from" bundle="${ rb }" /></label>
                                            <div class="col-9">
                                                <input type="text" name="from" id="from" pattern="^\d{4}$"
                                                       class="form-control" value=""
                                                       data-toggle="tooltip" data-placement="right"
                                                       title="<fmt:message key="from.date.tooltip" bundle="${rb}"/>"
                                                       oninvalid="setCustomValidity('<fmt:message key="from.filter.incorrect" bundle="${rb}"/>')"
                                                       onchange="setCustomValidity('')"
                                                       oninput="setCustomValidity('')"
                                                       onblur="setCustomValidity('')"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <div class="form-group row">
                                            <label for="to" class="col-3 col-form-label"><fmt:message key="admin.review.date.to" bundle="${ rb }" /></label>
                                            <div class="col-9">
                                                <input type="text" name="to" id="to" pattern="^\d{4}$"
                                                       class="form-control" value=""
                                                       data-toggle="tooltip" data-placement="right"
                                                       title="<fmt:message key="from.date.tooltip" bundle="${rb}"/>"
                                                       oninvalid="setCustomValidity('<fmt:message key="from.filter.incorrect" bundle="${rb}"/>')"
                                                       onchange="setCustomValidity('')"
                                                       oninput="setCustomValidity('')"
                                                       onblur="setCustomValidity('')"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row pb-1">
                                    <label for="inlineRadio1" class="col-12 col-form-label">
                                        <strong><fmt:message key="admin.placeholder.picture.type" bundle="${ rb }" /></strong>
                                    </label>
                                    <div class="col-12">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="type" id="inlineRadio1" value="serial">
                                                <fmt:message key="admin.placeholder.picture.type.serial" bundle="${ rb }" />
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="type" id="inlineRadio2" value="film" >
                                                <fmt:message key="admin.placeholder.picture.type.film" bundle="${ rb }" />
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row pb-1">
                                    <div class=" form-group col-12">
                                        <div class="form-group row">
                                            <label for="age" class="col-12 col-form-label">
                                                <strong><fmt:message key="admin.placeholder.picture.age" bundle="${ rb }" /></strong>
                                            </label>
                                            <div class="col-12">
                                                <input type="range" min="0" max="21" step="1" name="age" id="age" class="form-control " value=""
                                                       onchange="document.getElementById('rangeValue').innerHTML = this.value;" />
                                                <span id="rangeValue">18</span>+
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="dropdown pb-4">
                                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton1"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <fmt:message key="admin.button.choose.countries" bundle="${ rb }" />
                                    </button>
                                    <div class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton1">
                                        <c:forEach var="countryElem" items="${applicationScope.countriesDirectory}" >
                                            <div class="form-check form-check-inline col-12 p-1">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="countries" type="checkbox" id="inlineCheckbox1"
                                                           value="${ countryElem.countryId }">
                                                    <span><c:out value="${ countryElem.name }" /></span>
                                                </label>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>

                                <div class="dropdown pb-4">
                                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton2"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <fmt:message key="admin.button.choose.genres" bundle="${ rb }" />
                                    </button>
                                    <div class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton2">
                                        <c:forEach var="genreElem" items="${applicationScope.genresDirectory}" >

                                            <div class="form-check form-check-inline col-12 p-1">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="genres" type="checkbox" id="inlineCheckbox2"
                                                           value="${ genreElem.genreId }">
                                                    <span><c:out value="${ genreElem.name }" /></span>
                                                </label>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>

                                <div class="dropdown pb-3">
                                    <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton3"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <fmt:message key="admin.button.choose.directors" bundle="${ rb }" />
                                    </button>
                                    <div class="dropdown-menu pre-scrollable" aria-labelledby="dropdownMenuButton3">
                                        <c:forEach var="directorElem" items="${applicationScope.directorsDirectory}" >
                                            <div class="form-check form-check-inline col-12 p-1">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" name="directors" type="checkbox" id="inlineCheckbox3"
                                                           value="${ directorElem.directorId }">
                                                    <span><c:out value="${ directorElem.name }" /> </span>
                                                    <span><c:out value="${ directorElem.lastName }" /></span>
                                                </label>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>

                                <div class="col-12 pb-2 ">
                                    <c:set var="submitSI"><fmt:message key="admin.search.review.submit" bundle="${ rb }" /></c:set>
                                    <input type="submit" class="btn btn-primary" value="${pageScope.submitSI}">
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row align-self-center col-md-12 ">
                <div class="col-md-4 "></div>
                <div class="col-md-8 ">
                    <h4 style="opacity: 0"><fmt:message key="main.welcome" bundle="${ rb }" /></h4>
                </div>
            </div>
        </div>

    <footer class="footer">
        <c:import url="common/footer.jsp" charEncoding="UTF-8"/>
    </footer>

<script src="../js/jquery.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

</body>
</html>
