<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctm" uri="customtags" %>
<ctm:locale value="${sessionScope.currentLocale}"/>
<fmt:setBundle basename="properties/pagecontent" var="rb"/>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><fmt:message key="mail.page" bundle="${ rb }" /></title>
    <link rel="stylesheet" href="../../css/normalize.css">
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/main.css" rel="stylesheet">
    <link href="../../css/background.css" rel="stylesheet">
    <link href="../../css/fonts/font-awesome.min.css" rel="stylesheet">
</head>
<body>
    <header>
        <c:import url="common/header.jsp" charEncoding="UTF-8"/>
    </header>
    <div class="container-fluid p-wrapper ">
        <div class="col-md-8 align-self-center block rounded ">
            <div class="row justify-content-center">
                <div class="col-12"><br/><br/></div>
                <div class="col-6 form-block align-self-center rounded p-2 ">
                    <form action="${ pageContext.servletContext.contextPath }/controller" method="POST" >
                        <input type="hidden" name="command" value="password-recovery" />
                        <input type="hidden" name="fromPage" value="${pageContext.request.requestURI}" />
                        <label for="email" class="required">
                            <fmt:message key="insert.your.email" bundle="${ rb }" />
                        </label>
                        <c:set var="emailph"><fmt:message key="placeholder.email" bundle="${ rb }" /></c:set>
                        <input type="text" name="email" id="email" required  placeholder="${emailph}"
                               class="form-control"
                               pattern="^([\w|\.]{6,})@(\w+\.)(\w{2,4})$"
                               data-toggle="tooltip" data-placement="right"
                               title="<fmt:message key="tooltip.email" bundle="${rb}"/>"
                               oninvalid="setCustomValidity('<fmt:message key="email.incorrect" bundle="${rb}"/>')"
                               onchange="setCustomValidity('')"
                               oninput="setCustomValidity('')"
                               onblur="setCustomValidity('')"/>
                        <div class="row p-2 justify-content-center">
                            <div class="col-6">
                                <input type="submit" value="<fmt:message key="send.email" bundle="${rb}"/>" class="btn btn-primary"/>
                            </div>
                        </div>
                    </form>
                    <div class="row p-2 justify-content-center">
                        <div class="col-10">
                            <h5 class="text-success">${requestScope.mailSuccessfullySent}</h5>
                            <h5 class="text-danger">${requestScope.unknownEmail}</h5>
                            <h5 class="text-danger">${requestScope.renameError}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <c:import url="common/footer.jsp" charEncoding="UTF-8"/>
    </footer>
</body>
</html>
