<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:choose>
  <c:when test="${ sessionScope.currentLocale eq 'RU'}" >
    <fmt:setLocale value="ru_RU" scope="request"/>
  </c:when>
  <c:when test="${ sessionScope.currentLocale eq 'EN'} " >
    <fmt:setLocale value="en_US" scope="request"/>
  </c:when>
  <c:when test="${ sessionScope.currentLocale eq 'BY'} " >
    <fmt:setLocale value="be_BY" scope="request"/>
  </c:when>
  <c:otherwise>
    <fmt:setLocale value="en_US" scope="request"/>
  </c:otherwise>
</c:choose>
<fmt:setBundle basename="properties/pagecontent" var="rb"/>
<html>
  <head>
    <title><fmt:message key="index.page" bundle="${ rb }" /></title>
  </head>
  <body>
    <jsp:forward page="${ pageContext.servletContext.contextPath }/jsp/main.jsp"/>
  </body>
</html>